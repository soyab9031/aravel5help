<?php

use Illuminate\Database\Seeder;

class FirstUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {

            $brand = config('project.brand');

            $faker = Faker\Factory::create('en_IN');

            $package = \App\Models\Package::create([
                'name' => 'First Package',
                'prefix' => 'first-package',
                'amount' => '0',
                'capping' => 0,
                'sponsor_income' => 0,
                'pv' => 0,
                'declaration' => 'Certified that I am at least 18 years of age. I have read completely and understood all the Terms and Conditions for availing PACKAGE. I have received complete Products & Services immediately after registration. I am completely satisfied with ' .$brand. ' Product/services.I have completely understood how to learn courses given in Package online by myself (using Computer/mobile and Internet at my own cost). I am aware that I can evaluate the purchased Product online and the cost of the product would only be sent, if completely satisfied. I am aware that the product cost could be sent only if satisfied to ' .$brand. ' office within 15 days after the date of registration for Purchase. The refund policy is understood by me very clearly.I am aware of all the ' .$brand. '. TERMINATION AND CANCELLATION POLICIES and adhere to the consequences if any. I have carefully read the Terms and Conditions and FAQs applicable to ' .$brand. '. as given on website www.xyz.com and agree / accept to them. I am signing this DECLARATION with complete understanding and with my own WILL, without any PRESSURE and INFLUENCE. I am aware that any dispute arising out of this purchase would first be solved as per Terms and Conditions of the company, failing which could be addressed exclusively in jurisdiction of (INDIA) only.
                                ',
            ]);


            $payment_detail = \App\Models\PinPaymentDetail::create([
                'payment_mode' => 'IMPS',
                'qty' => 1,
                'payment_at' => \Carbon\Carbon::now(),
                'remarks' => 'First Member Payment'
            ]);


            $pin = \App\Models\Pin::create([
                'admin_id' => \App\Models\Admin::first()->id,
                'package_id' => $package->id,
                'pin_payment_id' => $payment_detail->id,
                'number' =>  strtoupper(str_random(15)),
                'amount' => 0,
            ]);

            $user = \App\Models\User::create([
                'parent_id' => null,
                'sponsor_by' => null,
                'pin_id' => $pin->id,
                'package_id' => $package->id,
                'joining_amount' => $package->amount,
                'tracking_id' => '100000',
                'username' => config('project.brand'),
                'password' => strtoupper(str_random(8)),
                'wallet_password' => strtoupper(str_random(6)),
                'email' => $faker->email,
                'token' => strtoupper(str_random(20)),
                'paid_at' => \Carbon\Carbon::now(),
                'mobile' => $faker->numberBetween(7894561230, 9876543210)
            ]);

            \App\Models\PackageOrder::create([
                'user_id' => $user->id, 'pin_id' => $pin->id, 'pv_calculation' => 1, 'package_id' => $package->id, 'amount' => $package->amount, 'pv' => $package->pv
            ]);

            $pin->user_id = $user->id;
            $pin->status = \App\Models\Pin::USED;
            $pin->save();

            \App\Models\UserDetail::create([
                'user_id' => $user->id,
                'title' => 'Mr',
                'first_name' => config('project.brand'),
                'last_name' => 'Person',
                'pan_no' => '',
                'gender' => 1,
                'birth_date' => \Carbon\Carbon::now()->subYears(18),
                'nominee_name' => $faker->name,
                'nominee_relation' => 'Father',
                'nominee_birth_date' => \Carbon\Carbon::now()->subYears(45)
            ]);

            \App\Models\UserStatus::create([
                'user_id' => $user->id,
                'tree_calculation' => \App\Models\UserStatus::YES,
            ]);

            \App\Models\UserBank::create([
                'user_id' => $user->id
            ]);

            \App\Models\UserAddress::create([
                'user_id' => $user->id,
                'address' => $faker->address,
                'landmark' => $faker->address,
                'city' => $faker->city,
                'district' => $faker->city,
                'state_id' => 8,
                'pincode' => $faker->numberBetween(365560, 375000)
            ]);

        });
    }
}
