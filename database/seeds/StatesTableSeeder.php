<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = \App\Models\Country::whereName('India')->first();

        collect(\App\Library\Helper::arrayToObject($this->getStates()))->map( function ($state) use ($country) {

            DB::table('states')->insert([
                'code' => $state->code,
                'name' => $state->name,
                'country_id' => $country->id,
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);

        });
    }

    public function getStates()
    {
        return [
            ['code' => 'AN', 'name' => 'Andaman and Nicobar Islands'],
            ['code' => 'AP', 'name' => 'Andhra Pradesh'],
            ['code' => 'AR', 'name' => 'Arunachal Pradesh'],
            ['code' => 'AS', 'name' => 'Assam'],
            ['code' => 'BR', 'name' => 'Bihar'],
            ['code' => 'CT', 'name' => 'Chhattisgarh'],
            ['code' => 'DN', 'name' => 'Dadra and Nagar Haveli'],
            ['code' => 'DD', 'name' => 'Daman and Diu'],
            ['code' => 'DL', 'name' => 'Delhi'],
            ['code' => 'GA', 'name' => 'Goa'],
            ['code' => 'GJ', 'name' => 'Gujarat'],
            ['code' => 'HR', 'name' => 'Haryana'],
            ['code' => 'HP', 'name' => 'Himachal Pradesh'],
            ['code' => 'JK', 'name' => 'Jammu and Kashmir'],
            ['code' => 'JH', 'name' => 'Jharkhand'],
            ['code' => 'KA', 'name' => 'Karnataka'],
            ['code' => 'KL', 'name' => 'Kerala'],
            ['code' => 'LD', 'name' => 'Lakshadweep'],
            ['code' => 'MP', 'name' => 'Madhya Pradesh'],
            ['code' => 'MH', 'name' => 'Maharashtra'],
            ['code' => 'MN', 'name' => 'Manipur'],
            ['code' => 'ML', 'name' => 'Meghalaya'],
            ['code' => 'MZ', 'name' => 'Mizoram'],
            ['code' => 'NL', 'name' => 'Nagaland'],
            ['code' => 'OR', 'name' => 'Orissa'],
            ['code' => 'PY', 'name' => 'Pondicherry'],
            ['code' => 'PB', 'name' => 'Punjab'],
            ['code' => 'RJ', 'name' => 'Rajasthan'],
            ['code' => 'SK', 'name' => 'Sikkim'],
            ['code' => 'TN', 'name' => 'Tamil Nadu'],
            ['code' => 'TR', 'name' => 'Tripura'],
            ['code' => 'UP', 'name' => 'Uttar Pradesh'],
            ['code' => 'UT', 'name' => 'Uttarakhand'],
            ['code' => 'WB', 'name' => 'West Bengal'],

        ];
    }
}
