<?php

use Illuminate\Database\Seeder;

class SupportCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Pan Card', 'Name Correction', 'Pin Request', 'Payout', 'Product', 'Other'
        ];

        collect($categories)->map( function ($category) {

            \App\Models\SupportCategory::create([
               'name' => $category
            ]);

        });
    }
}
