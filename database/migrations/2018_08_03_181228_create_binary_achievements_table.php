<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinaryAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('binary_achievements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('calculation_id')->unsigned();
            $table->integer('calculation_reference_id')->unsigned();
            $table->tinyInteger('status')->comment('1: Active, 2: Inactive');
            $table->integer('wallet_id')->unsigned()->nullable();
            $table->timestamps();
            $table->index('calculation_id');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('binary_achievements');
    }
}
