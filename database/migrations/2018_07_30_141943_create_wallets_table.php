<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('payout_id')->unsigned()->nullable();
            $table->integer('wallet_id')->unsigned()->nullable();
            $table->tinyInteger('type')->comment('1:Credit, 2:Debit');
            $table->tinyInteger('income_type')->comment('1:Binary, 2:Sponsor, 3: Other, 4:Re-Purchase, 5:Level, 6:Royalty, 7:Reward, 8:ROI, 9:Matrix');
            $table->float('total');
            $table->float('tds');
            $table->float('admin_charge');
            $table->float('service_charge');
            $table->float('amount');
            $table->text('remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('admin_id')->references('id')->on('admins');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wallets');
    }
}
