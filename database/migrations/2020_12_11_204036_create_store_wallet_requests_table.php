<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreWalletRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_wallet_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id')->unsigned()->index();
            $table->integer('admin_id')->nullable()->index();
            $table->integer('wallet_id')->nullable();
            $table->float('amount', 10, 2)->default(0);
            $table->string('payment_mode');
            $table->string('reference_number');
            $table->string('bank_name');
            $table->dateTime('deposited_at');
            $table->string('image', 100)->nullable();
            $table->tinyInteger('status')->comment('1: Pending, 2: Approved, 3: Rejected')->default(1);
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_wallet_requests');
    }
}
