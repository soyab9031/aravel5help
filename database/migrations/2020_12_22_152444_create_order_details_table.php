<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->index()->unsigned();
            $table->integer('product_price_id')->index()->unsigned();
            $table->float('price', 10, 2);
            $table->float('distributor_price', 10, 2);
            $table->float('selling_price', 10, 2);
            $table->float('bv', 10, 2);
            $table->integer('qty')->unsigned();
            $table->string('gst')->comment('Json: percentage & hsn/sac code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
