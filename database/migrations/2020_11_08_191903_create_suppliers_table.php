<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->index();
            $table->string('name', 100);
            $table->string('email', 100)->nullable();
            $table->string('mobile', 20);
            $table->string('other_contact', 50)->nullable();
            $table->string('gst_number', 100);
            $table->string('address');
            $table->string('city', 100);
            $table->string('pincode', 10);
            $table->integer('state_id')->index();
            $table->tinyInteger('status')->default(1)->comment('1: Active, 2: Inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
