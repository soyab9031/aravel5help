<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinaryCalculationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('binary_calculations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('left');
            $table->integer('right');
            $table->float('total_left');
            $table->float('total_right');
            $table->float('current_left');
            $table->float('current_right');
            $table->float('forward_left');
            $table->float('forward_right');
            $table->tinyInteger('status')->default('0')->comment('0: Open, 1: Closed, 2: Rejected');
            $table->text('remarks')->nullable();
            $table->datetime('closed_at')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('binary_calculations');
    }
}
