<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('image');
            $table->string('secondary_image')->nullable();
            $table->tinyInteger('type')->comment('1: PanCard, 2: Invoice, 3: Cancel Cheque, 4: Bank Passbook, 5: Aadhar Card, 6: Voter Id, 7: Electricity Bill, 8: Paasport, 9: Driving Licence');
            $table->tinyInteger('status')->comment('1: Pending, 2: Verified, 3: Rejected')->default(1);
            $table->text('remarks')->nullable();
            $table->string('number')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_documents');
    }
}
