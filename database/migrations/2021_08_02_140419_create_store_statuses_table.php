<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id')->index();
            $table->tinyInteger('pancard')->comment('1: Pending, 2: Verified, 3:InProgress, 4:Rejected')->default(1);
            $table->tinyInteger('aadhar_card')->comment('1: Pending, 2: Verified, 3:InProgress, 4:Rejected')->default(1);
            $table->tinyInteger('gst')->comment('1: Pending, 2: Verified, 3:InProgress, 4:Rejected')->default(1);
            $table->tinyInteger('address_proof')->comment('1: Pending, 2: Verified, 3:InProgress, 4:Rejected')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_statuses');
    }
}
