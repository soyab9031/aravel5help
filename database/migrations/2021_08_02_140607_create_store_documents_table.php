<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id')->unsigned();
            $table->string('image');
            $table->string('secondary_image')->nullable();
            $table->tinyInteger('type')->comment('1: PanCard, 2: AadharCard, 3: GSTNumber,4: AddressProof');
            $table->tinyInteger('status')->comment('1: Pending, 2: Verified, 3: Rejected')->default(1);
            $table->text('remarks')->nullable();
            $table->string('number')->nullable();
            $table->timestamps();
            $table->foreign('store_id')->references('id')->on('stores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_documents');
    }
}
