<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockPurchaseDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_purchase_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stock_purchase_id');
            $table->integer('product_price_id')->index()->unsigned();
            $table->float('amount',10,2);
            $table->integer('qty');
            $table->string('gst')->comment('Json: percentage & hsn/sac code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_purchase_details');
    }
}
