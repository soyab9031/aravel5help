<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStorePendingCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_pending_commissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id')->unsigned();
            $table->integer('sender_store_id')->unsigned()->nullable();
            $table->integer('admin_id')->unsigned()->nullable();
            $table->float('amount', 14,4);
            $table->float('tds', 14,4);
            $table->float('total', 14,4);
            $table->text('remarks')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1: Pending, 2:Transferred');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_pending_commissions');
    }
}
