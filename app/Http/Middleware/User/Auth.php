<?php

namespace App\Http\Middleware\User;

use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!\Session::has('user'))
            return redirect()->route('user-login')->with(['error' => 'Session has been Expire, Login Again']);

        return $next($request);
    }
}
