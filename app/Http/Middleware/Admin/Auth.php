<?php

namespace App\Http\Middleware\Admin;

use App\Models\AdminRoute;
use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!\Session::has('admin'))
            return redirect()->route('admin-login')->with(['error' => 'Session has been Expire, Login Again']);

        $current_route = \Route::getCurrentRoute()->getName();

        $isOpenRoute = AdminRoute::openRoutes()->filter( function ($open_route) use ($current_route) {
            return $current_route == $open_route->key;
        });

        if ($isOpenRoute->count() == 0)
        {
            if(!AdminRoute::where('route', $current_route)->where('admin_id', \Session::get('admin')->id )->first())
                return redirect()->route('admin-access-denied');
        }


        return $next($request);
    }
}
