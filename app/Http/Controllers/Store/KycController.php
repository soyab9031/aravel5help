<?php

namespace App\Http\Controllers\Store;

use App\Library\ImageUpload;
use App\Models\State;
use App\Models\StoreManager\StoreBank;
use App\Models\StoreDocument;
use App\Models\StoreStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KycController extends Controller
{
    public function dashboard()
    {
        $store_status = StoreStatus::whereStoreId(\Session::get('store')->id)->first();

        return view('store-manager.kyc.dashboard', [
            'store_status' => $store_status
        ]);
    }

    public function pancard(Request $request)
    {
        $store_status = StoreStatus::whereStoreId(\Session::get('store')->id)->first();

        if ($request->isMethod('post'))
        {
            $this->validate($request,[
                'image' => 'required|mimes:jpeg,jpg,png|max:4000',
                'pancard' => 'required|regex:/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/',
            ],[
                'image.image' => 'Invalid image',
                'image.max' => 'Image Size should be less than 2MB',
                'image.required' => 'Upload File is Required',
                'pancard.regex' => 'Invalid PanCard Number',
                'pancard.required' => 'Pan Number is required'
            ]);

            $image_uploader = (new ImageUpload());
            if(!$image_uploader->process($request->file('image'),ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Unable to fetch Image, Upload another image']);

            $image_name = $image_uploader->store(env('DOCUMENT_IMAGE_PATH'));

            $pancard_exists = StoreDocument::whereNumber($request->pan_number)->whereType(StoreDocument::PAN_CARD)
                ->whereIn('status', [StoreDocument::PENDING, StoreDocument::VERIFIED])->exists();

            if ($pancard_exists) {
                return redirect()->back()->withInput()->with(['error' => 'Pan Number: ' . $request->pan_number . ' is already registered with Other User so You can not enter again']);
            }

            StoreDocument::create([
                'store_id' => \Session::get('store')->id,
                'image' => $image_name,
                'type' => StoreDocument::PAN_CARD,
                'number'=> $request->pancard,
                'status' => StoreDocument::PENDING
            ]);

            $store_status->update(['pancard' => StoreStatus::KYC_INPROGRESS]);

            return redirect()->route('store-kyc-dashboard')->with(['success' => 'You Pan card details are submitted, It will be reviewed soon']);
        }

        $documents = StoreDocument::whereType(StoreDocument::PAN_CARD)->latest()->whereStoreId(\Session::get('store')->id)->get();

        return view('store-manager.kyc.pancard', [
            'store_status' => $store_status,
            'documents' => $documents
        ]);
    }

    public function aadharNumber(Request $request)
    {
        $store_status = StoreStatus::whereStoreId(\Session::get('store')->id)->first();

        if ($request->isMethod('post'))
        {
            $this->validate($request,[
                'image' => 'required|mimes:jpeg,jpg,png|max:4000',
                'image2' => 'required|mimes:jpeg,jpg,png|max:4000',
                'aadhar_number' => 'required|regex:/^[0-9]{12}?$/',
            ],[
                'image.image' => 'Invalid Front image',
                'image.max' => 'Front Image Size should be less than 2MB',
                'image.required' => 'Upload Front Image is Required',
                'image2.image' => 'Back Invalid image',
                'image2.max' => 'Back Image Size should be less than 2MB',
                'image2.required' => 'Upload Back Image is Required',
                'aadhar_number.regex' => 'Invalid Aadhar Number',
                'aadhar_number.required' => 'Aadhar Number is required'
            ]);

            $image_uploader = (new ImageUpload());
            if(!$image_uploader->process($request->file('image'),ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Invalid / Broken or Corrupted Front Image, Try Another']);

            $image_name = $image_uploader->store(env('DOCUMENT_IMAGE_PATH'));

            $image_uploader2 = (new ImageUpload());
            if(!$image_uploader2->process($request->file('image2'),ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Invalid / Broken or Corrupted Back Image, Try Another']);

            $image_name2 = $image_uploader2->store(env('DOCUMENT_IMAGE_PATH'));

            StoreDocument::create([
                'store_id' => \Session::get('store')->id,
                'image' => $image_name,
                'secondary_image' => $image_name2,
                'type' => StoreDocument::AADHAR_CARD,
                'number'=> $request->aadhar_number,
                'status' => StoreDocument::PENDING
            ]);

            $store_status->update(['aadhar_card' => StoreStatus::KYC_INPROGRESS]);

            return redirect()->route('store-kyc-dashboard')->with(['success' => 'You Aadhar card details are submitted, It will be reviewed soon']);
        }

        $documents = StoreDocument::whereType(StoreDocument::AADHAR_CARD)->latest()->whereStoreId(\Session::get('store')->id)->get();

        return view('store-manager.kyc.aadhar-number',[
            'store_status' => $store_status,
            'documents' => $documents
        ]);
    }

    public function gst(Request $request)
    {
        $store_status = StoreStatus::whereStoreId(\Session::get('store')->id)->first();

        if ($request->isMethod('post'))
        {
            $this->validate($request,[
                'image' => 'required|mimes:jpeg,jpg,png|max:4000',
            ],[
                'image.image' => 'Invalid Front image',
                'image.max' => 'Front Image Size should be less than 2MB',
                'image.required' => 'Upload Front Image is Required',
            ]);

            if($request->gst_number){
                if(!preg_match('/^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/', $request->gst_number))
                    return redirect()->back()->with(['error' => 'Invalid GST Number']);
            }

            $image_uploader = (new ImageUpload());
            if(!$image_uploader->process($request->file('image'),ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Invalid / Broken or Corrupted Front Image, Try Another']);

            $image_name = $image_uploader->store(env('DOCUMENT_IMAGE_PATH'));

            StoreDocument::create([
                'store_id' => \Session::get('store')->id,
                'image' => $image_name,
                'type' => StoreDocument::GST_NUMBER,
                'number'=> $request->gst_number,
                'status' => StoreDocument::PENDING
            ]);

            $store_status->update(['gst' => StoreStatus::KYC_INPROGRESS]);

            return redirect()->route('store-kyc-dashboard')->with(['success' => 'You GST details are submitted, It will be reviewed soon']);
        }

        $documents = StoreDocument::whereType(StoreDocument::GST_NUMBER)->latest()->whereStoreId(\Session::get('store')->id)->get();

        return view('store-manager.kyc.gst',[
            'store_status' => $store_status,
            'documents' => $documents
        ]);
    }

    public function address(Request $request)
    {
        $store_status = StoreStatus::whereStoreId(\Session::get('store')->id)->first();

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'type' => 'required',
                'image' => 'required|mimes:jpeg,jpg,png|max:4000',
            ],[
                'type.required' => 'Select Address Proof Type',
                'image.image' => 'Invalid image',
                'image.max' => 'Image Size should be less than 2MB',
            ]);

            $image_uploader = (new ImageUpload());
            if(!$image_uploader->process($request->file('image'),ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Unable to fetch Image, Upload another image']);

            $image_name = $image_uploader->store(env('DOCUMENT_IMAGE_PATH'));


            StoreDocument::create([
                'store_id' => \Session::get('store')->id,
                'image' => $image_name,
                'type' => $request->type,
                'status' => StoreDocument::PENDING
            ]);

            $store_status->update(['address_proof' => StoreStatus::KYC_INPROGRESS]);

            return redirect()->route('store-kyc-dashboard')->with(['success' => 'You Address details are submitted, It will be reviewed soon']);
        }

        $documents = StoreDocument::whereIn('type', [StoreDocument::ELECTRICITY_BILL, StoreDocument::TELEPHONE_BILL])->latest()->whereStoreId(\Session::get('store')->id)->get();

        return view('store-manager.kyc.address', [
            'states' => $states = State::active()->get(),
            'store_status' => $store_status,
            'documents' => $documents
        ]);
    }

    public function bank(Request $request){

        $store_status = StoreStatus::whereStoreId(\Session::get('store')->id)->first();

        $store_bank = StoreBank::whereStoreId(\Session::get('store')->id)->first();

       $allow_update = in_array($store_status->bank_account, [StoreStatus::KYC_PENDING, StoreStatus::KYC_REJECTED]);

        if ($request->isMethod('post'))
        {
            if ($allow_update == false) {
                return redirect()->back()->with(['error' => 'Your Bank KYC Details is Under Reviewed, Will Update Soon']);
            }

            $this->validate($request, [
                'type' => 'required',
                'image' => 'required|mimes:jpeg,jpg,png|max:4000',
                'bank_name' => 'required',
                'account_name' => 'required|min:3',
                'account_number' => 'required|numeric',
                'branch' => 'required|min:3',
                'ifsc' => 'required|min:3',
                'account_type' => 'required|numeric',
                'city' => 'required|min:3'
            ],[
                'type.required' => 'Bank Document Type is required',
                'image.image' => 'Invalid image',
                'image.max' => 'Image Size should be less than 2MB',
                'image.required' => 'Upload Cancel Cheque Cheque or Bank Passbook is Required'
            ]);

            $image_uploader = (new ImageUpload());
            if(!$image_uploader->process($request->file('image'),ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Unable to fetch Image, Upload another image']);

            $image_name = $image_uploader->store(env('DOCUMENT_IMAGE_PATH'));

            StoreDocument::create([
                'store_id' => \Session::get('store')->id,
                'image' => $image_name,
                'type' => $request->type,
                'number'=> $request->account_number,
                'status' => StoreDocument::PENDING
            ]);

            $store_bank->account_name = $request->account_name;
            $store_bank->bank_name = $request->bank_name;
            $store_bank->account_number = $request->account_number;
            $store_bank->branch = $request->branch;
            $store_bank->ifsc = $request->ifsc;
            $store_bank->type = $request->account_type;
            $store_bank->city = $request->city;
            $store_bank->save();

            $store_status->update(['bank_account' => StoreStatus::KYC_INPROGRESS]);

            return redirect()->route('store-kyc-dashboard')->with(['success' => 'You Bank details are submitted, It will be reviewed soon']);
        }

        $documents = StoreDocument::whereIn('type', [StoreDocument::CANCEL_CHEQUE, StoreDocument::BANK_PASSBOOK])->latest()->whereStoreId(\Session::get('store')->id)->get();

        return view('store-manager.kyc.bank',[
            'allow_update' => $allow_update,
            'store_bank' => $store_bank,
            'user_status' => $store_status,
            'documents' => $documents
        ]);
    }
}
