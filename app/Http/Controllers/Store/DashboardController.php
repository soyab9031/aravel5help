<?php

namespace App\Http\Controllers\Store;

use App\Models\Order;
use App\Models\StoreManager\Store;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $store = Store::whereId(\Session::get('store')->id)->first();

        return view('store-manager.dashboard', [
            'store' => $store,
            'order_count' => Order::whereStoreId($store->id)->count(),
            'recent_orders' => Order::whereStoreId(\Session::get('store')->id)->with(['user.detail:user_id,title,first_name,last_name'])->latest()->take(7)->get(),
        ]);
    }

    public function reward()
    {
        return view('store-manager.rewards.index');
    }
}
