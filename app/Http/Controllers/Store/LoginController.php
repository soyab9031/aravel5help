<?php

namespace App\Http\Controllers\Store;

use App\Library\Helper;
use App\Models\Admin;
use App\Models\StoreManager\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'tracking_id' => 'required|exists:stores,tracking_id',
                'password' => 'required'

            ], [
                'tracking_id.required' => 'Store Id is Required',
                'tracking_id.exists' => 'Invalid Store Id',
                'password.required' => 'Password is Required'
            ]);

            $store = Store::where('tracking_id', $request->tracking_id)->first();

            if ($store->status == Store::INACTIVE)
                return redirect()->back()->with(['error' => 'You are blocked to access your account, Contact to Support Team']);

            if($request->password != $store->password)
                return redirect()->back()->with(['error'=> 'Password is Not Match with Store!']);

            \Session::put('store', $store);

            Store::whereId($store->id)->update([
                'last_logged_in_ip' => Helper::getClientIp(), 'last_logged_in_at' => Carbon::now()
            ]);

            return redirect()->route('store-dashboard')->with([
                'success' => 'Welcome to ' . config('project.brand') . ' Store account'
            ]);

        }

        return view('store-manager.login');
    }

    public function logout(Request $request)
    {
        \Session::forget('store');

        return redirect()->route('store-login')->with(['success' => 'Store Account Logout successfully']);
    }

    public function adminAccess(Request $request)
    {

        if (!\Session::has('admin'))
            return redirect()->back()->with(['error' => 'Invalid Credentials for Store Access']);

        if (!Admin::whereToken($request->token)->exists())
            return redirect()->back()->with(['error' => 'Invalid Admin token for Store Access']);

        \Session::forget('store');

        if (!$store = Store::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Credentials for Store Access']);

        \Session::put('store', $store);

        return redirect()->route('store-dashboard');

    }
}
