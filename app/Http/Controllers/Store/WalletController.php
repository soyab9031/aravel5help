<?php

namespace App\Http\Controllers\Store;

use App\Library\ImageUpload;
use App\Models\StoreManager\StoreWalletRequest;
use App\Models\StoreManager\StoreWalletTransaction;
use App\Models\StorePendingCommission;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WalletController extends Controller
{
    public function index(Request $request)
    {
        $transactions = StoreWalletTransaction::filterDate($request->dateRange)
            ->where('status',StoreWalletTransaction::ACTIVE)
            ->whereStoreId(\Session::get('store')->id)->latest()->paginate(50);

        $total_credit = StoreWalletTransaction::whereStoreId(\Session::get('store')->id)
            ->where('status',StoreWalletTransaction::ACTIVE)
            ->whereType(1)->sum('amount');

        $total_debit = StoreWalletTransaction::whereStoreId(\Session::get('store')->id)
            ->where('status',StoreWalletTransaction::ACTIVE)
            ->whereType(2)->sum('amount');

        return view('store-manager.wallet.index', [
            'transactions' => $transactions,
            'total_credit' => $total_credit,
            'total_debit' => $total_debit,
            'balance' => ($total_credit - $total_debit)
        ]);
    }

    public function requestView(Request $request)
    {
        $wallet_requests = StoreWalletRequest::whereStoreId(\Session::get('store')->id)->latest()->paginate(50);

        return view('store-manager.wallet.request.index', [
            'wallet_requests' => $wallet_requests
        ]);
    }

    public function requestCreate(Request $request)
    {
        if($request->isMethod('post')) {

            $this->validate($request, [
                'amount' => 'required|numeric',
                'payment_mode' => 'required',
                'reference_number' => 'required',
                'bank_name' => 'required',
                'deposit_date' => 'required',
                'deposit_time' => 'required',
                'image' => 'required|image|mimes:jpeg,jpg,png|max:4000'
            ], [
                'amount.required' => 'Request Amount is Required',
                'payment_mode.required' => 'Payment Mode is Required',
                'reference_number.required' => 'Reference Number is Required',
                'bank_name.required' => 'Bank Name is Required',
                'deposit_date.required' => 'Deposit Date is Required',
                'deposit_time.required' => 'Deposit Time is Required',
                'image.required' => 'Deposit Receipt Image is Required',
            ]);

            if(StoreWalletRequest::where('reference_number', $request->reference_number)->where('status', StoreWalletRequest::APPROVED)->exists())
                return redirect()->back()->withInput()->with(['error' => 'Entered reference number is already exists, please enter another reference number']);

            $image_name = null;

            if($request->image)
            {
                if ($request->file('image')->isValid())
                {
                    $image_uploader = (new ImageUpload());
                    if(!$image_uploader->process($request->file('image'),ImageUpload::DOCUMENT_TYPE))
                        return redirect()->back()->with(['error' => 'Unable to fetch Image, Upload another image']);

                    $image_name = $image_uploader->store(env('PAYMENT_RECEIPT_PATH'));

                } else {
                    return redirect()->back()->with(['error' => 'We could not fetch Image data due to security reason, Try to upload another image']);
                }
            }

            StoreWalletRequest::create([
                'store_id' => \Session::get('store')->id,
                'payment_mode' => $request->payment_mode,
                'amount' => $request->amount,
                'reference_number' => $request->reference_number,
                'bank_name' => $request->bank_name,
                'deposited_at' => Carbon::parse($request->deposit_date . $request->deposit_time),
                'image' => $image_name
            ]);

            return redirect()->route('store-wallet-request-view')->with(['success' => 'New Request has been Submitted..!']);
        }

        return view('store-manager.wallet.request.create', [
            'payment_modes' => json_decode(\File::get(public_path('data/payment_mode.json')))
        ]);
    }

    public function commission(Request $request)
    {
        $transactions = StorePendingCommission::with(['sender_store','admin'])
            ->filterDate($request->dateRange)
            ->where('store_id', \Session::get('store')->id)->latest()->paginate(50);

        return view('store-manager.wallet.commission', [
            'transactions' => $transactions,
        ]);
    }

    public function pendingCommission(Request $request)
    {
        $transactions = StorePendingCommission::with(['sender_store','admin'])
            ->filterDate($request->dateRange)
            ->where('sender_store_id', \Session::get('store')->id)->latest()->paginate(50);

        return view('store-manager.wallet.pending-commission', [
            'transactions' => $transactions,
        ]);
    }

    public function updateCommission(Request $request)
    {
        if(!$store_commission = StorePendingCommission::whereId($request->id)->first())
            return redirect()->with(['error' => 'Unable to find commission details, please try again']);

        if($request->isMethod('post')){

            $this->validate($request, [
                'payment_mode' => 'required',
                'reference_number' => 'required',
                'bank_name' => 'required',
                'deposit_date' => 'required',
                'deposit_time' => 'required',
                'image' => 'nullable|image|mimes:jpeg,jpg,png|max:4000'
            ], [
                'payment_mode.required' => 'Payment Mode is Required',
                'reference_number.required' => 'Reference Number is Required',
                'bank_name.required' => 'Bank Name is Required',
                'deposit_date.required' => 'Deposit Date is Required',
                'deposit_time.required' => 'Deposit Time is Required',
            ]);

            $image_name = null;

            if($request->image)
            {
                if ($request->file('image')->isValid())
                {
                    $image_uploader = (new ImageUpload());
                    if(!$image_uploader->process($request->file('image'),ImageUpload::DOCUMENT_TYPE))
                        return redirect()->back()->with(['error' => 'Unable to fetch Image, Upload another image']);

                    $image_name = $image_uploader->store(env('PAYMENT_RECEIPT_PATH'));

                } else {
                    return redirect()->back()->with(['error' => 'We could not fetch Image data due to security reason, Try to upload another image']);
                }
            }

            $date = Carbon::parse($request->deposit_date)->toDateString().' '.Carbon::parse($request->deposit_time)->toTimeString();

            $payment_details = [
              'payment_mode' => $request->payment_mode,
              'reference_number' => $request->reference_number,
              'bank_name' => $request->bank_name,
              'date' => $date,
              'image' => $image_name,
            ];

            $store_commission->payment_details = $payment_details;
            $store_commission->status = StorePendingCommission::TRANSFERRED;
            $store_commission->save();

            return redirect()->route('store-wallet-pending-commission')->with(['success' => 'Commission details has been updated successfully']);

        }

        return view('store-manager.wallet.update-commission',[
           'store_commission' => $store_commission,
            'payment_modes' => json_decode(\File::get(public_path('data/payment_mode.json')))
        ]);

    }
}
