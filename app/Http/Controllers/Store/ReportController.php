<?php

namespace App\Http\Controllers\Store;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\ProductPrice;
use App\Models\StoreManager\StoreSupply;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function sales(Request $request)
    {
        if($request->dateRange) {
            $dates = explode('-', $request->dateRange);
            $start_at = Carbon::parse($dates[0])->startOfDay();
            $end_at = Carbon::parse($dates[1])->endOfDay();
        }
        else {
            $start_at = Carbon::now()->subDays(9)->startOfDay();
            $end_at = Carbon::now()->endOfDay();
        }

        $period = CarbonPeriod::create($start_at, $end_at);

        $orders =  Order::whereStoreId(\Session::get('store')->id)->selectRaw('sum(amount) as total_amount, DATE(created_at) as created_at, count(id) as order_count')->where([
            ['created_at', '>=', $start_at],
            ['created_at', '<=', $end_at],
        ])->groupBy(\DB::raw('DATE(created_at)'))->get();


        $records = collect($period)->map(function ($date) use ($orders, $request) {

            $day_turnover = collect($orders)->filter(function ($order) use ($date) {
                return $order->created_at->format('Y-m-d') == $date->format('Y-m-d');
            })->first();

            return (object) [
                'date' => Carbon::parse($date),
                'amount' => $day_turnover ? $day_turnover->total_amount : 0,
                'order_count' => $day_turnover ? $day_turnover->order_count : 0,
            ];

        })->sortByDesc('date')->values();

        if($request->download == 'yes')
        {
            $date_range = Carbon::parse($start_at)->format('d M Y') . ' - ' . Carbon::parse($end_at)->format('d M Y');

            \Excel::create('Sales Report of ' . $date_range, function($excel) use ($records, $request) {

                $excel->sheet('Sheet1', function($sheet) use ($records, $request) {

                    $data = $records->map( function($record) {
                        return [
                            'Date' => Carbon::parse($record->date)->format('M d, Y'),
                            'Amount' => number_format($record->amount),
                            'Orders' => number_format($record->order_count)
                        ];

                    })->toArray();

                    $sheet->fromArray($data);
                });

            })->download('xls');
        }

        return view('store-manager.reports.sales',[
            'records' => $records
        ]);
    }

    public function supplyTransfer(Request $request)
    {
        if($request->dateRange) {
            $dates = explode('-', $request->dateRange);
            $start_at = Carbon::parse($dates[0])->startOfDay();
            $end_at = Carbon::parse($dates[1])->endOfDay();
        }
        else {
            $start_at = Carbon::now()->subDays(9)->startOfDay();
            $end_at = Carbon::now()->endOfDay();
        }

        $period = CarbonPeriod::create($start_at, $end_at);

        $supplies = StoreSupply::whereSenderStoreId(\Session::get('store')->id)->selectRaw('sum(amount) as total_amount, DATE(created_at) as created_at, count(id) as supply_count')->where([
            ['created_at', '>=', $start_at],
            ['created_at', '<=', $end_at],
        ])->groupBy(\DB::raw('DATE(created_at)'))->get();

        $records = collect($period)->map(function ($date) use ($supplies, $request) {

            $day_turnover = collect($supplies)->filter(function ($supply) use ($date) {
                return $supply->created_at->format('Y-m-d') == $date->format('Y-m-d');
            })->first();

            return (object) [
                'date' => Carbon::parse($date),
                'amount' => $day_turnover ? $day_turnover->total_amount : 0,
                'supply_count' => $day_turnover ? $day_turnover->supply_count : 0,
            ];

        })->sortByDesc('date')->values();

        if($request->download == 'yes')
        {
            $date_range = Carbon::parse($start_at)->format('d M Y') . ' - ' . Carbon::parse($end_at)->format('d M Y');

            \Excel::create('Supply Report of ' . $date_range, function($excel) use ($records, $request) {

                $excel->sheet('Sheet1', function($sheet) use ($records, $request) {

                    $data = $records->map( function($record) {
                        return [
                            'Date' => Carbon::parse($record->date)->format('M d, Y'),
                            'Amount' => number_format($record->amount),
                            'Total Supplies' => number_format($record->supply_count)
                        ];

                    })->toArray();

                    $sheet->fromArray($data);
                });

            })->download('xls');
        }

        return view('store-manager.reports.supply-transfer',[
            'records' => $records
        ]);
    }

    public function productRanking(Request $request)
    {

        $orders = OrderDetail::select(\DB::raw('SUM(qty) as total, product_price_id'))->groupBy('product_price_id')->whereHas('order', function ($q) {
            $q->where('store_id', \Session::get('store')->id);
        })->filterDate($request->dateRange)->get();

        $items = ProductPrice::with(['product:id,name'])->get()->map( function ($product_price) use ($orders) {

            $sold_item = collect($orders)->filter( function ($order) use ($product_price) {
                return $order->product_price_id == $product_price->id;
            })->first();

            $product_price->sold_qty = $sold_item ? $sold_item->total : 0;
            return $product_price;

        })->sortByDesc( function ($item) {
            return $item->sold_qty;
        })->values();

        if($request->download == 'yes')
        {
            \Excel::create('Product Ranking Report' , function($excel) use ($items) {

                $excel->sheet('Sheet1', function($sheet) use ($items) {

                    $data = $items->map( function($item) {
                        return [
                            'Item' => $item->product->name,
                            'Product Code' =>  $item->code,
                            'Item Sold' =>  number_format($item->sold_qty)
                        ];

                    })->toArray();

                    $sheet->fromArray($data);
                });

            })->download('xls');

        }


        return view('store-manager.reports.product-ranking',[
            'items' => $items
        ]);
    }

}
