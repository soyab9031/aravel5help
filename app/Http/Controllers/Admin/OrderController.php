<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\Setting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Knp\Snappy\Pdf;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $orders = Order::with(['user.detail','admin'])->search($request->search, [
            'user.tracking_id', 'customer_order_id'
        ])->filterDate($request->dateRange, 'approved_at');

        if($request->order_from){
            if($request->order_from == 1)
                $orders = $orders->whereNotNull('store_id');
            else
                $orders = $orders->whereNull('store_id');
        }

        return view('admin.orders.index',[
            'orders' => $orders->latest()->paginate(30)
        ]);
    }

    public function details(Request $request)
    {
        $order = Order::with(['user.detail', 'details.product_price.product', 'admin', 'store'])
            ->whereCustomerOrderId($request->customer_order_id)->first();

        if (!$order)
            return redirect()->back()->with(['error' => 'Invalid Order Details']);

        if ($request->download) {

            $setting = Setting::where('name','Company Profile')->first();

            $filename =  $order->customer_order_id;

            $snappy = new Pdf(base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'));

            $snappy->generateFromHtml(
                view('admin.orders.invoice', [
                    'filename' => $filename, 'order' => $order, 'setting' => $setting
                ])->__toString(), storage_path("app/".$filename.".pdf"), [
                'orientation' => 'Portrait',
                'page-height' => 297,
                'page-width'  => 210,
            ], true);

            return response()->download(storage_path("app/".$filename.".pdf"))->deleteFileAfterSend(true);

        }

        if($request->isMethod('post')){
            $this->validate($request,[
                'status' => 'required',
            ]);

            if($request->status == Order::REJECTED){
                $order->status = $request->status;
                $order->admin_remarks = $request->admin_remarks;
                $order->save();

                return redirect()->route('admin-shopping-order-view')->with(['success' => 'Customer order rejected successfully']);

            }

            if($request->status == Order::APPROVED){

                $payment_details = [
                    'payment_mode' => $request->payment_mode,
                    'reference_number' => $request->reference_number ? : 'N.A'
                ];

                $order->admin_id = \Session::get('admin')->id;
                $order->status = Order::APPROVED;
                $order->approved_at = Carbon::now();
                $order->admin_remarks = $request->admin_remarks;
                $order->payment_status = Order::PAYMENT_SUCCESS;
                $order->payment_reference = $payment_details;
                $order->invoice_number = $this->AutoInvoiceIncrement();
                $order->save();

//                $current_month = now()->format("M Y");
//
////                $current_month_orders = Order::selectRaw("COALESCE(SUM(amount), 0) as total_amount")
////                    ->whereUserId($order->user_id)
////                    ->whereRaw("DATE_FORMAT(created_at, '%b %Y') in ('{$current_month}')")
////                    ->whereNotNull('approved_at')
////                    ->first();


                if(!$order->user->paid_at){

                    User::whereId($order->user_id)->update([
                        'paid_at' => $order->approved_at,
                    ]);
                }

                return redirect()->route('admin-shopping-order-view')->with(['success' => 'Customer order approved successfully']);
            }
        }

        return view('admin.orders.details', [
            'order' => $order,
            'payment_modes' => json_decode(\File::get(public_path('data/payment_mode.json'))),
        ]);
    }
    public function AutoInvoiceIncrement(){

        $temp = Order::where('created_at','>=', Carbon::parse('2021-09-01')->toDateTimeString())
            ->whereNotNull('admin_id')->orderBy('id','dese')->first();

        return  ($temp->invoice_number)+1;
    }
}
