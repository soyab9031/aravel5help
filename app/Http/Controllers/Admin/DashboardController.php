<?php

namespace App\Http\Controllers\Admin;

use App\Library\Helper;
use App\Models\ContactInquiry;
use App\Models\Order;
use App\Models\PackageOrder;
use App\Models\Payout;
use App\Models\Pin;
use App\Models\PinRequest;
use App\Models\Support;
use App\Models\User;
use App\Models\UserDocument;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $user_count = [
            'total' => User::count(),
            'active' => User::whereNotNull('paid_at')->count(),
            'inactive' => User::whereNull('paid_at')->count(),
            'today' => User::whereRaw('DATE(created_at) = "' . Carbon::now()->toDateString() . '"')->count()
        ];


        return view('admin.dashboard', [
            'user_count' => Helper::arrayToObject($user_count),
            'pins' => Pin::count(),
            'turnover' => $this->getTurnoverReport(),
            'chart_joined_users' => $this->userJoiningChart(),
            'pending_documents' => UserDocument::whereStatus(UserDocument::PENDING)->count(),
            'open_supports' => Support::whereNull('parent_id')->whereStatus(Support::OPEN)->count(),
            'contactInquiries' => ContactInquiry::whereStatus(ContactInquiry::PENDING)->count(),
            'pinsRequest' => PinRequest::whereStatus(PinRequest::PENDING)->count(),
            'bvs'=>$this->getBvOfLast7Days(),
        ]);
    }

    private function getBvOfLast7Days()
    {
        $bv = Order::selectRaw('sum(total_bv) as total_bvs, DATE(approved_at) as approved_at')->where([
            ['approved_at', '>=', Carbon::now()->subDays(6)->startOfDay()],
            ['approved_at', '<=', Carbon::now()->endOfDay()],
        ])->whereNotNull('approved_at')->groupBy(\DB::raw('DATE(approved_at)'))->get();

        $period = CarbonPeriod::create(Carbon::now()->subDays(6)->startOfDay(), Carbon::now()->endOfDay());

        return collect($period)->map(function ($date) use ($bv) {

            $day_turnover = collect($bv)->filter(function ($order) use ($date) {
                return Carbon::parse($order->approved_at)->format('Y-m-d') == Carbon::parse($date)->format('Y-m-d');
            })->first();

            return (object)[
                'date' => $date,
                'bv' => $day_turnover ? $day_turnover->total_bvs : 0,
            ];
        })->sortByDesc('date');
    }
    private function getTurnoverReport()
    {
        $orders =  Order::selectRaw('sum(amount) as total_amount, DATE(created_at) as created_at')->where([
            ['created_at', '>=', Carbon::now()->subDays(6)->startOfDay()],
            ['created_at', '<=', Carbon::now()->endOfDay()],
        ])->whereNotNull('approved_at')->groupBy(\DB::raw('DATE(created_at)'))->get();

        $period = CarbonPeriod::create(Carbon::now()->subDays(6)->startOfDay(), Carbon::now()->endOfDay());

        return collect($period)->map(function ($date) use ($orders) {

            $day_turnover = collect($orders)->filter(function ($order) use ($date) {
                return $order->created_at->format('Y-m-d') == $date->format('Y-m-d');
            })->first();

            return (object) [
                'date' => $date,
                'amount' => $day_turnover ? $day_turnover->total_amount : 0,
            ];
        })->sortByDesc('date');
    }

    private function userJoiningChart() {

        $users = User::selectRaw('count(*) as total, DATE(created_at) as created_at')->where([
            ['created_at', '>=', Carbon::now()->subDays(9)->startOfDay()],
            ['created_at', '<=', Carbon::now()->endOfDay()],
        ])->groupBy(\DB::raw('DATE(created_at)'))->get();

        $period = CarbonPeriod::create(Carbon::now()->subDays(9)->startOfDay(), Carbon::now()->endOfDay());

        return collect($period)->map(function ($date) use ($users) {

            $each_day_user = collect($users)->filter(function ($user) use ($date) {
                return $user->created_at->format('Y-m-d') == $date->format('Y-m-d');
            })->first();

            return (object) [
                'date' => $date,
                'user' => $each_day_user ? $each_day_user->total : 0,
            ];

        })->sortByDesc('date');

    }
}
