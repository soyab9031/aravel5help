<?php

namespace App\Http\Controllers\Admin\StoreManager;

use App\Library\Helper;
use App\Models\ProductPrice;
use App\Models\Setting;
use App\Models\StoreManager\PurchaseOrder;
use App\Models\StoreManager\PurchaseOrderDetail;
use App\Models\StoreManager\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Knp\Snappy\Pdf;

class PurchaseOrderController extends Controller
{
    public function index(Request $request)
    {
        $purchase_orders = PurchaseOrder::with(['supplier', 'admin'])->search($request->search, [
            'supplier.name'
        ])->filterDate($request->dateRange)->latest()->paginate(30);

        return view('admin.store-manager.purchase-order.index', [
            'purchase_orders' => $purchase_orders
        ]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $validator = \Validator::make($request->all(), [
                'supplier_id' => 'required|exists:suppliers,id',
                'delivery_days' => 'numeric',
            ], [
                'supplier_id.required' => 'Select Supplier to Create Purchase Order',
                'supplier_id.exists' => 'Invalid Supplier',
            ]);

            if ($validator->fails())
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);

            $form_request = Helper::arrayToObject($request->all());

            $order_items = collect($form_request->order_items)->filter(function ($order_item) {
                return $order_item->selected_qty > 0;
            });

            if (count($order_items) == 0)
                return response()->json(['status' => false, 'message' => 'Select at least one Item to Create Purchase Order']);

            $purchase_order = \DB::transaction(function () use ($form_request, $order_items) {

                $purchase_order = PurchaseOrder::create([
                    'admin_id' => \Session::get('admin')->id,
                    'supplier_id' => $form_request->supplier_id,
                    'delivery_days' => $form_request->delivery_days,
                    'amount' => $form_request->order_details->amount,
                    'shipping_amount' => 0,
                    'tax_amount' => $form_request->order_details->gst,
                    'total' => $form_request->order_details->total_amount,
                    'remarks' => $form_request->remarks
                ]);

                $purchase_order->customer_order_id = 'PO' . $purchase_order->created_at->format('Ymd') . $purchase_order->id;
                $purchase_order->save();

                collect($order_items)->map(function ($order_item) use ($purchase_order) {

                    PurchaseOrderDetail::create([
                        'order_id' => $purchase_order->id,
                        'product_name' => $order_item->name,
                        'note' => $order_item->note,
                        'qty' => $order_item->selected_qty,
                        'amount' => $order_item->purchase_price,
                        'gst_percentage' => $order_item->tax_percentage
                    ]);

                });

                return $purchase_order;

            });

            $request->session()->flash('success', 'New Purchase Order Generated: ' . $purchase_order->customer_order_id);

            return response()->json(['status' => true, 'message' => 'New Purchase Order Generated: ' . $purchase_order->customer_order_id, 'route' => route('admin-store-manager-purchase-order-view')]);

        }

        $items = ProductPrice::with(['product:id,name'])->latest()->get()->map(function ($item) {
            return [
                'id' => $item->id,
                'code' => $item->code,
                'name' => $item->product->name,
                'note' => null,
                'selected_qty' => 0,
                'tax_percentage' => 0,
                'purchase_price' => 0,
            ];
        });

        return view('admin.store-manager.purchase-order.create', [
            'suppliers' => Supplier::selectRaw('id as value, name as label, mobile, city')->active()->get(),
            'items' => $items
        ]);
    }

    public function update(Request $request)
    {
        if (!$purchase_order = PurchaseOrder::whereId($request->id)->with(['supplier:id,name,city'])->first())
            return redirect()->back()->with(['error' => 'Invalid Purchase Order Data, Try Again']);

        if ($request->isMethod('post'))
        {
            $validator = \Validator::make($request->all(), [
                'delivery_days' => 'numeric',
            ]);

            if ($validator->fails())
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);

            $form_request = Helper::arrayToObject($request->all());

            $order_items = collect($form_request->order_items)->filter(function ($order_item) {
                return $order_item->selected_qty > 0;
            });

            if (count($order_items) == 0)
                return response()->json(['status' => false, 'message' => 'Select at least one Item to Create Purchase Order']);

            \DB::transaction(function () use ($form_request, $order_items, $purchase_order) {

                $purchase_order->delivery_days = $form_request->delivery_days;
                $purchase_order->amount = $form_request->order_details->amount;
                $purchase_order->tax_amount = $form_request->order_details->gst;
                $purchase_order->total = $form_request->order_details->total_amount;
                $purchase_order->remarks = $form_request->remarks;
                $purchase_order->save();

                PurchaseOrderDetail::whereOrderId($purchase_order->id)->delete();

                collect($order_items)->map(function ($order_item) use ($purchase_order) {

                    PurchaseOrderDetail::create([
                        'order_id' => $purchase_order->id,
                        'product_name' => $order_item->name,
                        'note' => $order_item->note,
                        'qty' => $order_item->selected_qty,
                        'amount' => $order_item->purchase_price,
                        'gst_percentage' => $order_item->tax_percentage
                    ]);

                });

                return $purchase_order;

            });

            $request->session()->flash('success', 'Purchase Order Updated Successfully');

            return response()->json(['status' => true, 'message' => 'Purchase Order Updated Successfully', 'route' => route('admin-store-manager-purchase-order-view')]);
        }

        $items = ProductPrice::with(['product:id,name'])->latest()->get()->map(function ($item) {
            return [
                'id' => $item->id,
                'code' => $item->code,
                'name' => $item->product->name,
                'note' => null,
                'selected_qty' => 0,
                'tax_percentage' => 0,
                'purchase_price' => 0,
            ];
        });

        $order_items = collect($purchase_order->details)->map(function ($item) {
            return [
                'id' => null,
                'code' => 'N.A',
                'name' => $item->product_name,
                'note' => $item->note,
                'selected_qty' => $item->qty,
                'tax_percentage' => $item->gst_percentage,
                'purchase_price' => $item->amount,
            ];
        })->toArray();

        return view('admin.store-manager.purchase-order.update', [
            'purchase_order' => $purchase_order,
            'order_items' => $order_items,
            'items' => $items
        ]);
    }

    public function detail(Request $request)
    {
        if (!$purchase_order = PurchaseOrder::whereId($request->id)->with(['details'])->first())
            return redirect()->back()->with(['error' => 'Invalid Purchase Order Data, Try Again']);

        return view('admin.store-manager.purchase-order.detail', [
            'purchase_order' => $purchase_order
        ]);
    }

    public function download(Request $request)
    {
        if (!$purchase_order = PurchaseOrder::whereId($request->id)->with(['details', 'supplier'])->first())
            return redirect()->back()->with(['error' => 'Invalid Purchase Order Data, Try Again']);

        $company_profile = Setting::where('name','Company Profile')->first()->value;

        $filename =  $purchase_order->customer_order_id;

        $snappy = new Pdf(base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'));

        $snappy->generateFromHtml(
            view('admin.store-manager.purchase-order.download', [
                'filename' => $filename,
                'purchase_order' => $purchase_order,
                'company_profile' => $company_profile
            ])->__toString(), storage_path("app/".$filename.".pdf"), [
            'orientation' => 'Portrait',
            'page-height' => 297,
            'page-width'  => 210,
        ], true);

        return response()->download(storage_path("app/".$filename.".pdf"))->deleteFileAfterSend(true);

    }
}
