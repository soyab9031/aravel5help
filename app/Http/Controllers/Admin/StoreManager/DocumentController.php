<?php

namespace App\Http\Controllers\Admin\StoreManager;

use App\Models\StoreDocument;
use App\Models\StoreStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DocumentController extends Controller
{
//    public function index(Request $request)
//    {
//        $documents = StoreDocument::with(['store'])->search($request->search, [
//                'store.tracking_id', 'store.name'
//            ])->filterDate($request->dateRange);
//
//        if($request->status)
//            $documents->whereStatus($request->status);
//
//        if ($request->export)
//        {
//            if (empty($request->status) && empty($request->dateRange) && empty($request->package_id) && empty($request->search))
//                return redirect()->back()->with(['error' => 'At least one filter is required to export the report']);
//
//            \Excel::create('Store Document Details', function ($excel) use ($documents) {
//
//                $excel->sheet('Store Document Details' , function ($sheet) use ($documents) {
//
//                    $data = $documents->get()->map( function ($document) {
//
//                        return [
//                            'Created Date' => $document->created_at->format('M d, Y'),
//                            'Store' => $document->store->name.' ('.$document->store->tracking_id.')',
//                            'Type' => $document->type == 1 ? 'PANCARD' : ( $document->type == 2 ?  'AADHAR_CARD' : ($document->type == 3 ?  'GST_NUMBER' : ($document->type == 4 ?  'ELECTRICITY_BILL' : ( $document->type == 5 ?  'AADHAR_CARD' :  'FOOD_LICENCE')))),
//                            'Status' => $document->status == 1 ? 'Pending' : $document->status == 2 ? 'Verified' : 'Rejected',
//                            'Image' => env('DOCUMENT_IMAGE_URL').$document->image
//                        ];
//
//                    })->toArray();
//
//                    $sheet->fromArray($data);
//                });
//
//            })->download('xls');
//        }
//
//        return view('admin.store-manager.document.index',[
//            'documents' => $documents->orderBy('id','desc')->paginate(50)
//        ]);
//    }

    public function index(Request $request)
    {
        $documents = StoreDocument::with('store')->search($request->search,[
            'store.name', 'store.mobile', 'store.email', 'store.tracking_id'
        ])->filterDate($request->dateRange);

        if($request->type)
            $documents  =  $documents->where('type', $request->type);

        if($request->status)
            $documents->whereStatus($request->status);

        if ($request->export)
        {
            if (empty($request->status) && empty($request->dateRange) && empty($request->type) && empty($request->search))
                return redirect()->back()->with(['error' => 'At least one filter is required to export the report']);

            \Excel::create('Store Document Details', function ($excel) use ($documents) {

                $excel->sheet('Store Document Details' , function ($sheet) use ($documents) {

                    $data = $documents->get()->map( function ($document) {

                        return [
                            'Created Date' => $document->created_at->format('M d, Y'),
                            'Store' => $document->store->name.' ('.$document->store->tracking_id.')',
                            'Type' => $document->type == 1 ? 'PANCARD' : ( $document->type == 2 ?  'AADHAR_CARD' : ($document->type == 3 ?  'GST_NUMBER' : ($document->type == 4 ?  'ELECTRICITY_BILL' : ( $document->type == 5 ?  'AADHAR_CARD' :  'Other')))),
                            'Status' => $document->status == 1 ? 'Pending' : $document->status == 2 ? 'Verified' : 'Rejected',
                        ];

                    })->toArray();

                    $sheet->fromArray($data);
                });

            })->download('xls');
        }

        return view('admin.store-manager.store.documents.index',[
            'documents' => $documents->orderBy('id','desc')->paginate(100)
        ]);
    }


    public function update(Request $request)
    {
        $document = StoreDocument::with(['store'])->whereId($request->id)->first();

        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'status' => 'required',
            ],[
                'status.required' => 'Status is Required',
            ]);

            \DB::transaction(function () use ($document, $request) {

                if($request->status == StoreDocument::PENDING)
                    $status = StoreStatus::KYC_INPROGRESS;
                elseif($request->status == StoreDocument::VERIFIED)
                    $status = StoreStatus::KYC_VERIFIED;
                else
                    $status = StoreStatus::KYC_REJECTED;

                $document->status = $request->status;
                $document->remarks = $request->remarks;
                $document->save();

                if ($document->type == StoreDocument::PAN_CARD)
                {
                    StoreStatus::whereStoreId($document->store_id)->update(['pancard' => $status]);
                }

                if(in_array($document->type, [StoreDocument::VOTER_ID, StoreDocument::ELECTRICITY_BILL, StoreDocument::DRIVING_LICENCE, StoreDocument::PASSPORT]))
                    StoreStatus::whereStoreId($document->store_id)->update(['address_proof' => $status]);

                if ($document->type == StoreDocument::AADHAR_CARD)
                    StoreStatus::whereStoreId($document->user_id)->update(['aadhar_card' => $status]);

                if ($document->type == StoreDocument::GST_NUMBER)
                    StoreStatus::whereStoreId($document->store_id)->update(['gst' => $status]);

                if (in_array($document->type, [StoreDocument::BANK_PASSBOOK, StoreDocument::CANCEL_CHEQUE]))
                    StoreStatus::whereStoreId($document->store_id)->update(['bank_account' => $status]);

            });

            return redirect()->route('admin-store-manager-document-view')->with(['success' => 'Document Update has been successfully..']);

        }

        return view('admin.store-manager.store.documents.update', [
            'document' => $document
        ]);
    }
}
