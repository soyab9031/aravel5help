<?php

namespace App\Http\Controllers\Admin\StoreManager;

use App\Library\ImageUpload;
use App\Models\StoreManager\Store;
use App\Models\StoreManager\StoreWalletRequest;
use App\Models\StoreManager\StoreWalletTransaction;
use App\Models\StorePendingCommission;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoreWalletController extends Controller
{
    public function index(Request $request)
    {
        $transactions = StoreWalletTransaction::with(['store:id,name,city,tracking_id', 'admin:id,name'])
            ->where('status',StoreWalletTransaction::ACTIVE)
            ->filterDate($request->dateRange);

        if ($request->store_id) {
            $transactions = $transactions->whereStoreId($request->store_id);
        }

        return view('admin.store-manager.store.wallet.index', [
            'stores' => Store::select(['name', 'city', 'id'])->get(),
            'transactions' => $transactions->latest()->paginate(30)
        ]);
    }
    public function pendingCommission(Request $request)
    {
        $transactions = StorePendingCommission::with(['admin'])
            ->filterDate($request->dateRange)
            ->whereNotNull('admin_id')
            ->latest()->paginate(50);

        return view('admin.store-manager.store.wallet.pending-commission', [
            'transactions' => $transactions,
        ]);
    }
    public function updateCommission(Request $request)
    {
        if(!$store_commission = StorePendingCommission::whereId($request->id)->first())
            return redirect()->with(['error' => 'Unable to find commission details, please try again']);

        if($request->isMethod('post')){

            $this->validate($request, [
                'payment_mode' => 'required',
                'reference_number' => 'required',
                'bank_name' => 'required',
                'deposit_date' => 'required',
                'deposit_time' => 'required',
                'image' => 'nullable|image|mimes:jpeg,jpg,png|max:4000'
            ], [
                'payment_mode.required' => 'Payment Mode is Required',
                'reference_number.required' => 'Reference Number is Required',
                'bank_name.required' => 'Bank Name is Required',
                'deposit_date.required' => 'Deposit Date is Required',
                'deposit_time.required' => 'Deposit Time is Required',
            ]);

            $image_name = null;

            if($request->image)
            {
                if ($request->file('image')->isValid())
                {
                    $image_uploader = (new ImageUpload());
                    if(!$image_uploader->process($request->file('image'),ImageUpload::DOCUMENT_TYPE))
                        return redirect()->back()->with(['error' => 'Unable to fetch Image, Upload another image']);

                    $image_name = $image_uploader->store(env('PAYMENT_RECEIPT_PATH'));

                } else {
                    return redirect()->back()->with(['error' => 'We could not fetch Image data due to security reason, Try to upload another image']);
                }
            }

            $date = Carbon::parse($request->deposit_date)->toDateString().' '.Carbon::parse($request->deposit_time)->toTimeString();

            $payment_details = [
                'payment_mode' => $request->payment_mode,
                'reference_number' => $request->reference_number,
                'bank_name' => $request->bank_name,
                'date' => $date,
                'image' => $image_name,
            ];

            $store_commission->payment_details = $payment_details;
            $store_commission->status = StorePendingCommission::TRANSFERRED;
            $store_commission->save();

            return redirect()->route('admin-store-manager-store-pending-commission-report-view')->with(['success' => 'Commission details has been updated successfully']);

        }

        return view('admin.store-manager.store.wallet.update-commission',[
            'store_commission' => $store_commission,
            'payment_modes' => json_decode(\File::get(public_path('data/payment_mode.json')))

        ]);

    }

    public function createTransaction(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'store_id' => 'required|exists:stores,id',
                'type' => 'required',
                'amount' => 'required|numeric',
                'remarks' => 'required',
            ],[
                'store_id.required' => 'Store is required',
                'store_id.exists' => 'Invalid Store',
                'type.required' => 'Type is Required',
                'amount.required' => 'Amount is Required',
                'amount.numeric' => 'Amount is Must be Numeric',
                'remarks.required' => 'Remarks is Required'
            ]);

            $store = Store::whereId($request->store_id)->first();

            if ($request->type == StoreWalletTransaction::DEBIT_TYPE && $store->wallet_balance  == 0 )
                return redirect()->back()->with(['error' => 'Wallet cannot debited due to low balance.']);

            if($request->type == StoreWalletTransaction::CREDIT_TYPE)
            {
                $store->credit(collect([
                    'admin_id' => \Session::get('admin')->id,
                    'amount' => $request->amount,
                    'remarks' => $request->remarks,
                ]));
            }
            else
            {
                $store->debit(collect([
                    'admin_id' => \Session::get('admin')->id,
                    'amount' => $request->amount,
                    'remarks' => $request->remarks,
                ]));
            }

            return redirect()->route('admin-store-manager-store-wallet-view', ['store_id' => $store->id])->with(['success' => 'Wallet Transaction is Completed.']);
        }

        return view('admin.store-manager.store.wallet.transaction', [
            'stores' => Store::select(['name', 'tracking_id', 'id'])->get(),
        ]);
    }

    public function requestView(Request $request)
    {
        $wallet_requests = StoreWalletRequest::with(['store:id,name,city,tracking_id', 'admin:id,name'])->filterDate($request->dateRange);

        if ($request->store_id) {
            $wallet_requests = $wallet_requests->whereStoreId($request->store_id);
        }

        return view('admin.store-manager.store.wallet.request.index', [
            'stores' => Store::select(['name', 'city', 'id'])->get(),
            'wallet_requests' => $wallet_requests->latest()->paginate(50)
        ]);
    }

    public function requestUpdate(Request $request)
    {
        $wallet_request = StoreWalletRequest::with(['store:id,name,city,tracking_id', 'admin:id,name'])->whereId($request->id)->first();

        if($request->isMethod('post'))
        {
            if ($request->instant_transfer == 'YES' && $request->status == StoreWalletRequest::APPROVED) {

                $wallet = $wallet_request->store->credit(collect([
                    'admin_id' => \Session::get('admin')->id,
                    'amount' => $wallet_request->amount,
                    'remarks' => 'Credit Against Wallet Request - ' . $request->remarks,
                ]));

                $wallet_request->wallet_id = $wallet->id;
            }

            $wallet_request->admin_id = \Session::get('admin')->id;
            $wallet_request->status = $request->status;
            $wallet_request->remarks = $request->remarks;
            $wallet_request->save();

            return redirect()->route('admin-store-manager-wallet-request-view')->with(['success' => 'Store Wallet Request has been Updated..']);
        }

        return view('admin.store-manager.store.wallet.request.update', [
            'wallet_request' => $wallet_request
        ]);
    }

    public function commission(Request $request)
    {
        $transactions = StoreWalletTransaction::with(['store:id,name,city,tracking_id', 'admin:id,name'])
            ->where('status',StoreWalletTransaction::INACTIVE)
            ->filterDate($request->dateRange);

        if ($request->store_id) {
            $transactions = $transactions->whereStoreId($request->store_id);
        }

        return view('admin.store-manager.store.wallet.index', [
            'stores' => Store::select(['name', 'city', 'id'])->get(),
            'transactions' => $transactions->latest()->paginate(30)
        ]);
    }
}
