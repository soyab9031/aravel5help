<?php

namespace App\Http\Controllers\Admin\StoreManager;

use App\Library\Helper;
use App\Models\Setting;
use App\Models\StoreManager\StockTransaction;
use App\Models\StoreManager\Store;
use App\Models\StoreManager\StoreSupply;
use App\Models\StoreManager\StoreSupplyDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Knp\Snappy\Pdf;

class StoreSupplyController extends Controller
{
    public function index(Request $request)
    {
        $supplies = StoreSupply::with(['store:id,name,tracking_id', 'admin:id,name'])
            ->where('created_at','>=', Carbon::parse('2021-09-01')->toDateTimeString())
            ->whereNotNull('admin_id')->filterDate($request->dateRange)->latest()->paginate(30);

        return view('admin.store-manager.store.supply.index', [
            'stores' => Store::select(['name', 'city', 'id'])->get(),
            'supplies' => $supplies
        ]);
    }

    public function deliveryNote(Request $request)
    {
        if (!$supply = StoreSupply::with(['store', 'admin:id,name', 'details.product_price.product:id,name'])->whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Record, Try Again']);
       $gstNumber= Setting::selectRaw('value')->get();
        $company_profile = Setting::where('name','Company Profile')->first()->value;

        $filename =  $supply->admin_supply_id >= 17 ? $supply->admin_supply_id - 16 : $supply->admin_supply_id;

        $snappy = new Pdf(base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'));

        $snappy->generateFromHtml(
            view('admin.store-manager.store.supply.delivery-note', [
                'filename' => $filename,
                'supply' => $supply,
                'GST_Number' => collect($gstNumber[0]->value)->where('title','gst_no')->values()[0]->value,
                'company_profile' => collect($company_profile)
            ])->__toString(), storage_path("app/".$filename.".pdf"), [
            'orientation' => 'Portrait',
            'page-height' => 297,
            'page-width'  => 210,
        ], true);

        return response()->download(storage_path("app/".$filename.".pdf"))->deleteFileAfterSend(true);

    }

    public function create(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $validator = \Validator::make($request->all(), [
                'store_id' => 'required|exists:stores,id',
                'delivery_type' => 'required',
            ], [
                'store_id.required' => 'Select Store or warehouse to create Stock Supply',
                'store_id.exists' => 'Invalid Store or Warehouse',
            ]);

            if ($validator->fails())
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);

            $form_request = Helper::arrayToObject($request->all());

            $order_items = collect($form_request->order_items)->filter(function ($order_item) {
                return $order_item->selected_qty > 0;
            });

            if (count($order_items) == 0)
                return response()->json(['status' => false, 'message' => 'Select at least one Item to Create Stock Supply']);

            $supply = \DB::transaction(function () use ($form_request, $order_items) {

                $store = Store::whereId($form_request->store_id)->first();

                $supply = StoreSupply::create([
                    'admin_id' => \Session::get('admin')->id,
                    'store_id' => $store->id,
                    'delivery_type' => $form_request->delivery_type,
                    'amount' => $form_request->order_details->amount,
                    'total_supply_discount' => $form_request->order_details->total_supply_discount,
                    'tax_amount' => $form_request->order_details->gst,
                    'total' => $form_request->order_details->total_amount - $form_request->order_details->total_supply_discount,
                    'courier_name' => $form_request->courier_name,
                    'courier_docket_number' => $form_request->courier_docket_number,
                    'remarks' => $form_request->remarks
                ]);

                collect($order_items)->map(function ($order_item) use ($supply, $store,$form_request) {

                    StoreSupplyDetail::create([
                        'supply_id' => $supply->id,
                        'product_price_id' => $order_item->id,
                        'price' => $order_item->price,
                        'distributor_price' => $order_item->distributor_price,
                        'qty' => $order_item->selected_qty,
                        'gst' => json_encode([
                            'percentage' => $order_item->tax_percentage,
                            'code' => $order_item->tax_code,
                        ])
                    ]);

                    StockTransaction::create([
                        'admin_id' => \Session::get('admin')->id,
                        'supply_id' => $supply->id,
                        'store_id' => $supply->store_id,
                        'product_price_id' => $order_item->id,
                        'type' => StockTransaction::DEBIT,
                        'qty' => $order_item->selected_qty
                    ]);

                    $store->stockCredit(collect([
                        'admin_id' => \Session::get('admin')->id,
                        'supply_id' => $supply->id,
                        'product_price_id' => $order_item->id,
                        'qty' => $order_item->selected_qty
                    ]));

                });

                $store->debit(collect([
                    'admin_id' => \Session::get('admin')->id,
                    'amount' => $form_request->order_details->amount,
                    'remarks' => 'Debit towards Supply from Company #' . $supply->id
                ]));

                return $supply;

            });

            $request->session()->flash('success', 'New Supply Created to Store ' . $supply->store->name);

            return response()->json(['status' => true, 'route' => route('admin-store-manager-store-supply-detail', ['id' => $supply->id])]);

        }

        $balance_query = 'COALESCE(SUM(CASE WHEN type = 1 THEN qty END),0) - COALESCE(SUM(CASE WHEN type = 2 THEN qty END),0)';

        $items = StockTransaction::selectRaw($balance_query . ' as balance, product_price_id')
            ->groupBy('product_price_id')
            ->havingRaw($balance_query . '> 0')
            ->with(['product_price.product:id,name'])->get()->map(function ($transaction) {

                return [
                    'id' => $transaction->product_price->id,
                    'code' => $transaction->product_price->code,
                    'name' => $transaction->product_price->product->name,
                    'selected_qty' => 0,
                    'balance' => $transaction->balance,
                    'price' => $transaction->product_price->price,
                    'distributor_price' => $transaction->product_price->distributor_price,
                    'tax_percentage' => $transaction->product_price->gst->percentage,
                    'tax_code' => $transaction->product_price->gst->code,
                ];

            });

        return view('admin.store-manager.store.supply.create', [
            'stores' => Store::selectRaw('id as value, name as label, name, city, id, type')->whereIn('type', [Store::WAREHOUSE_TYPE, Store::MASTER_TYPE])->get(),
            'items' => $items
        ]);
    }

    public function detail(Request $request)
    {
        $supply = StoreSupply::with(['store', 'admin:id,name', 'details.product_price.product:id,name'])->whereId($request->id)->first();

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'delivery_type' => 'required',
                'status' => 'required',
            ],[
                'delivery_type.required' => 'Delivery Type is required field',
                'status.required' => 'Status is required field',
            ]);

            $supply->delivery_type = $request->delivery_type;
            $supply->courier_name = $request->courier_name;
            $supply->courier_docket_number = $request->courier_docket_number;
            $supply->remarks = $request->remarks;
            $supply->status = $request->status;
            $supply->save();

            return redirect()->route('admin-store-manager-store-supply-detail', ['id' => $supply->id ])->with(['success' => 'Update Successfully']);
        }
        return view('admin.store-manager.store.supply.detail', [
            'supply' => $supply,
        ]);
    }
}
