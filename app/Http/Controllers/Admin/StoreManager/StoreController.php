<?php

namespace App\Http\Controllers\Admin\StoreManager;

use App\Models\State;
use App\Models\StoreManager\Store;
use App\Models\StoreManager\StoreBank;
use App\Models\StoreStatus;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    public function index(Request $request)
    {
        $stores = Store::with(['user', 'state'])
            ->whereNotIn('tracking_id', ['ST679220','ST500398','MA411064', 'MI491459', 'MI468945', 'MI441734', 'MA301198', 'MI513871', 'MA527936', 'MI321302', 'MA285510'])
            ->search($request->search, [
            'name', 'tracking_id'
        ])->filterDate($request->dateRange);

        if($request->status) {
            $stores = $stores->whereStats($request->status);
        }

        return view('admin.store-manager.store.index', [
            'stores' => $stores->latest()->paginate(30)
        ]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $this->validate($request,[
                'name' => 'required',
                'user_id' => 'required_if:owner_type,1|exists:users,id',
                'reference_user_id' => 'exists:users,id',
                'password' => 'min:6|required_if:owner_type,2',
                'owner_type' => 'required',
                'type' => 'required',
                'address' => 'required',
                'city' => 'required',
                'state_id' => 'required',
                'pincode' => 'required|digits:6',
                'email' => 'nullable|email',
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10',
            ], [
                'name.required' => 'Franchisee Name is Required',
                'owner_type.required' => 'Please Select Store Owner Type',
                'user_id.required_if' => 'Select User for Member Type Store',
                'user_id.exists' => 'Invalid User, Enter Correct Tracking Id',
                'reference_user_id.exists' => 'Invalid Reference User, Enter Correct Tracking Id',
                'password.required_if' => 'Enter Password for Non Member Type Store',
                'type.required' => 'Please Select Store Type',
                'address.required' => 'Address is Required',
                'city.required' => 'City is Required',
                'state_id.required' => 'State is Required',
                'pincode.required' => 'Pincode is Required',
                'pincode.digits' => 'Pincode Must be 6 digit number',
                'email.email' => 'Invalid Email Address',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'password.min' => 'Password length should be 6 or more',
            ]);

            $user = null;
            $reference_user = null;
            if($request->owner_type == 1)
                $user = User::whereId($request->user_id)->first();

            if($user) {
                if(Store::whereUserId($user->id)->first())
                    return redirect()->back()->with(['error' => 'This User is already created for Store, try another']);
            }

            if($request->reference_user_id){
                $reference_user = User::whereId($request->reference_user_id)->first();
            }

            if($user && $reference_user){
                if($reference_user->id == $user->id)
                    return redirect()->back()->with(['error' => 'Store User and reference user not allow same']);
            }

            $type = $request->type == 1 ? 'MA' : ($request->type == 2 ? 'MI' : 'WH');

            $tracking_id = $type.rand(100000, 999999);

            while(Store::whereTrackingId($tracking_id)->exists()) {
                $tracking_id = $type.rand(100000, 999999);
            }

            $store = Store::create([
                'tracking_id' => $tracking_id,
                'name' => $request->name,
                'user_id' => $user ? $user->id : null,
                'reference_user_id' => $reference_user ? $reference_user->id : null,
                'password' => $user ? $user->password : $request->password,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'type' => $request->type,
                'address' => $request->address,
                'city' => $request->city,
                'pincode' => $request->pincode,
                'state_id' => $request->state_id,
            ]);

            StoreStatus::create([
                'store_id' => $store->id,
            ]);

            StoreBank::create([
                'store_id' => $store->id,
            ]);

            return redirect()->route('admin-store-manager-store-view')->with(['success' => 'New Store Created Successfully']);

        }

        return view('admin.store-manager.store.create', [
            'states' => State::get()
        ]);
    }

    public function update(Request $request)
    {
        if (!$store = Store::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Store Data']);

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'name' => 'required',
                'address' => 'required',
                'city' => 'required',
                'state_id' => 'required',
                'pincode' => 'required|digits:6',
                'email' => 'nullable|email',
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10',
            ], [
                'name.required' => 'Franchisee Name is Required',
                'address.required' => 'Address is Required',
                'city.required' => 'City is Required',
                'state_id.required' => 'State is Required',
                'pincode.required' => 'Pincode is Required',
                'pincode.digits' => 'Pincode Must be 6 digit number',
                'email.email' => 'Invalid Email Address',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
            ]);

            $store->name = $request->name;
            $store->password = !empty($request->password) ? $request->password : $store->password;
            $store->email = $request->email;
            $store->mobile = $request->mobile;
            $store->type = $request->type;
            $store->address = $request->address;
            $store->city = $request->city;
            $store->pincode = $request->pincode;
            $store->state_id = $request->state_id;
            $store->status = $request->status;
            $store->save();

            return redirect()->route('admin-store-manager-store-view')->with(['success' => 'Store is Updated Successfully']);

        }

        return view('admin.store-manager.store.update', [
            'states' => State::get(),
            'store' => $store
        ]);
    }

    public function accountAccess(Request $request)
    {
        if(!$store = Store::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Store Details for View']);

        return redirect()->route('store-admin-view-access', ['id' => $store->id, 'token' => \Session::get('admin')->token]);
    }

    public function apiGetStoreDetails(Request $request)
    {
        if (!$store = Store::whereId($request->store_id)->first())
            return response()->json(['status' => false, 'message' => 'Invalid Store request, try again']);

        return response()->json([
            'status' => true,
            'store' => [
                'id' => $store->id,
                'tracking_id' => $store->tracking_id,
                'name' => $store->name,
                'city' => $store->city,
                'type' => $store->type,
                'wallet_balance' => $store->wallet_balance
            ]
        ]);
    }

    public function stockList(Request $request)
    {
        return view('admin.store-manager.store.stock-list');
    }
}
