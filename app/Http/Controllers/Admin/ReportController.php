<?php

namespace App\Http\Controllers\Admin;

use App\Library\Helper;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Package;
use App\Models\PackageOrder;
use App\Models\Payout;
use App\Models\ProductPrice;
use App\Models\StoreManager\Store;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function topEarner(Request $request)
    {
        $payouts = Payout::with(['user.detail'])->selectRaw('sum(total) as gross_total, user_id')->groupBy('user_id')->search($request->search, [
            'user.tracking_id', 'user.username', 'user.mobile'
        ])->filterDate($request->dateRange)->orderby('gross_total', 'desc')->paginate(30);

        return view('admin.reports.top-earners', [
            'payouts' => $payouts
        ]);
    }

    public function sales(Request $request)
    {

        if($request->dateRange) {

            $dates = explode('-', $request->dateRange);

            $start_at = Carbon::parse($dates[0])->startOfDay();
            $end_at = Carbon::parse($dates[1])->endOfDay();
        }
        else {

            $start_at = Carbon::now()->subDays(15)->startOfDay();
            $end_at = Carbon::now()->endOfDay();

        }

        $period = CarbonPeriod::create($start_at, $end_at);

        $package_orders =  Order::selectRaw('sum(amount) as total_amount, DATE(created_at) as created_at')->where([
            ['created_at', '>=', $start_at],
            ['created_at', '<=', $end_at],
        ])->whereNotNull('approved_at')->groupBy(\DB::raw('DATE(created_at)'))->get();


        $data = collect($period)->map(function ($date) use ($package_orders, $request) {

            $day_turnover = collect($package_orders)->filter(function ($order) use ($date) {
                return $order->created_at->format('Y-m-d') == $date->format('Y-m-d');
            })->first();

            return (object) [
                'date' => $date,
                'amount' => $day_turnover ? $day_turnover->total_amount : 0,
            ];

        })->sortByDesc('date');

        return view('admin.reports.sales',[
            'turnover' => $data,
        ]);
    }
    public function bv(Request $request)
    {

        if($request->dateRange) {

            $dates = explode('-', $request->dateRange);

            $start_at = Carbon::parse($dates[0])->startOfDay();
            $end_at = Carbon::parse($dates[1])->endOfDay();
        }
        else {

            $start_at = Carbon::now()->subDays(15)->startOfDay();
            $end_at = Carbon::now()->endOfDay();

        }

        $period = CarbonPeriod::create($start_at, $end_at);

        $package_orders =  Order::selectRaw('sum(total_bv) as total_bvs, DATE(approved_at) as approved_at')->where([
            ['approved_at', '>=', $start_at],
            ['approved_at', '<=', $end_at],
        ])->whereNotNull('approved_at')->groupBy(\DB::raw('DATE(approved_at)'))->get();


        $data = collect($period)->map(function ($date) use ($package_orders, $request) {

            $day_As_per_BV = collect($package_orders)->filter(function ($order) use ($date) {
//                return $order->approved_at->format('Y-m-d') == $date->format('Y-m-d');
                return Carbon::parse($order->approved_at)->format('Y-m-d') == Carbon::parse($date)->format('Y-m-d');
            })->first();

            return (object) [
                'date' => $date,
                'bv' => $day_As_per_BV ? $day_As_per_BV->total_bvs : 0,
            ];

        })->sortByDesc('date');

        return view('admin.reports.bv',[
            'allBv' => $data,
        ]);
    }

    public function productRanking(Request $request)
    {

        $orders = PackageOrder::select(\DB::raw('COUNT(id) as total, package_id'))->groupBy('package_id')->filterDate($request->dateRange)->get();

        $packages = Package::get()->map( function ($package) use ($orders) {

            $sold_package = collect($orders)->filter( function ($order) use ($package) {
                return $order->package_id == $package->id;
            })->first();

            $package->sold_qty = $sold_package ? $sold_package->total : 0;
            return $package;

        })->sortByDesc( function ($package) {
            return $package->sold_qty;
        })->values();

        return redirect()->back();

        return view('admin.reports.package-ranking',[
            'packages' => $packages
        ]);
    }

    public function gstReport(Request $request)
    {
        $records = [];

        if ($request->month_name) {

            if(!$request->tax_area)
                return redirect()->back()->with(['error' => 'please select text area before search']);

            $records = OrderDetail::with('order.user.address')->whereHas('order', function ($q) use ($request) {
                $q->whereRaw("DATE_FORMAT(approved_at, '%b %Y') = '" . $request->month_name . "'")
                    ->whereStatus(Order::APPROVED)
                    ->whereNotNull('approved_at');
            });

            if ($request->tax_area == 1) {
                $records = $records->whereHas('order.user.address', function ($q) {
                    $q->where('state_id', 34);
                })->selectRaw('SUM(qty), COALESCE(SUM(qty*distributor_price), 0) as total_selling_price,id,gst,order_id');
            } else {
                $records = $records->whereHas('order.user.address', function ($q) {
                    $q->where('state_id', '<>', 34);
                })->selectRaw('SUM(qty), COALESCE(SUM(qty*distributor_price), 0) as total_selling_price,id,gst,order_id');
            }


            if($request->order_from){
                if($request->order_from == 1){
                    $records = $records->whereHas('order' ,function($q){
                        $q->whereNotNull('admin_id');
                    });
                }
                else{
                    $records = $records->whereHas('order' ,function($q) use ($request){
                        $q->where('store_id', $request->order_from);
                    });
                }
            }

            $records = $records->groupBy(['gst->code'])->get()->map(function ($record) use ($request) {

                if ($record->order->user->address->state_id == 34) {
                    $tax_amount = round(($record->total_selling_price * $record->gst->percentage) / ((float)100 + (float)$record->gst->percentage), 4);
                    $SGST_amount = $tax_amount / 2;
                    $CGST_amount = $tax_amount / 2;
                    $IGST_amount = 0;
                    $taxable_amount = $record->total_selling_price - $tax_amount;
                } else {
                    $tax_amount = round(($record->total_selling_price * $record->gst->percentage) / ((float)100 + (float)$record->gst->percentage), 4);
                    $SGST_amount = 0;
                    $CGST_amount = 0;
                    $IGST_amount = $tax_amount ? $tax_amount : 0;
                    $taxable_amount = $record->total_selling_price - $tax_amount;

                }

                return (object)[
                    'hsn_code' => $record->gst->code,
                    'percentage' => $record->gst->percentage,
                    'taxable_amount' => $taxable_amount,
                    'sgst_amount' => $SGST_amount,
                    'cgst_amount' => $CGST_amount,
                    'igst_amount' => $IGST_amount ? $IGST_amount : 0,
                    'total_tax_amount' => $tax_amount ? $tax_amount : 0,
                    'total_amount' => $record->total_selling_price,
                ];
            })->sortBy('hsn_code')->values();

            if ($request->download == 'yes') {
                ob_end_clean();
                ob_start();
                \Excel::create($request->month_name . ' GST Report with HSN Code', function ($excel) use ($records, $request) {

                    $excel->sheet('Sheet1', function ($sheet) use ($records, $request) {

                        $data = $records->map(function ($record, $index) {

                            return [
                                '#' => ($index + 1),
                                'HSN Code' => $record->hsn_code,
                                'GST Rate' => $record->percentage . '%',
                                'Taxable Amount' => number_format($record->taxable_amount),
                                'SGST' => number_format($record->sgst_amount),
                                'CGST' => number_format($record->cgst_amount),
                                'IGST' => number_format($record->igst_amount),
                                'Total Tax' => number_format($record->total_tax_amount),
                                'Total Amount' => number_format($record->total_amount),
                            ];
                        })->toArray();
                        $sheet->fromArray($data);
                    });

                })->download('xls');
            }
        }

        return view('admin.reports.tax-report', [
            'records' => $records,
            'month_names' => Helper::monthRange(Carbon::parse('2021-09-01')->startOfDay(), Carbon::now()),
            'stores' => Store::selectRaw('id, name, tracking_id, type')->whereNotIn('tracking_id', ['ST679220','ST500398','MA411064', 'MI491459', 'MI468945', 'MI441734', 'MA301198', 'MI513871', 'MA527936', 'MI321302', 'MA285510'])->get()
        ]);
    }
}
