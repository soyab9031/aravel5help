<?php

namespace App\Http\Controllers\Admin;

use App\Models\Package;
use App\Models\PackageItem;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PackageController extends Controller
{
    public function index()
    {

        $packages = Package::orderBy('id','desc')->get();
        return view('admin.package.index', ['packages' => $packages]);
    }

    public function create(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'name' => 'required|unique:packages,name',
                'amount' => 'required|numeric',
                'capping' => 'required|numeric',
                'sponsor_income' => 'numeric|nullable',
                'pv' => 'required|numeric',
                'declaration' => 'required',
                'status' => 'required'
            ],[
                'name.required' => 'Name is Required',
                'name.unique' => 'Package Name must be unique',
                'amount.required' => 'Amount is Required',
                'amount.numeric' => 'Amount is Must be Numeric',
                'capping.required' => 'Capping is Required',
                'capping.numeric' => 'Capping is Must be Numeric',
                'sponsor_income.numeric' => 'Sponsor Income is Must be Numeric',
                'pv.required' => 'PV is Required',
                'pv.numeric' => 'PV is Must be Numeric',
                'declaration.required' => 'Declaration is Required',
                'status.required' => 'Status is Required'
            ]);

            Package::create([
                'name' => $request->name,
                'prefix' => str_slug($request->name, '-'),
                'amount' => $request->amount,
                'capping' => $request->capping,
                'sponsor_income' =>$request->sponsor_income,
                'pv' => $request->pv,
                'declaration' => $request->declaration,
                'status' => $request->status
            ]);

            return redirect()->route('admin-package-view')->with(['success' => 'New Package has been created..!!']);
        }
        return view('admin.package.create');
    }


    public function update(Request $request, $id)
    {
        if($request->isMethod('post')){

            $package = Package::find($id);

            $this->validate($request,[
                'name' => 'required|unique:packages,name,' . $package->id,
                'amount' => 'required|numeric',
                'capping' => 'required|numeric',
                'declaration' => 'required',
                'status' => 'required'
            ],[
                'name.required' => 'Name is Required',
                'amount.required' => 'Amount is Required',
                'amount.numeric' => 'Amount is Must be Numeric',
                'capping.required' => 'Capping is Required',
                'capping.numeric' => 'Capping is Must be Numeric',
                'declaration.required' => 'Declaration is Required',
                'status.required' => 'Status is Required'
            ]);

            $package->name = $request->name;
            $package->prefix = str_slug($request->name, '-');
            $package->amount = $request->amount;
            $package->declaration = $request->declaration;
            $package->status = $request->status;
            $package->save();

            return redirect()->route('admin-package-view')->with(['success' => 'Package has been updated..!!']);
        }

        $package = Package::whereId($id)->first();

        return view('admin.package.update', ['package' => $package]);
    }

    public function items(Request $request)
    {
        $package = Package::with(['items'])->whereId($request->id)->first();

        if ($request->delete_item_id) {

            if (!$package_item = PackageItem::whereId($request->delete_item_id)->first())
                return redirect()->back()->with(['error' => 'Invalid Item.']);

            $package_item->delete();

            return redirect()->route('admin-package-items-view', ['id' => $package_item->package_id])
                ->with(['success' => 'Item has been deleted successfully']);
        }

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'item_name' => 'required',
                'item_amount' => 'required|numeric',
                'gst_rate' => 'required',
                'hsn_code' => 'required'
            ], [
                'item_name.required' => 'Package Item name is required field',
                'item_amount.required' => 'Package Item amount is required field',
                'gst_rate_id.required' => 'Gst Rate is required field',
                'item_amount.numeric' => 'Package Item amount should be in numeric',
                'hsn_code.required' => 'HSN Code or SAC Code is required'
            ]);

            PackageItem::create([
                'package_id' => $package->id,
                'name' => $request->item_name,
                'amount' => $request->item_amount,
                'gst' => json_encode([
                    'percentage' => $request->gst_rate, 'code' => $request->hsn_code
                ])
            ]);

            return redirect()->route('admin-package-items-view', ['id' => $package->id])->with(['success' => 'New Item has been added into ' . $package->name]);
        }

        return view('admin.package.items.index', [
            'package' => $package
        ]);
    }

    public function itemUpdate(Request $request)
    {
        $package_item = PackageItem::with(['package'])->whereId($request->id)->first();

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'item_name' => 'required',
                'item_amount' => 'required|numeric',
                'gst_rate' => 'required',
                'hsn_code' => 'required'
            ], [
                'item_name.required' => 'Package Item name is required field',
                'item_amount.required' => 'Package Item amount is required field',
                'gst_rate_id.required' => 'Gst Rate is required field',
                'item_amount.numeric' => 'Package Item amount should be in numeric',
                'hsn_code.required' => 'HSN Code or SAC Code is required'
            ]);

            $package_item->name = $request->item_name;
            $package_item->amount = $request->item_amount;
            $package_item->gst = json_encode([
                'percentage' => $request->gst_rate, 'code' => $request->hsn_code
            ]);
            $package_item->save();

            return redirect()->route('admin-package-items-view', ['id' => $package_item->id])->with(['success' => 'Item has been updated successfully']);

        }
        return view('admin.package.items.update', [
            'package_item' => $package_item
        ]);
    }


}
