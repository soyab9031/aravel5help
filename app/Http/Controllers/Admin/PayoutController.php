<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\Payout;
use App\Models\UserStatus;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PayoutController extends Controller
{

    public function index(Request $request)
    {
        $payouts = Payout::whereProcessType(Payout::ADMIN_PROCESS)->with(['user.detail', 'user.bank'])->search($request->search, [
            'user.tracking_id', 'user.mobile', 'user.username', 'user.detail.first_name', 'user.detail.last_name', 'reference_number'
        ])->where('created_at','>=', Carbon::parse('2021-08-31')->toDateString())
            ->filterDate($request->dateRange);

        if ($request->status)
            $payouts = $payouts->where('status', $request->status);

        if ($request->download)
        {
            if (empty($request->status) && empty($request->dateRange))
                return redirect()->back()->with(['error' => 'At least one filter is required to export the Payout report']);

            \Excel::create('Payout Report', function ($excel) use ($payouts) {

                $excel->sheet('Sheet 1', function ($sheet) use ($payouts) {

                    $data = $payouts->get()->map( function ($payout) {

                        return [
                            'index_number' => $payout->id,
                            'Created Date' => $payout->created_at->format('M d, Y'),
                            'Member Name' => $payout->user->detail->full_name,
                            'Tracking Id' => $payout->user->tracking_id,
                            'Contact Number' => $payout->user->mobile,
                            'Bank Name' => $payout->user->bank ? $payout->user->bank->bank_name : null,
                            'Account Number' => $payout->user->bank ? $payout->user->bank->account_number : null,
                            'IFSC Code' => $payout->user->bank ? $payout->user->bank->ifsc : null,
                            'Bank Address' => $payout->user->bank ? $payout->user->bank->address : null,
                            'Total Amount' => $payout->total,
                            'TDS' => $payout->tds,
                            'Admin Charge' => $payout->admin_charge,
                            'Net Amount' => $payout->amount,
                            'reference_number' => $payout->reference_number,
                            'remarks' => $payout->remarks
                        ];

                    })->toArray();

                    // manipulate the range of cells //
                    $sheet->cells('A1', function($cells) {
                        $cells->setBackground('#EA2C54'); $cells->setFontColor('#ffffff');
                    });

                    $sheet->cells('N1:O1', function($cells) {
                        $cells->setBackground('#EA2C54'); $cells->setFontColor('#ffffff');
                    });

                    $sheet->fromArray($data);

                });

            })->download('xls');
        }

        return view('admin.payout.index', [
            'payouts' => $payouts->orderBy('id', 'desc')->paginate(50)
        ]);
    }

    public function update(Request $request)
    {
        if (!$payout = Payout::with(['user.detail', 'user.bank'])->whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Payout Data']);

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'reference_number' => 'required',
            ], [
                'reference_number.required' => 'Transfer Reference Number is required field'
            ]);

            $payout->reference_number = $request->reference_number;
            $payout->status = $request->status;
            $payout->transfer_type = $request->transfer_type;
            $payout->remarks = $request->remarks;
            $payout->save();

            return redirect()->route('admin-payout-view')->with(['success' => 'Payout Details has been updated']);
        }

        return view('admin.payout.update', [
            'payout' => $payout
        ]);
    }

    public function bulkUpdate(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'csv_file'=>'required'
            ],[
                'csv_file.required'=>'CSV file is required'
            ]);

            $file_name = 'payout_'.Carbon::now()->toDateString().'.csv';

            \Storage::disk('public')->put('payout/'.$file_name, file_get_contents($request->file('csv_file')->path()) );

            $sheet_records = \Excel::load(storage_path('app/public/payout/'.$file_name))->get();

            collect($sheet_records)->filter(function ($row) {
                return $row->reference_number;
            })->values()->map(function ($row) {

                Payout::whereId($row->index_number)->update([
                    'reference_number' => $row->reference_number, 'remarks' => $row->remarks, 'status' => Payout::TRANSFERRED_TYPE, 'transfer_type' => Payout::NEFT_TYPE
                ]);

            });

            return redirect()->route('admin-payout-view')->with(['success' => 'Payout Bulk Records are updated successfully']);
        }

        return view('admin.payout.bulk-update');
    }

    public function create(Request $request)
    {
        $wallet_records = Wallet::with(['user.detail'])->whereHas('user.status', function ($q) {
            $q->where('payment', UserStatus::YES)->where('terminated', UserStatus::NO)->where('fake', UserStatus::REAL_TYPE);
        })->whereHas('user', function ($q) {
            $q->whereNotNull('paid_at');
        })->whereNull('payout_id')->get()->filter( function($wallet_record){

            $previous_month = now()->subMonthNoOverflow()->format("M Y");

            $order =  Order::whereRaw("DATE_FORMAT(created_at, '%b %Y') in ('{$previous_month}')")
                ->where('user_id', $wallet_record->user_id)->where('status', Order::APPROVED)->exists();

            if(!$order)
                return false;

            return $wallet_record;
        });



        $wallet_records = collect($wallet_records)->groupBy('user_id');

        if ($request->isMethod('post'))
        {
            $calculations = $this->calculation($wallet_records);

            \DB::transaction( function () use ($calculations) {

                collect($calculations)->map( function ($calculation) {

                    $payout = Payout::create([
                        'user_id' => $calculation->user->id,
                        'total' => $calculation->total,
                        'tds' => $calculation->tds,
                        'admin_charge' => $calculation->admin_charge,
                        'amount' => $calculation->amount,
                        'process_type' => Payout::ADMIN_PROCESS,
                        'created_at' => Carbon::now()->subMonth()->endOfMonth()->toDateTimeString()
                    ]);

                    Wallet::create([
                        'user_id' => $calculation->user->id,
                        'payout_id' => $payout->id,
                        'amount' => $calculation->amount,
                        'type' => Wallet::DEBIT_TYPE,
                        'remarks' => 'Generated Payout at '. $payout->created_at->format('D d-m-Y'),
                        'created_at'  => Carbon::now()->subMonth()->endOfMonth()->toDateTimeString()
                    ]);

                    Wallet::whereIn('id', $calculation->wallet_ids)->update(['payout_id' => $payout->id]);

                });

            });

            return redirect()->route('admin-payout-view')->with(['success' => 'Payout has been successfully generated']);
        }

        return view('admin.payout.create', [
            'payouts' => $this->calculation($wallet_records)
        ]);
    }

    private function calculation($wallet_records)
    {
        return $wallet_records->map( function ($wallets) {

            $wallet_credits = collect($wallets)->filter( function ($wallet) { return $wallet->type == Wallet::CREDIT_TYPE;});

            $wallet_debits = collect($wallets)->filter( function ($wallet) { return $wallet->type == Wallet::DEBIT_TYPE;});

            /* Total Values */
            $total = $wallet_credits->sum('total');
            $tds = $wallet_credits->sum('tds');
            $admin_charge = $wallet_credits->sum('admin_charge');

            $wallet_ids = collect($wallets)->pluck('id')->toArray();

            return (object) [
                'wallet_ids' => $wallet_ids,
                'user' => $wallets->first()->user,
                'total' => $total -  collect($wallet_debits)->sum('total'),
                'tds' => $tds - collect($wallet_debits)->sum('tds'),
                'admin_charge' => $admin_charge - collect($wallet_debits)->sum('admin_charge'),
                'amount' => $total - ( (collect($wallet_debits)->sum('amount')) + $tds + $admin_charge)
            ];

        })->values();
    }

    public function tdsReport(Request $request)
    {
        $tdsReport = Payout::where('created_at','>=', Carbon::parse('2021-08-31'))->selectRaw('COALESCE(sum(total), 0) as total_payout, COALESCE(sum(tds), 0) as total_tds, user_id')->search($request->search, [
            'user.tracking_id'
        ])->groupBy('user_id')->with(['user.detail:user_id,title,first_name,last_name,pan_no'])->havingRaw('SUM(tds) > 0')->filterDate($request->dateRange);

        if($request->status)
            $tdsReport = $tdsReport->where('status', $request->status);

        if ($request->download)
        {
            \Excel::create('Total TDS Report', function ($excel) use ($tdsReport,$request) {

                $excel->sheet('Sheet 1', function ($sheet) use ($tdsReport, $request) {

                    if(!empty($request->dateRange) || !empty($request->search)) {
                        $tdsReport = $tdsReport->orderBy('created_at','desc')->get();
                    }
                    else {
                        $tdsReport = $tdsReport->orderBy('id', 'desc')->paginate(50);
                    }

                    $data = $tdsReport->map( function ($tdsReport) {

                        return [
                            'Member Name' => $tdsReport->user->detail->full_name,
                            'Tracking Id' => $tdsReport->user->tracking_id,
                            'PAN Number' => $tdsReport->user->detail->pan_no ? $tdsReport->user->detail->pan_no : 'N.A',
                            'Payout' => $tdsReport->total_payout,
                            'TDS' => $tdsReport->total_tds,
                        ];

                    })->toArray();

                    $sheet->fromArray($data);
                });

            })->download('xls');
        }

        return view('admin.payout.tds-report',[
            'tdsReports' => $tdsReport->paginate(50)
        ]);
    }
}
