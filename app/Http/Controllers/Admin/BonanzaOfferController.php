<?php

namespace App\Http\Controllers\Admin;

use App\Models\BonanzaOffer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BonanzaOfferController extends Controller
{
    public function index(Request $request)
    {
        $bonanza_offers = BonanzaOffer::orderby('id','desc');


        return view('admin.offer.index',[
            'bonanza_offers' => $bonanza_offers->paginate(30)
        ]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {

            $this->validate($request, [
                'name' => 'required',
                'lft' => 'required',
                'rgt' => 'required',
                'start_at' => 'required',
                'end_at' => 'required',
            ], [
                'name:required' => 'Name Is Required',
                'lft:required' => 'lft Of Offer Is Required',
                'rgt:required' => 'rgt Of Offer Is Required',
                'start_at.required' => 'Start Date is Required',
                'end_at.required' => 'End Date is Required',
            ]);

            \DB::transaction(function () use ($request) {

                BonanzaOffer::create([
                    'name' => $request->name,
                    'start_at' => Carbon::parse($request->start_at)->toDateString(),
                    'end_at' => Carbon::parse($request->end_at)->toDateString(),
                    'lft' => $request->lft,
                    'rgt' => $request->rgt,
                    'status' => $request->status
                ]);
            });

            return redirect()->route('admin-bonanza-offer-view')->with(['success' => 'Your Offer Created Successfully']);
        }

        return view('admin.offer.create');
    }

    public function update(Request $request)
    {
        if(!$bonanza_offer = BonanzaOffer::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Offer Not Found please try again']);


        if ($request->isMethod('post')) {

            $this->validate($request, [
                'name' => 'required',
//                'lft' => 'required',
//                'rgt' => 'required',
//                'start_at' => 'required',
//                'end_at' => 'required',
            ], [
                'name:required' => 'Name Is Required',
//                'lft:required' => 'lft Of Offer Is Required',
//                'rgt:required' => 'rgt Of Offer Is Required',
//                'start_at.required' => 'Start Date is Required',
//                'end_at.required' => 'End Date is Required',
            ]);


            $bonanza_offer->name = $request->name;
//            $bonanza_offer->start_at = Carbon::parse($request->start_at)->toDateString();
//            $bonanza_offer->end_at = Carbon::parse($request->end_at)->toDateString();
            $bonanza_offer->status = $request->status;
            $bonanza_offer->save();

            return redirect()->route('admin-bonanza-offer-view')->with(['success' => 'Your Offer Record Updated Successfully']);
        }

        return view('admin.offer.update',[
            'bonanza_offer' => $bonanza_offer
        ]);
    }


}
