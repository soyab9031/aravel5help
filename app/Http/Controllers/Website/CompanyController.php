<?php

namespace App\Http\Controllers\Website;

use App\Models\Faq;
use App\Models\Gallery;
use App\Models\GalleryItem;
use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    public function about()
    {
        return view('website.about');
    }

    public function legals()
    {
        return view('website.legals');
    }

    public function vision_mission()
    {
        return view('website.vision-mission');
    }

    public function terms()
    {
        return view('website.terms');
    }

    public function gallery(){

        $galleries = Gallery::with('items')->whereStatus(Gallery::ACTIVE)->get();

        return view('website.gallery',[
            'galleries' => $galleries
        ]);
    }

    public function galleryItems(Request $request){

        if(!$gallery = Gallery::with('items')->where('id', $request->id)->first())
            return redirect()->route('website-gallery')->with(['error' => 'Unable to fine gallery ']);

        return view('website.gallery-items',[
            'gallery' => $gallery
        ]);
    }

    public function news(){

        $news = News::whereStatus(News::Active)->orderBy('id','desc')->get();

        return view('website.news',[
            'news' => $news
        ]);
    }

    public function faqs(){

        $faqs = Faq::whereStatus(Faq::ACTIVE)->orderBy('id','desc')->get();
        return view('website.faqs',[
            'faqs' => $faqs
        ]);
    }
}
