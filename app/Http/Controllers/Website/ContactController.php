<?php

namespace App\Http\Controllers\Website;

use App\Models\ContactInquiry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email',
                'contact' => 'required|regex:/^[987][0-9]{9}$/|digits:10',
                'title' => 'required',
                'message' => 'required'
            ],[
                'name.required' => 'Name is Required',
                'email.required' => 'Email is Required',
                'email.email' => 'Invalid Email',
                'contact.required' => 'Mobile Number is Required',
                'contact.regex' => 'Invalid Mobile Number',
                'contact.digits' => 'Mobile Number must be 10 digits',
                'title.required' => 'Subject is Required',
                'message.required' => 'Message is Required'
            ]);

            ContactInquiry::create([
                'name' => $request->name,
                'email' => $request->email,
                'contact' => $request->contact,
                'message' => $request->message,
                'title' => $request->title
            ]);

            return redirect()->route('website-contact')->with(['success' => 'Contact Request has been Sent, We will contact you soon..!!']);
        }

        return view('website.contact');

    }
}
