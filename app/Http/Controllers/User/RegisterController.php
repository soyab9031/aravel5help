<?php

namespace App\Http\Controllers\User;

use App\Library\Calculation\Binary\Helper;
use App\Models\Country;
use App\Models\Package;
use App\Models\Pin;
use App\Models\State;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserBank;
use App\Models\UserDetail;
use App\Models\UserStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{

    public function index(Request $request)
    {
        \Session::forget('user_registration');

        $sponsorUser = null;
        $leg = null;

        if (isset($request->referral))
        {
            if (!$sponsorUser = User::whereTrackingId($request->referral)->first()) {
                return redirect()->route('user-register')->with(['error' => 'Invalid Referral Upline Data']);
            }

            if (isset($request->leg)) {

                $leg = $request->leg;

                if (!in_array($leg, ['L', 'R'])) {
                    return redirect()->route('user-register')->with(['error' => 'Invalid Position Under Parent']);
                }
            }

        }

        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'tracking_id' => 'required|exists:users,tracking_id',
            ], [
                'tracking_id.required' => 'Tracking Id is Required',
                'tracking_id.exists' => 'Invalid Tracking Id',
            ]);

            $sponsorUser = User::whereTrackingId($request->tracking_id)->first();

            \Session::put('user_registration', [
                [
                    'sponsor_user' => $sponsorUser,
                    'selected_leg' => $leg,
                ]
            ]);

            return redirect()->route('user-register-details');
        }

        return view('user.authentication.register.index', [
            'sponsor_upline' => $sponsorUser,
        ]);
    }

    public function details(Request $request)
    {
        if(!\Session::has('user_registration'))
            return redirect()->route('user-register')->with(['error' => 'Not able to retrieve the User Details, Try again']);

        $session_details = (object) (array_collapse(\Session::get('user_registration')));

        $nominees = \File::get(public_path('data/nominees.json'));

        $states = State::active()->get();

        $country = Country::whereIn('id',[99,148])->active()->get();

        if ($request->isMethod('post'))
        {
            if($request->country_id == 99)
            {
                $this->validate($request,[
                    'pincode' => 'required|digits:6',
                ],[
                    'pincode.required' => 'Pincode is Required',
                    'pincode.digits' => 'Pincode Must be 6 digit number',
                ]);
            }
            if($request->country_id == 148)
            {
                $this->validate($request,[
                    'pincode' => 'required|digits:5',
                ],[
                    'pincode.required' => 'Pincode is Required',
                    'pincode.digits' => 'Pincode Must be 5 digit number',
                ]);
            }
            if(!$request->apply_pan){
                $this->validate($request,[
                    'pan_no' => 'regex:/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/',
                ],[
                    'pan_no.regex' => 'Invalid Pancard Number',
                ]);

                if($request->pan_no == UserDetail::wherePanNo($request->pan_no)->exists())
                    return redirect()->back()->withInput()->with(['error' => 'This Pancard Is Already Used ']);
            }
            $this->validate($request,[
                'leg' => 'required',
                'title' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'birth_date' => 'required',
                'mobile' => 'required|unique:users,mobile|regex:/^[9876][0-9]{9}$/|digits:10',
                'address' => 'required',
                'city' => 'required',
                'district' => 'required',
                'password' => 'required|min:6',
                'terms' => 'required',
                'apply_pan' => 'integer',
                'isd_code' => 'required',
            ], [
                'leg.required' => 'Kindly Select your Placement under Upline',
                'title.required' => 'Title is Required',
                'first_name.required' => 'First Name is Required',
                'last_name.required' => 'Last Name is Required',
                'birth_date.required' => 'Date of Birth is Required',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'address.required' => 'Address is Required',
                'city.required' => 'City is Required',
                'district.required' => 'District is Required',
                'password.required' => 'Password is required',
                'password.min' => 'Password length should be 6 or more',
                'terms.required' => 'Terms & Conditions is Required',
                'isd_code.digits' => 'ISD Code is Required',
            ]);

            \Session::push('user_registration', $request->all());

            return redirect()->route('user-register-overview');
        }

        return view('user.authentication.register.details', [
            'sponsor_user' => $session_details->sponsor_user,
            'nominee_relation' => json_decode($nominees),
            'states' => $states,
            'countries' => $country,
            'selected_leg' => $session_details->selected_leg
        ]);
    }

    public function overview(Request $request)
    {
        if(!\Session::has('user_registration'))
            return redirect()->route('user-register')->with(['error' => 'Not able to retrieve the User Details, Try again']);

        $user_detail = (object) (array_collapse(\Session::get('user_registration')));

        if ($request->isMethod('post'))
        {
            $response = (new Helper())->findTheUpline($user_detail->sponsor_user, $user_detail->leg);

            if ($response->status == false) {

                return redirect()->route('user-register')->with([
                    'error' => 'We can not place this user to Leg: ' . $user_detail->leg . ' Try Again'
                ]);
            }


            $user = \DB::transaction(function () use($user_detail, $response) {

                $parent = $response->upline;

                $tracking_id = rand(1000000, 9999999);

                while(User::whereTrackingId($tracking_id)->exists()) {
                    $tracking_id = rand(1000000, 9999999);
                }

                $user = User::create([
                    'parent_id' => $parent->id,
                    'sponsor_by' => $user_detail->sponsor_user->id,
                    'tracking_id' => $tracking_id,
                    'username' => $tracking_id,
                    'password' => $user_detail->password,
                    'wallet_password' => strtoupper(str_random(6)),
                    'leg' => $user_detail->leg,
                    'email' => $user_detail->email,
                    'isd_code' => $user_detail->isd_code,
                    'mobile' => $user_detail->mobile,
                    'token' => strtoupper(str_random(15))
                ]);

//                if($user_detail->apply_pan){
//
//                }else{
//
//                }

                $userDetail = new UserDetail();
                $userDetail->user_id = $user->id;
                $userDetail->title = $user_detail->title;
                $userDetail->first_name = $user_detail->first_name;
                $userDetail->last_name = $user_detail->last_name;
                $userDetail->gender = in_array($user_detail->title, ['Mr', 'Sri', 'Dr']) ? 1 : 2;
                $userDetail->birth_date = Carbon::parse($user_detail->birth_date);

                if($user_detail->pan_no)
                {
                    $userDetail->apply_pan = (isset($user_detail->apply_pan) && $user_detail->apply_pan ==1 ? UserDetail::Yes : UserDetail::No);
                    $userDetail->pan_no = (isset($user_detail->apply_pan)) ?  null : $user_detail->pan_no;
                }
                $userDetail->nagrikta_card_no = $user_detail->nagrikta_card_no ?  $user_detail->nagrikta_card_no : null;
                $userDetail->save();


                UserStatus::create([
                    'user_id' => $user->id,
                ]);

                UserBank::create([
                    'user_id' => $user->id
                ]);

                UserAddress::create([
                    'user_id' => $user->id,
                    'address' => $user_detail->address,
                    'city' => $user_detail->city,
                    'district' => $user_detail->district,
                    'state_id' => $user_detail->state_id,
                    'country_id' => $user_detail->country_id,
                    'pincode' => $user_detail->pincode
                ]);

                return $user;

            });

            \Session::put('registered_user', $user);

            return redirect()->route('user-register-thanks')->with([
                'success' => 'Your Registration has been successfully completed!',
            ]);
        }

        return view('user.authentication.register.overview', [
            'session_detail' => $user_detail
        ]);
    }

    public function thanks()
    {
        if (!\Session::has('user_registration') || !\Session::has('registered_user')) {
            return redirect()->route('user-register');
        }

        $user = User::with('detail')->whereId(\Session::get('registered_user')->id)->first();

        \Session::forget('user_registration');
        \Session::forget('registered_user');

        return view('user.authentication.register.thanks', [
            'user' => $user
        ]);
    }
}
