<?php

namespace App\Http\Controllers\User;

use App\Library\Helper;
use App\Library\ImageUpload;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Knp\Snappy\Pdf;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::whereUserId(\Session::get('user')->id)->latest()->paginate(30);

        return view('user.shopping.orders.index', [
            'orders' => $orders
        ]);
    }

    public function details(Request $request)
    {
        $order = Order::whereUserId(\Session::get('user')->id)->with(['details.product_price.product','store'])
            ->whereCustomerOrderId($request->customer_order_id)->first();

        if (!$order)
            return redirect()->back()->with(['error' => 'Not able to find the Order details, try again']);

        if ($request->download) {

            $setting = Setting::where('name','Company Profile')->first();

            $filename =  $order->customer_order_id;

            $snappy = new Pdf(base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'));

            $snappy->generateFromHtml(
                view('user.shopping.orders.invoice', [
                    'filename' => $filename, 'order' => $order, 'setting' => $setting
                ])->__toString(), storage_path("app/".$filename.".pdf"), [
                'orientation' => 'Portrait',
                'page-height' => 297,
                'page-width'  => 210,
            ], true);

            return response()->download(storage_path("app/".$filename.".pdf"))->deleteFileAfterSend(true);

        }

        return view('user.shopping.orders.details', [
            'order' => $order
        ]);
    }

    public function place(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'remarks' => 'required',
            'payment_mode' => 'required',
            'reference_number' => 'required',
        ], [
            'remarks.required' => 'Your Message is required for this Shopping Order',
            'payment_mode.required' => 'Please select payment mode for this Shopping Order',
            'reference_number.required' => 'Please Enter Payment Reference Number for Order',
        ]);

        if ($validator->fails())
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);

        if(Order::where('payment_reference->reference_number', $request->reference_number)->where('status', Order::APPROVED)->exists())
            return response()->json(['status' => false, 'message' => 'Entered reference number is already exists, please enter another reference number']);


        $image_name = null;
        if($request->payment_ref_image){

            $image_uploader = (new ImageUpload());
            if (!$image_uploader->process(null, ImageUpload::DOCUMENT_TYPE,null, base64_decode($image_uploader->getBase64Encode($request->payment_ref_image)))){

                return response()->json(['status' => false, 'message' => 'Invalid Image, May be broken try another one']);
            }

            $image_name = $image_uploader->store(env('PAYMENT_RECEIPT_PATH'));
        }

        $payment_details = [
            'payment_mode' => $request->payment_mode,
            'reference_number' => $request->reference_number ? : 'N.A'
        ];

        $order = \DB::transaction(function() use ($request, $payment_details, $image_name) {

            $items = collect(Helper::arrayToObject($request->items));

            $order =  Order::create([
                'user_id' => \Session::get('user')->id,
                'amount' => $request->total_amount,
                'discount' => 0,
                'wallet' => 0,
                'total' => $request->total_amount,
                'total_bv' => $request->total_bv,
                'status' => Order::PLACED,
                'remarks' => $request->remarks,
                'payment_reference' => $payment_details,
                'payment_ref_image' => $image_name
            ]);

            $order->customer_order_id = 'O' . $order->created_at->format('Ymd') . $order->id;
            $order->save();

            collect($items)->map(function ($item) use ($order) {

                OrderDetail::create([
                    'order_id' => $order->id,
                    'product_price_id' => $item->id,
                    'price' => $item->price,
                    'distributor_price' => $item->distributor_price,
                    'selling_price' => $item->selling_price,
                    'bv' => $item->bv,
                    'qty' => $item->quantity->current,
                    'gst' => json_encode($item->gst)
                ]);

            });

            \Session::forget('cart');

            return $order;

        });

        $request->session()->flash('success', 'New Order is Placed Successfully');

        return response()->json([
            'status' => true, 'customer_order_id' => $order->customer_order_id,
            'route' => route('user-shopping-order-details', ['customer_order_id' => $order->customer_order_id])
        ]);
    }
}
