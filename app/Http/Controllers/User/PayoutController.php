<?php

namespace App\Http\Controllers\User;

use App\Models\Payout;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PayoutController extends Controller
{
    public function index(Request $request)
    {
        $payouts = Payout::with(['user.detail'])->whereProcessType(Payout::ADMIN_PROCESS)->whereUserId(\Session::get('user')->id)->orderBy('id', 'desc');

        if ($request->download == "yes"){

            \Excel::create('Payout Report', function($excel) use ($payouts) {

                $excel->sheet('sheet1', function($sheet) use ($payouts) {

                    $data = $payouts->get()->map( function($payout){

                        return [
                            'Date' => Carbon::parse($payout->created_at)->format('M d, Y h:i A'),
                            'Member Name' => $payout->user->detail->full_name,
                            'Tracking Id' => $payout->user->tracking_id,
                            'Contact Number' => $payout->user->mobile,
                            'Total Amount' => $payout->total,
                            'TDS' => $payout->tds,
                            'Admin Charge' => $payout->admin_charge,
                            'Net Amount' => $payout->amount,
                            'reference_number' => $payout->reference_number,
                            'status' => $payout->status==1 ? "Pending" : "Transferred",
                            'remarks' => $payout->remarks
                        ];

                    })->toArray();
                    $sheet->fromArray($data);
                });

            })->download('xls');
        }

        return view('user.payout.index', [
            'payouts' => $payouts->paginate(20),
        ]);
    }

    public function makeRequest(Request $request)
    {
        $payout_requests = Payout::whereProcessType(Payout::USER_PROCESS)->whereUserId(\Session::get('user')->id)->orderBy('id', 'desc')->paginate(20);

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'transfer_amount' => 'required|numeric'
            ]);
        }

        return view('user.payout.request-view', [
            '$payout_request' => $payout_requests,
        ]);
    }

}
