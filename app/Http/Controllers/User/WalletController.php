<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\MonthlyBv;
use App\Models\Order;
use App\Models\OrderBvLog;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    public function index(Request $request)
    {
        $income_type = 0;

        if ($request->type) {

            if (!$income_type = Wallet::getIncomeTypeId($request->type))
                return redirect()->back()->with(['error' => 'Invalid Wallet Request Type']);
        }

        $wallets = Wallet::whereUserId(\Session::get('user')->id)->whereType(Wallet::CREDIT_TYPE)->orderBy('id', 'desc');

        if ($income_type > 0)
            $wallets = $wallets->whereIncomeType($income_type);

        return view('user.wallet.index', [
            'type' => $request->type,
            'wallets' => $wallets->paginate(30)
        ]);
    }


    public function totalReport(Request $request)
    {
        $wallets = Wallet::whereUserId(\Session::get('user')->id)->filterDate($request->dateRange);

        return view('user.wallet.report', [
            'wallets' => $wallets->orderBy('id', 'desc')->paginate(50)
        ]);
    }


    public function monthlyReport()
    {
        $months = MonthlyBv::selectRaw('month_name,id')
            ->whereNotIn('month_name', ['Jul 2021', 'Aug 2021'])
            ->groupBy('month_name')->orderBy('id','desc');

        return view('user.wallet.monthly-report',[
            'months' => $months->paginate(50)
        ]);
    }

    public function receipt(Request $request)
    {

        $user = User::whereId(\Session::get('user')->id)->first();

        if($request->month)
            $month =  $request->month;
        else
            $month =  now()->subMonthNoOverflow()->format("M Y");

        $self_records = OrderBvLog::whereUserId($user->id)
            ->selectRaw("COALESCE(SUM(bv), 0) as total_bv, DATE_FORMAT(created_at, '%b %Y') as month_name")
            ->whereNull('leg')
            ->whereRaw("DATE_FORMAT(created_at, '%b %Y') in ('{$month}')")
            ->groupBy('month_name')->get();


        $team_records = MonthlyBv::where('month_name', $month)->whereUserId($user->id)->get();

        $current_month_self_bv = optional($self_records->where('month_name', $month)->first())->total_bv;

        $current_month_team_record = $team_records->where('month_name', $month)->first();

        $records = Wallet::selectRaw('
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::SELF_PURCHASE .' THEN total END), 0) as self_repurchase, 
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::SELF_PURCHASE .' THEN tds END), 0) as self_repurchase_tds,
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::SELF_PURCHASE .' THEN admin_charge END), 0) as self_repurchase_admin,
        
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::PERFORMANCE_BONUS .' THEN total END), 0) as performance_bonus,
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::PERFORMANCE_BONUS .' THEN tds END), 0) as performance_bonus_tds,
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::PERFORMANCE_BONUS .' THEN admin_charge END), 0) as performance_bonus_admin,
        
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::LEADERSHIP_BONUS .' THEN total END), 0) as leadership_bonus,
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::LEADERSHIP_BONUS .' THEN tds END), 0) as leadership_bonus_tds,
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::LEADERSHIP_BONUS .' THEN admin_charge END), 0) as leadership_bonus_admin,
        
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::TEAM_DEVELOPMENT_BONUS .' THEN total END), 0) as team_development_bonus,
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::TEAM_DEVELOPMENT_BONUS .' THEN tds END), 0) as team_development_bonus_tds,
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::TEAM_DEVELOPMENT_BONUS .' THEN admin_charge END), 0) as team_development_bonus_admin,
        
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::ROYALTY_BONUS .' THEN total END), 0) as royalty_bonus,
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::ROYALTY_BONUS .' THEN tds END), 0) as royalty_bonus_tds,
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::ROYALTY_BONUS .' THEN admin_charge END), 0) as royalty_bonus_admin,
        
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::SPONSOR_TYPE .' THEN total END), 0) as sponsor_income,
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::SPONSOR_TYPE .' THEN tds END), 0) as sponsor_income_tds,
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::SPONSOR_TYPE .' THEN admin_charge END), 0) as sponsor_income_admin,
        
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::STORE_SPONSOR .' THEN total END), 0) as store_sponsor_income,
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::STORE_SPONSOR .' THEN tds END), 0) as store_sponsor_income_tds,
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::STORE_SPONSOR .' THEN admin_charge END), 0) as store_sponsor_income_admin,
        
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::SUPER_STORE_SPONSOR .' THEN total END), 0) as super_store_sponsor_income,
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::SUPER_STORE_SPONSOR .' THEN tds END), 0) as super_store_sponsor_income_tds,
        COALESCE(SUM(CASE WHEN income_type = '. Wallet::SUPER_STORE_SPONSOR .' THEN admin_charge END), 0) as super_store_sponsor_income_admin
        ')->whereRaw("DATE_FORMAT(created_at, '%b %Y') in ('{$month}')")
            ->whereUserId($user->id)->first();


        $income = [
//            'self_repurchase' => [
//                'name' => 'Self Repurchase',
//                'amount' => $records->self_repurchase,
//                'tds' => $records->self_repurchase_tds,
//                'admin' => $records->self_repurchase_admin
//            ],
            'performance_bonus' => [
                'name' => 'Performance Bonus',
                'amount' => $records->performance_bonus,
                'tds' => $records->performance_bonus_tds,
                'admin' => $records->performance_bonus_admin
            ],
            'leadership_bonus' => [
                'name' => 'Leadership Bonus',
                'amount' => $records->leadership_bonus,
                'tds' => $records->leadership_bonus_tds,
                'admin' => $records->leadership_bonus_admin
            ],
            'team_development_bonus' => [
                'name' => 'Team Development Bonus',
                'amount' => $records->team_development_bonus,
                'tds' => $records->team_development_bonus_tds,
                'admin' => $records->team_development_bonus_admin
            ],
            'royalty_bonus' => [
                'name' => 'Royalty Bonus',
                'amount' => $records->royalty_bonus,
                'tds' => $records->royalty_bonus_tds,
                'admin' => $records->royalty_bonus_admin
            ],
            'sponsor_income' => [
                'name' => 'Sponsor Income',
                'amount' => $records->sponsor_income,
                'tds' => $records->sponsor_income_tds,
                'admin' => $records->sponsor_income_admin
            ],
            'store_sponsor_income' => [
                'name' => 'Store Sponsor Income 1%',
                'amount' => $records->store_sponsor_income,
                'tds' => $records->store_sponsor_income_tds,
                'admin' => $records->store_sponsor_income_admin
            ],
            'super_store_sponsor_income' => [
                'name' => 'Super Store Income 2%',
                'amount' => $records->super_store_sponsor_income,
                'tds' => $records->super_store_sponsor_income_tds,
                'admin' => $records->super_store_sponsor_income_admin
            ],

        ];

        $performance_remarks = Wallet::selectRaw('remarks, id')
            ->whereUserId($user->id)
            ->where('income_type',Wallet::PERFORMANCE_BONUS)
            ->whereRaw("DATE_FORMAT(created_at, '%b %Y') in ('{$month}')")->first();

        $leadership_remarks = Wallet::selectRaw('remarks, id')
            ->whereUserId($user->id)
            ->where('income_type',Wallet::LEADERSHIP_BONUS)
            ->whereRaw("DATE_FORMAT(created_at, '%b %Y') in ('{$month}')")->first();

        $point_detail = (object)[
          'performance_bonus' => $performance_remarks ? explode(" ", $performance_remarks->remarks)[4] : 0,
          'leadership_bonus'  =>  $leadership_remarks ? explode(" ", $leadership_remarks->remarks)[4] : 0,
        ];

        return view('user.wallet.income-receipt',[
            'incomes' => collect($income),
            'user' => $user,
            'month' => $month,
            'current_month_self_bv' => $current_month_self_bv,
            'current_month_team_record' => $current_month_team_record,
            'point_detail' => $point_detail
        ]);
    }

}
