<?php

namespace App\Http\Controllers\User;

use App\Library\Helper;
use App\Models\Admin;
use App\Models\User;
use App\Models\UserStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        if($request->isMethod('post'))
        {

            $this->validate($request, [
                'username_tracking_id' => 'required',
                'password' => 'required'

            ], [
                'username_tracking_id.required' => 'Username or Tracking Id is Required',
                'password.required' => 'Password is Required'
            ]);

            $user = User::with('status')->whereUsername($request->username_tracking_id)->orWhere('tracking_id', $request->username_tracking_id)->first();

            if (!$user)
                return redirect()->back()->with(['error' => 'Invalid Username or Tracking ID']);

            if ($user->status->terminated == UserStatus::YES)
                return redirect()->back()->with(['error' => 'You are blocked to access your account, Contact to Support Team']);

            if($request->password != $user->password)
                return redirect()->back()->with(['error'=> 'Password is Not Match with Username!']);

            \Session::put('user', $user);

            User::whereId($user->id)->update([
                'last_logged_in_ip' => Helper::getClientIp(),
                'last_logged_in_at' => Carbon::now()
            ]);

            return redirect()->route('user-dashboard')->with([
                'success' => 'Welcome to ' . config('project.brand') . ' account'
            ]);

        }

        return view('user.authentication.login');

    }

    public function forgetPassword(Request $request)
    {
        if($request->isMethod('post')) {

            $this->validate($request, [
                'username' => 'required|exists:users,username',
                'email' => 'required|email'
            ], [
                'username.required' => 'Username is required to get New Password',
                'username.exists' => 'Invalid Username',
                'email.required' => 'Email Address is required',
                'email.email' => 'Invalid Email Address',
            ]);

            // --- After Activation of Email Functions Remove this Line ---- //
            return redirect()->route('user-login')->with(['error' => 'Contact Admin team to Activate this function']);


            $new_password = strtoupper(str_random(8));

            if (!$user = User::whereUsername($request->username)->whereEmail($request->email)->first())
                return redirect()->back()->withInput()->with(['error' => 'This Email id is not registered with this User']);

            $user->password = $new_password;
            $user->save();


            if(env('APP_ENV') != 'local')
            {
                \Mail::send('emails.user.forgot-password', [
                    'user' => $user, 'new_password' => $new_password
                ], function ($mail) use ($user) {
                    $mail->from('noreply@project.com', config('project.brand'));
                    $mail->to($user->email)->subject('New Password from ' . config('project.brand'));
                });
            }

            return redirect()->route('user-login')->with(['success' => 'New Password has been sent on Your Registered Email']);

        }

        return view('user.authentication.forget-password');
    }

    public function logout()
    {
        \Session::forget('user');
        \Session::forget('wallet_session');

        return redirect()->route('user-login');
    }

    public function adminViewAccess(Request $request)
    {
        if (!\Session::has('admin'))
            return redirect()->back()->with(['error' => 'Invalid Credentials for User Access']);

        if (!Admin::whereToken($request->token)->exists())
            return redirect()->back()->with(['error' => 'Invalid token for User Access']);

        \Session::forget('user');
        \Session::forget('wallet_session');

        if (!$user = User::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Credentials for User Access']);

        \Session::put('user', $user);
        \Session::put('wallet_session', $user);
        return redirect()->route('user-dashboard');
    }
}
