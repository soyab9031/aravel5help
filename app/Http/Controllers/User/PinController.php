<?php

namespace App\Http\Controllers\User;

use App\Library\Helper;
use App\Library\ImageUpload;
use App\Models\Package;
use App\Models\Pin;
use App\Models\PinRequest;
use App\Models\PinTransferHistory;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Webpatser\Uuid\Uuid;

class PinController extends Controller
{

    public function index(Request $request)
    {

        if ($request->isMethod('post'))
        {

            $this->validate($request, [
                'tracking_id' => 'required|exists:users,tracking_id',
                'pin_ids' => 'required|array'
            ], [
                'tracking_id.required' => 'User`s Tracking is required',
                'tracking_id.exists' => 'Invalid Tracking ID or Username',
                'pin_ids.required' => 'Select at least one PIN'
            ]);

            $pins = Pin::whereIn('id', $request->pin_ids)
                ->whereUserId(\Session::get('user')->id)->whereStatus(Pin::UNUSED)->get();


            if ($pins->count() > 0)
            {
                $user = User::whereTrackingId($request->tracking_id)->first();

                \DB::transaction( function () use ($request, $pins, $user) {

                    $pins->map( function ($pin) use ($user) {

                        PinTransferHistory::create([
                            'sender_user_id' => \Session::get('user')->id,
                            'receiver_user_id' => $user->id,
                            'pin_id' => $pin->id
                        ]);

                        $pin->user_id = $user->id;
                        $pin->save();
                    });

                });
            }

            return redirect()->route('user-pins')->with([
                'success' => $pins->count() . ' pin(s) has been transferred successfully..!!'
            ]);
        }

        $pins = Pin::with(['package','package_order.user.detail'])->whereUserId(\Session::get('user')->id)->search($request->search, [
            'number'
        ])->orderBy('id', 'desc')->paginate(30);

        return view('user.pin.index',[
            'pins' => $pins,
        ]);

    }


    public function viewRequest(Request $request)
    {
        $pins = PinRequest::with(['package'])->whereUserId(\Session::get('user')->id)->orderBy('id', 'desc')->paginate(30);

        return view('user.pin.request.index',[
            'pins' => $pins,
            'packages' => Package::active()->get()
        ]);
    }


    public function makeRequest(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'pin' => 'required|array',
                'payment_mode' => 'required',
                'reference_number' => 'required',
                'bank_name' => 'required',
                'deposit_date' => 'required',
                'deposit_time' => 'required',
            ],[
                'pin.required' => 'Pin Quantity is Required',
                'payment_mode.required' => 'Payment Mode is Required',
                'reference_number.required' => 'Reference Number is Required',
                'bank_name.required' => 'Bank Name is Required',
                'deposit_date.required' => 'Deposit Date is Required',
                'deposit_time.required' => 'Deposit Time is Required',
            ]);


            $filter_pin_data = collect(Helper::arrayToObject($request->pin))->filter(function ($pin) {
                return $pin->qty > 0;
            })->values();

            if (count($filter_pin_data) == 0)
                return redirect()->back()->with(['error' => 'Kindly Select at least one Qty for Request']);

            $image = null;

            $image_uploader = (new ImageUpload());

            if($request->image && $request->image->isValid()) {

                if(!$image_uploader->process($request->image,ImageUpload::DOCUMENT_TYPE))
                    return redirect()->back()->with(['error' => 'Invalid / Broken or Corrupted Image, Try Another']);

                $image = $image_uploader->store(env('PAYMENT_RECEIPT_PATH'));
            }

            PinRequest::create([
                'user_id' => \Session::get('user')->id,
                'details' => json_encode($filter_pin_data->toArray()),
                'payment_mode' => $request->payment_mode,
                'reference_number' => $request->reference_number,
                'bank_name' => $request->bank_name,
                'deposited_at' => Carbon::parse($request->deposit_date . $request->deposit_time),
                'image' => $image
            ]);

            return redirect()->route('user-pin-request')->with(['success' => 'Pin Request has been Created!!..']);
        }

        return view('user.pin.request.make', [
            'packages' => Package::active()->get(),
            'payment_modes' => json_decode(\File::get(public_path('data/payment_mode.json')))
        ]);
    }


}
