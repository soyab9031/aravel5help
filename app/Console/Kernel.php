<?php

namespace App\Console;

use App\Console\Commands\ProjectSetup;
use App\Library\Calculation\Binary\Builder;
use App\Library\Calculation\WalletCalculation;
use App\Models\NestedSetUser;
use App\Models\Order;
use App\Models\RewardAchiever;
use App\Models\SpecialIncome;
use App\Models\TeamDevelopmentAchiever;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ProjectSetup::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            (new NestedSetUser())->setData();
            (new Builder())->addToTree();
        })->everyMinute()->name('NestedSet')->withoutOverlapping();


        $schedule->call(function () {
            (new Order())->addBvToTree();
//            (new WalletCalculation())->selfPurchase();
        })->everyMinute()->name('OrderPVCalculation')->withoutOverlapping();

        $schedule->call(function () {
            (new WalletCalculation())->matchingIncome();
            (new TeamDevelopmentAchiever())->teamDevelopmentAchievers();
            (new WalletCalculation())->teamDevelopmentCalculation();
            (new RewardAchiever())->createAchievers();
            (new WalletCalculation())->sponsor();
        })->cron('30 0 1 * *')->name('MonthlyClosing')->withoutOverlapping();

        $schedule->call(function () {
            (new SpecialIncome())->calculation();
        })->cron('0 0 1 * *')->name('SpecialIncomeAchievers')->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
