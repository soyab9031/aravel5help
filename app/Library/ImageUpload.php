<?php

namespace App\Library;
use Aws\S3\S3Client;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\ImageManager;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use Webpatser\Uuid\Uuid;


class ImageUpload
{
    public $image;
    protected $client;

    CONST DOCUMENT_TYPE = 1, PROFILE_IMAGE_TYPE = 2;

    function __construct()
    {
        $this->client = S3Client::factory([
            'credentials' => [
                'key'    => env('S3_KEY'),
                'secret' => env('S3_SECRET'),
            ],
            'region' => env('S3_REGION'),
            'version' => 'latest',
        ]);
    }


    public function process($file, $type, $image = null, $base64String = null)
    {
        if ($type == self::DOCUMENT_TYPE) {
            $image_width = env('DOCUMENT_IMAGE_MAIN_WIDTH');
            $image_height = env('DOCUMENT_IMAGE_MAIN_HEIGHT');
        }
        else {
            $image_width = env('PROFILE_IMAGE_THUMB_WIDTH');
            $image_height = env('PROFILE_IMAGE_THUMB_HEIGHT');
        }

        try {

            if($base64String)
                $this->image = (new ImageManager())->make($base64String);
            else
                $this->image = (new ImageManager())->make(file_get_contents($file->path()));
        }
        catch(NotReadableException $e){
            return false;
        }

        if($this->image->height() < env('IMAGE_CONSTRAINT_HEIGHT') && $this->image->width() < env('IMAGE_CONSTRAINT_WIDTH'))
            return false;


        $this->image->resize($image_width, $image_height, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $canvas = (new ImageManager)->canvas($image_width+5, $image_height+5, '#ffffff');
        $this->image = $canvas->insert($this->image, 'center');

        return $this;
    }


    public function getBase64Encode($raw_image)
    {

        $image_data=null;
        if (strpos($raw_image, 'data:image/jpeg;base64,') === 0) {
            $image_data = str_replace('data:image/jpeg;base64,', '', $raw_image);
        }
        if (strpos($raw_image, 'data:image/png;base64,') === 0) {
            $image_data = str_replace('data:image/png;base64,', '', $raw_image);
        }

        $image_data = str_replace(' ', '+', $image_data);
        return $image_data;
    }

    public function store($path, $disk = 'S3')
    {
        $imageName =  Uuid::generate(4).'.jpg';

        if ($disk == 'S3') {

            $adapter = new AwsS3Adapter($this->client, env('S3_BUCKET'), $path, ['CacheControl' => 2592000]);
            $filesystem = new Filesystem($adapter);

            if(!$filesystem->put($imageName, $this->image->stream('jpg')->__toString()))
                return false;

        }
        else {

            $disk = \Storage::disk('spaces');
            $disk->put($path . $imageName, $this->image->stream('jpg')->__toString());
        }


        return $imageName;
    }

    public function delete($path, $image, $disk = 'S3')
    {
        if ($disk == 'S3') {

            $adapter = new AwsS3Adapter($this->client, env('S3_BUCKET'), $path, ['CacheControl' => 2592000]);
            $filesystem = new Filesystem($adapter);
            if($filesystem->delete($image))
                return true;
            else
                return false;

        }
        else {
            $disk = \Storage::disk('spaces');
            $disk->delete($path . $image);
        }
    }

}