<?php


namespace App\Library\Calculation;


use App\Models\Binary\BinaryAchievement;
use App\Models\Binary\BinaryCalculation;
use App\Models\MonthlyBv;
use App\Models\Order;
use App\Models\RewardAchiever;
use App\Models\TeamDevelopmentAchiever;
use App\Models\User;
use App\Models\UserStatus;
use App\Models\Wallet;
use Carbon\Carbon;

class WalletCalculation
{
    public function sponsor()
    {
        $previous_month = now()->subMonthNoOverflow()->format("M Y");

        Wallet::selectRaw("COALESCE(SUM(total), 0) as total_amount, user_id")
            ->whereRaw("DATE_FORMAT(created_at, '%b %Y') in ('{$previous_month}')")
            ->whereType(Wallet::CREDIT_TYPE)
            ->whereNotIn('income_type', [Wallet::SUPER_STORE_SPONSOR, Wallet::STORE_SPONSOR, Wallet::OTHER_TYPE])
            ->with(['user.sponsorBy', 'user.detail:user_id,title,first_name,last_name'])
            ->groupBy('user_id')->get()->map(function ($wallet_record) use ($previous_month){

                if(!$wallet_record->user->paid_at)
                    return false;

                if ($wallet_record->user->sponsor_by)
                {

                    $sponsor_user = Order::whereRaw("DATE_FORMAT(created_at, '%b %Y') in ('{$previous_month}')")
                        ->where('user_id', $wallet_record->user->sponsor_by)->where('status', Order::APPROVED)->exists();

                    if(!$sponsor_user)
                        return false;

                    $wallet_record->user->sponsorBy->credit(collect([
                        'amount' => $wallet_record->total_amount * 0.05,
                        'income_type' => Wallet::SPONSOR_TYPE,
                        'remarks' => 'Sponsor Income from ' . $wallet_record->user->detail->full_name . ' ('.$wallet_record->user->tracking_id.')',
                        'created_at' => Carbon::now()->subDay()
                    ]));
                }

            });
    }

    /**
     * Binary wallet Calculation with Capping
     */
    public function binary()
    {
        $pair_income = 100;

        BinaryAchievement::with(['user.package'])->whereStatus(BinaryAchievement::ACTIVE)->whereNull('wallet_id')
            ->whereHas('user.status', function ($query) {
                $query->where('binary_qualified', 1)->where('payment', UserStatus::YES);
            })->get()->map( function ($binary_achievement) use ($pair_income) {

                $today_income = Wallet::whereUserId($binary_achievement->user_id)->where([
                    ['created_at', '>=', Carbon::now()->startOfDay()],
                    ['created_at', '<=', Carbon::now()->endOfDay()]
                ])->whereType(Wallet::CREDIT_TYPE)->whereIncomeType(Wallet::BINARY_TYPE)->sum('total');

                if (($today_income + $pair_income) <= $binary_achievement->user->package->capping)
                {

                    \DB::transaction( function () use ($binary_achievement, $pair_income) {

                        $wallet = $binary_achievement->user->credit(collect([
                            'income_type' => Wallet::BINARY_TYPE,
                            'amount' => $pair_income,
                            'remarks' => 'Pair Matching Income # ' . $binary_achievement->calculation_reference_id
                        ]));

                        BinaryAchievement::whereId($binary_achievement->id)->update(['wallet_id' => $wallet->id ]);

                    });
                }
                else
                {
                    $rejected_calculation_ids = BinaryAchievement::whereUserId($binary_achievement->user_id)
                        ->whereNull('wallet_id')->whereStatus(BinaryAchievement::ACTIVE)->get()
                        ->pluck('calculation_id');


                    if ($rejected_calculation_ids->count() > 0)
                    {

                        \DB::transaction( function () use ($rejected_calculation_ids) {

                            BinaryCalculation::whereIn('id', $rejected_calculation_ids->toArray())->update([
                                'status' => BinaryCalculation::REJECTED,
                                'remarks' => 'Washed, Capping Applied'
                            ]);

                            BinaryAchievement::whereIn('calculation_id', $rejected_calculation_ids->toArray())->update([
                                'status' => BinaryAchievement::INACTIVE
                            ]);

                        });

                    }

                }

            });
    }


    public function selfPurchase()
    {

        Order::whereNull('wallet_id')->whereNotNull('approved_at')->get()->map( function($order){

            $amount = round($order->total_bv * 0.10);

            if($amount == 0)
                return false;

            $wallet = $order->user->credit(collect([
                'amount' => $amount,
                'income_type' => Wallet::SELF_PURCHASE,
                'remarks' => '10% Self Repurchase Income of order id ' . $order->customer_order_id
            ]));

            $order->wallet_id = $wallet->id;
            $order->save();

        });
    }


    /* Performance & Leadership Bonus */
    public function matchingIncome()
    {
        $previous_month = now()->subMonthNoOverflow()->format("M Y");

        $achievers = MonthlyBv::whereRaw("lft >= 2500 AND rgt >= 2500")->whereMonthName($previous_month)->get();
        $achievers = collect($achievers)->map(function ($achiever) use ($previous_month){

            /* PERFORMANCE_BONUS */
            $matching = min($achiever->lft, $achiever->rgt);

            $pairs = floor($matching/2500);

            $pairs = $pairs > 200 ? 200 : $pairs;

            $achiever->user->credit(collect([
                'amount' => round(($pairs * 2500) * 0.4),
                'income_type' => Wallet::PERFORMANCE_BONUS,
                'remarks' => "Performance Bonus Income against {$pairs} points of Month {$previous_month}",
                'created_at' => Carbon::now()->subDay()

            ]));
            /* PERFORMANCE_BONUS END */

//        Leadership income start
            $leadership_bonus_pairs = floor($matching/50000);

            if ($leadership_bonus_pairs > 0) {

                $leader_income = (floor($matching/5000) * 5000) * 0.1;

                $achiever->user->credit(collect([
                    'amount' => round($leader_income),
                    'income_type' => Wallet::LEADERSHIP_BONUS,
                    'remarks' => "Leadership Bonus Income against {$leadership_bonus_pairs} points of Month {$previous_month}",
                    'created_at' => Carbon::now()->subDay()
                ]));

            }
//        Leadership income end

        });

    }


    public function teamDevelopmentCalculation()
    {
        $previous_month = now()->subMonthNoOverflow()->format("M Y");

        $company_total_bv = Order::selectRaw("COALESCE(SUM(total_bv), 0) as total_bvs")->whereRaw("DATE_FORMAT(created_at, '%b %Y') in ('{$previous_month}')")->whereNotNull('approved_at')->first()->total_bvs;

        $team_dev_achiever = TeamDevelopmentAchiever::where('type',TeamDevelopmentAchiever::TEAM_BONUS)->count();

        if($team_dev_achiever == 0)
            return false;

        $royalty_bonus_achiever = TeamDevelopmentAchiever::where('type',TeamDevelopmentAchiever::ROYALTY_BONUS)->count();

        if($royalty_bonus_achiever == 0)
            return false;

        $team_dev_bonus = round($company_total_bv * 0.02) / $team_dev_achiever;
        $royalty_bonus= round($company_total_bv * 0.03) / $royalty_bonus_achiever;

        TeamDevelopmentAchiever::with(['user'])->orderBy('type','asc')->get()->map( function($achiever) use ($team_dev_bonus, $royalty_bonus, $previous_month){


            if($achiever->type == TeamDevelopmentAchiever::TEAM_BONUS) {

                $achiever->user->credit(collect([
                    'amount' => $team_dev_bonus,
                    'income_type' => Wallet::TEAM_DEVELOPMENT_BONUS,
                    'remarks' => "Team Development Bonus Income of Month {$previous_month}",
                    'created_at' => Carbon::now()->subDay()
                ]));
            }

            if($achiever->type == TeamDevelopmentAchiever::ROYALTY_BONUS){
                $achiever->user->credit(collect([
                    'amount' => $royalty_bonus,
                    'income_type' => Wallet::ROYALTY_BONUS,
                    'remarks' => "Royalty Bonus Income of Month {$previous_month}",
                    'created_at' => Carbon::now()->subDay()
                ]));
            }
        });
    }

}