<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\UserStatus
 *
 * @property int $id
 * @property int $user_id
 * @property int $address 0: NotVerify, 1: Verified
 * @property int $bank_account 0: NotVerify, 1: Verified
 * @property int $invoice 0: NotVerify, 1: Verified
 * @property int $pancard 0: NotVerify, 1: Verified
 * @property int $payment 0: Disallow, 1: Allow, (User Payment Status)
 * @property int $fake 0: No, 1: Yes (Consider as Fake ID)
 * @property int $binary_qualified 0: Disqualified, 1: Qualified
 * @property int $sponsor_qualified 0: Disqualified, 1: Qualified
 * @property int $tree_calculation 0: No, 1: Yes
 * @property int $nested_set_calculated 0: No, 1: Yes
 * @property int $dispatch 0: No, 1: Yes (Package Dispatch Status)
 * @property int $terminated 0: No, 1: Yes)
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus pendingTreeCalculation()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereBankAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereBinaryQualified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereDispatch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereFake($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereInvoice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus wherePancard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus wherePayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereSponsorQualified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereTerminated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereTreeCalculation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereUserId($value)
 * @mixin \Eloquent
 * @property int $email 0: No, 1: Yes (Welcome Email)
 * @property int $sms 0: No, 1: Yes (Welcome SMS)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereNestedSetCalculated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereSms($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read int|null $activity_count
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserStatus newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserStatus newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserStatus query()
 * @property int $aadhar_card 1: Pending, 2: Verified, 3:InProgress, 4:Rejected
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserStatus whereAadharCard($value)
 */
class UserStatus extends Model
{
    use Eloquence;

    CONST TREE_CALC_NO = 0, TREE_CALC_YES = 1;
    CONST FAKE_TYPE = 1, REAL_TYPE = 0;
    CONST YES = 1, NO = 0, PENDING = 2;

    const KYC_PENDING = 1, KYC_VERIFIED = 2, KYC_INPROGRESS = 3, KYC_REJECTED = 4;

    protected $fillable = [
        'user_id',
        'address',
        'invoice',
        'address',
        'bank_account',
        'pancard',
        'aadhar_card',
        'payment',
        'fake',
        'binary_qualified',
        'sponsor_qualified',
        'tree_calculation',
        'nested_set_calculated',
        'dispatch',
        'terminated',
        'nagrikta_card_no'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopePendingTreeCalculation($query)
    {
        return $query->where('tree_calculation', 0);
    }

    public static function  getKycStatuses()
    {
        return [
            self::KYC_PENDING => 'PENDING',
            self::KYC_VERIFIED => 'VERIFIED',
            self::KYC_INPROGRESS => 'IN PROGRESS',
            self::KYC_REJECTED => 'REJECTED',
        ];
    }
}
