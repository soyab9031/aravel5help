<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Payout
 *
 * @property int $id
 * @property int $user_id
 * @property float $total
 * @property float $tds
 * @property float $admin_charge
 * @property float $service_charge
 * @property float $amount
 * @property int|null $transfer_type 1: Cheque, 2: NEFT
 * @property int $status 1: Pending, 2: Transferred
 * @property int $process_type 1: Admin, 2: User
 * @property string|null $reference_number
 * @property string|null $remarks
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $transfer_type_name
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereAdminCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereReferenceNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereServiceCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereTds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereTransferType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereUserId($value)
 * @mixin \Eloquent
 * @property int|null $admin_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereProcessType($value)
 * @method static \Sofa\Eloquence\Builder|\App\Models\Payout newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Payout newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Payout query()
 * @property int|null $income_type 1: Default
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payout whereIncomeType($value)
 */
class Payout extends Model
{
    use Eloquence;

    const PENDING_TYPE = 1, TRANSFERRED_TYPE = 2;
    const CHEQUE_TYPE = 1, NEFT_TYPE = 2, IMPS_TYPE = 3, CASH_TYPE = 4;
    const ADMIN_PROCESS = 1, USER_PROCESS = 2;

    protected $fillable = [
        'admin_id', 'user_id', 'total', 'tds', 'admin_charge', 'service_charge', 'amount', 'status', 'transfer_type', 'reference_number', 'remarks', 'process_type', 'income_type','created_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getTransferTypeNameAttribute()
    {
        if ($this->transfer_type) {

            if ($this->transfer_type == self::CHEQUE_TYPE)
                return 'Cheque';
            elseif ($this->transfer_type == self::NEFT_TYPE)
                return 'NEFT';
            elseif ($this->transfer_type == self::IMPS_TYPE)
                return 'IMPS';
            else
                return 'CASH';

        }

        return 'N.A';
    }


}
