<?php

namespace App\Models;

use Baum\Node;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Support
 *
 * @property int $id
 * @property int $parent_id
 * @property int $category_id
 * @property int $admin_id
 * @property int $user_id
 * @property string $message
 * @property string $image
 * @property int $status 1: Open, 2: Closed
 * @property int $type 1: Admin to User, 2: User to Admin
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Admin $admin
 * @property-read \App\Models\SupportCategory $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Support whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Support whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Support whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Support whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Support whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Support whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Support whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Support whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Support whereUserId($value)
 * @mixin \Eloquent
 * @property int|null $lft
 * @property int|null $rgt
 * @property int|null $depth
 * @property-read \Baum\Extensions\Eloquent\Collection|\App\Models\Support[] $children
 * @property-read \App\Models\Support|null $parent
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node limitDepth($limit)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Support whereDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Support whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Support whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Support whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node withoutNode($node)
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node withoutRoot()
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node withoutSelf()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Support whereImage($value)
 * @property-read int|null $children_count
 * @method static \Sofa\Eloquence\Builder|\App\Models\Support newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Support newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Support query()
 */
class Support extends Node
{
    use Eloquence;

    CONST OPEN = 1, CLOSED = 2;
    const ADMIN_TO_USER = 1, USER_TO_ADMIN = 2;

    protected $fillable = [
        'parent_id', 'category_id', 'user_id', 'admin_id', 'message', 'image', 'status', 'type'
    ];

    public function category()
    {
        return $this->belongsTo(SupportCategory::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getImageAttribute($value)
    {
        return $value ? env('SUPPORT_IMAGE_URL') . $value : null;
    }
}
