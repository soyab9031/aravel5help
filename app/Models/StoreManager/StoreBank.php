<?php

namespace App\Models\StoreManager;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
/**
 * App\Models\StoreManager\StoreBank
 *
 * @property string|null $account_name
 * @property string|null $account_number
 * @property string|null $bank_name
 * @property string|null $branch
 * @property string|null $city
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property int $id
 * @property string|null $ifsc
 * @property int $store_id
 * @property int $type 1: Saving, 2: Current
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreBank newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreBank newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreBank query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreBank whereAccountName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreBank whereAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreBank whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreBank whereBranch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreBank whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreBank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreBank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreBank whereIfsc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreBank whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreBank whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreBank whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StoreBank extends Model
{
    use Eloquence;
    protected $fillable = [
        'store_id', 'bank_name', 'account_name', 'account_number', 'branch', 'ifsc', 'type', 'city'
    ];

}
