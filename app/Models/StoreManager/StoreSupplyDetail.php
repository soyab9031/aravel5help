<?php

namespace App\Models\StoreManager;

use App\Models\ProductPrice;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\StoreSupplyDetail
 *
 * @property int $id
 * @property int $supply_id
 * @property int $product_price_id
 * @property float $price
 * @property float $distributor_price
 * @property int $qty
 * @property string $gst Json: percentage & hsn/sac code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $total_amount
 * @property-read \App\Models\ProductPrice $product_price
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreSupplyDetail newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreSupplyDetail newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreSupplyDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupplyDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupplyDetail whereDistributorPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupplyDetail whereGst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupplyDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupplyDetail wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupplyDetail whereProductPriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupplyDetail whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupplyDetail whereSupplyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupplyDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property float $supply_price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreSupplyDetail whereSupplyPrice($value)
 * @property-read \App\Models\StoreManager\StoreSupply $supply
 * @property-read mixed $tax_amount
 * @property-read mixed $taxable_amount
 * @property-read mixed $total_tax_amount
 * @property-read mixed $total_taxable_amount
 */
class StoreSupplyDetail extends Model
{
    use Eloquence;

    protected $fillable = [
        'supply_id' , 'product_price_id', 'price', 'distributor_price', 'qty', 'gst', 'supply_price'
    ];

    public function product_price()
    {
        return $this->belongsTo(ProductPrice::class);
    }

    public function supply()
    {
        return $this->belongsTo(StoreSupply::class);
    }

    public function getGstAttribute($value)
    {
        return $value ? json_decode($value) : null;
    }

    public function getTotalAmountAttribute()
    {
        return $this->qty * $this->distributor_price;
    }

    public function getTaxAmountAttribute()
    {
        return round(($this->distributor_price*$this->gst->percentage)/((float)100+(float)$this->gst->percentage), 4);
    }

    public function getTotalTaxAmountAttribute()
    {
        return $this->tax_amount * $this->qty;
    }

    public function getTaxableAmountAttribute()
    {
        return round(($this->distributor_price - $this->tax_amount), 2);
    }

    public function getTotalTaxableAmountAttribute()
    {
        return $this->taxable_amount * $this->qty;
    }

}
