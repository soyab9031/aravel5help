<?php

namespace App\Models\StoreManager;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\PurchaseOrderDetail
 *
 * @property int $id
 * @property int $order_id
 * @property string $product_name
 * @property string|null $note
 * @property int $qty
 * @property float $amount
 * @property int $gst_percentage
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $total_amount
 * @property-read mixed $total_taxable_amount
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\PurchaseOrderDetail newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\PurchaseOrderDetail newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\PurchaseOrderDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrderDetail whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrderDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrderDetail whereGstPercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrderDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrderDetail whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrderDetail whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrderDetail whereProductName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrderDetail whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrderDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $total_tax_amount
 */
class PurchaseOrderDetail extends Model
{
    use Eloquence;

    protected $fillable = [
        'order_id', 'product_name', 'note', 'qty', 'amount', 'gst_percentage'
    ];

    public function getTotalTaxableAmountAttribute()
    {
        return $this->qty * $this->amount;
    }

    public function getTotalTaxAmountAttribute()
    {
        return round($this->total_taxable_amount * $this->gst_percentage / 100, 2);
    }

    public function getTotalAmountAttribute()
    {
        return $this->total_taxable_amount + round($this->total_taxable_amount * $this->gst_percentage / 100, 2);
    }
}
