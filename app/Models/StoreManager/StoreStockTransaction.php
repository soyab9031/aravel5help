<?php

namespace App\Models\StoreManager;

use App\Models\ProductPrice;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\StoreStockTransaction
 *
 * @property int $id
 * @property int|null $admin_id
 * @property int $store_id
 * @property int $product_price_id
 * @property int|null $order_id
 * @property int|null $supply_id
 * @property int $opening
 * @property int $qty
 * @property int $closing
 * @property int $type 1: Credit, 2: Debit
 * @property string|null $remarks
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ProductPrice $product_price
 * @property-read \App\Models\StoreManager\Store $store
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreStockTransaction newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreStockTransaction newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreStockTransaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockTransaction whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockTransaction whereClosing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockTransaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockTransaction whereOpening($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockTransaction whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockTransaction whereProductPriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockTransaction whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockTransaction whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockTransaction whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockTransaction whereSupplyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockTransaction whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockTransaction whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\StoreManager\MinimumStock $minimum_stock
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockTransaction withBalance()
 */
class StoreStockTransaction extends Model
{
    use Eloquence;

    CONST CREDIT = 1, DEBIT = 2;

    protected $fillable = [
        'admin_id', 'store_id', 'product_price_id', 'order_id', 'supply_id', 'opening', 'qty', 'closing', 'type', 'remarks'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function product_price()
    {
        return $this->belongsTo(ProductPrice::class);
    }

    public function minimum_stock()
    {
        return $this->belongsTo(MinimumStock::class,'product_price_id','product_price_id');
    }

    public function scopeWithBalance($q)
    {
        $balance_query = 'COALESCE(SUM(CASE WHEN type = 1 THEN qty END),0) - COALESCE(SUM(CASE WHEN type = 2 THEN qty END),0)';
        $q->selectRaw($balance_query . ' as balance, product_price_id')->groupBy('product_price_id')->havingRaw($balance_query . '> 0');
    }
}
