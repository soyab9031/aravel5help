<?php

namespace App\Models\StoreManager;

use App\Models\State;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\Supplier
 *
 * @property int $id
 * @property int $admin_id
 * @property string $name
 * @property string|null $email
 * @property string $mobile
 * @property string|null $other_contact
 * @property string $gst_number
 * @property string $address
 * @property string $city
 * @property string $pincode
 * @property int $state_id
 * @property int $status 1: Active, 2: Inactive
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\State $state
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Supplier active()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\Supplier newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\Supplier newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\Supplier query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Supplier whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Supplier whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Supplier whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Supplier whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Supplier whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Supplier whereGstNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Supplier whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Supplier whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Supplier whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Supplier whereOtherContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Supplier wherePincode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Supplier whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Supplier whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Supplier whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Supplier extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'admin_id', 'name', 'email', 'mobile', 'other_contact', 'address', 'city', 'state_id', 'gst_number', 'pincode', 'status'
    ];

    public function scopeActive($q)
    {
        $q->whereStatus(self::ACTIVE);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }
}
