<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\PinPaymentDetail
 *
 * @property int $id
 * @property string|null $payment_mode
 * @property int $qty
 * @property string|null $bank_name
 * @property string $payment_at
 * @property string|null $remarks
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Sofa\Eloquence\Builder|\App\Models\PinPaymentDetail newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PinPaymentDetail newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PinPaymentDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail wherePaymentAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail wherePaymentMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PinPaymentDetail extends Model
{
    use Eloquence;

    protected $fillable = [
        'payment_mode', 'qty', 'bank_name', 'payment_at', 'remarks'
    ];
}
