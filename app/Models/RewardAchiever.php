<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\RewardAchiever
 *
 * @property int $id
 * @property int $user_id
 * @property int $reward_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Reward $reward
 * @property-read \App\Models\User $user
 * @method static \Sofa\Eloquence\Builder|\App\Models\RewardAchiever newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\RewardAchiever newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\RewardAchiever query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardAchiever whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardAchiever whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardAchiever whereRewardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardAchiever whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardAchiever whereUserId($value)
 * @mixin \Eloquent
 */
class RewardAchiever extends Model
{
    use Eloquence;

    protected $fillable = [
        'user_id', 'reward_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function reward()
    {
        return $this->belongsTo(Reward::class);
    }


    public function createAchievers()
    {
        $previous_month = now()->subMonthNoOverflow()->format("M Y");

        Reward::get()->map(function ($reward) use($previous_month) {
            MonthlyBv::with(['user'])->selectRaw('COALESCE(SUM(lft), 0) as total_left, COALESCE(SUM(rgt), 0) as total_right, user_id')
                ->whereMonthName($previous_month)->groupBy('user_id')->get()->map( function($achiever) use ($reward){

                if($achiever->total_left >= $reward->pair && $achiever->total_right >= $reward->pair){
                    if (!self::whereUserId($achiever->user_id)->where('reward_id', $reward->id)->exists())
                        self::create(['user_id' => $achiever->user_id, 'reward_id' => $reward->id]);
                }
            });
        });

    }
}
