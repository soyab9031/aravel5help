<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\TeamDevelopmentAchiever
 *
 * @property int $id
 * @property int $user_id
 * @property int $status 1: Active, 2: Inactive
 * @property int $type 1: Team Bonus, 2: Royalty Bonus
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Sofa\Eloquence\Builder|\App\Models\TeamDevelopmentAchiever newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\TeamDevelopmentAchiever newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\TeamDevelopmentAchiever query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TeamDevelopmentAchiever whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TeamDevelopmentAchiever whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TeamDevelopmentAchiever whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TeamDevelopmentAchiever whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TeamDevelopmentAchiever whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TeamDevelopmentAchiever whereUserId($value)
 * @mixin \Eloquent
 */
class TeamDevelopmentAchiever extends Model
{
    use Eloquence;
    const TEAM_BONUS = 1, ROYALTY_BONUS = 2;
    protected $fillable = ['user_id', 'type', 'status'];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function teamDevelopmentAchievers()
    {
        MonthlyBv::selectRaw('COALESCE(SUM(lft), 0) as total_left, COALESCE(SUM(rgt), 0) as total_right, user_id')->groupBy('user_id')->get()->map( function($achiever){

            if($achiever->total_left < 200000 || $achiever->total_right < 200000)
                return false;

            $target_bv = 200000;
            $type =  TeamDevelopmentAchiever::TEAM_BONUS;

            if($type == TeamDevelopmentAchiever::TEAM_BONUS){
                if($achiever->total_left >= $target_bv && $achiever->total_right >= $target_bv){

                    if(TeamDevelopmentAchiever::whereUserId($achiever->user_id)->where('type',$type)->exists())
                        return false;

                    TeamDevelopmentAchiever::create([
                        'user_id' => $achiever->user_id,
                        'type' => $type
                    ]);
                }
            }

            $target_bv = 500000;
            $type =  TeamDevelopmentAchiever::ROYALTY_BONUS;

            if($type == TeamDevelopmentAchiever::ROYALTY_BONUS){
                if($achiever->total_left >= $target_bv && $achiever->total_right >= $target_bv){

                    if(TeamDevelopmentAchiever::whereUserId($achiever->user_id)->where('type',$type)->exists())
                        return false;

                    TeamDevelopmentAchiever::create([
                        'user_id' => $achiever->user_id,
                        'type' => $type
                    ]);

                }
            }

        });
    }

}
