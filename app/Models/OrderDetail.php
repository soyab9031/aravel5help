<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\OrderDetail
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_price_id
 * @property float $price
 * @property float $distributor_price
 * @property float $selling_price
 * @property float $bv
 * @property int $qty
 * @property string $gst Json: percentage & hsn/sac code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $tax_amount
 * @property-read mixed $taxable_amount
 * @property-read mixed $total_amount
 * @property-read mixed $total_bv
 * @property-read mixed $total_tax_amount
 * @property-read mixed $total_taxable_amount
 * @property-read \App\Models\Order $order
 * @property-read \App\Models\ProductPrice $product_price
 * @method static \Sofa\Eloquence\Builder|\App\Models\OrderDetail newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\OrderDetail newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\OrderDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereBv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereDistributorPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereGst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereProductPriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereSellingPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $actual_amount
 */
class OrderDetail extends Model
{
    use Eloquence;

    protected $fillable = [
        'order_id', 'product_price_id', 'price', 'distributor_price', 'selling_price', 'bv', 'qty', 'gst'
    ];

    public function product_price()
    {
        return $this->belongsTo(ProductPrice::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function getGstAttribute($value)
    {
        return $value ? json_decode($value) : null;
    }

    public function getTotalBvAttribute()
    {
        return $this->bv * $this->qty;
    }

    public function getTaxAmountAttribute()
    {
        return round(($this->selling_price*$this->gst->percentage)/((float)100+(float)$this->gst->percentage), 4);
    }

    public function getTotalTaxAmountAttribute()
    {
        return $this->tax_amount * $this->qty;
    }

    public function getTaxableAmountAttribute()
    {
        return round(($this->selling_price - $this->tax_amount), 2);
    }

    public function getTotalTaxableAmountAttribute()
    {
        return $this->taxable_amount * $this->qty;
    }

    public function getTotalAmountAttribute()
    {
        return $this->selling_price * $this->qty;
    }

    public function getActualAmountAttribute()
    {
        return round(($this->selling_price - $this->tax_amount), 2);
    }
}
