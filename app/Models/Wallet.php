<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Wallet
 *
 * @property int $id
 * @property int|null $admin_id
 * @property int $user_id
 * @property int|null $payout_id
 * @property int $type 1:Credit, 2:Debit
 * @property int $income_type 1:Binary, 2:Sponsor, 3:Re-Purchase, 4:Level, 5:Royalty, 6:Reward, 7:ROI, 8:Matrix
 * @property float $total
 * @property float $tds
 * @property float|null $admin_charge
 * @property float|null $service_charge
 * @property float $amount
 * @property string|null $remarks
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Admin|null $admin
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereAdminCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereIncomeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet wherePayoutId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereServiceCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereTds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereUserId($value)
 * @mixin \Eloquent
 * @property-read mixed $income_type_name
 * @method static \Sofa\Eloquence\Builder|\App\Models\Wallet newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Wallet newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Wallet query()
 */
class Wallet extends Model
{
    use Eloquence;

    const CREDIT_TYPE = 1, DEBIT_TYPE = 2;
    const BINARY_TYPE = 1, SPONSOR_TYPE = 2, OTHER_TYPE = 3, LEVEL_TYPE = 4, REWARD_TYPE = 5, SELF_PURCHASE = 6, PERFORMANCE_BONUS = 7, LEADERSHIP_BONUS = 8, TEAM_DEVELOPMENT_BONUS = 9, ROYALTY_BONUS = 10, SUPER_STORE_SPONSOR = 11, STORE_SPONSOR = 12 , SPECIAL_INCOME = 13;

    protected $fillable = [
        'admin_id', 'payout_id', 'user_id', 'total', 'tds','admin_charge', 'service_charge', 'amount', 'type', 'income_type', 'remarks', 'created_at'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function getIncomeTypeNameAttribute()
    {
        return $this->income_type > 0 ? self::incomeTypes($this->income_type) : 'N.A';
    }

    public static function incomeTypes($type = null)
    {
        $income_types =  [
            0 => 'OTHER',
            1 => 'BINARY',
            2 => 'SPONSOR',
            3 => 'OTHER',
            4 => 'LEVEL',
            5 => 'REWARD',
            6 => 'SELF PURCHASE TYPE',
            7 => 'PERFORMANCE BONUS TYPE',
            8 => 'LEADERSHIP BONUS TYPE',
            9 => 'TEAM DEVELOPMENT BONUS TYPE',
            10 => 'ROYALTY BONUS TYPE',
            11 => 'SUPER STORE SPONSOR TYPE',
            12 => 'STORE SPONSOR TYPE'
        ];

        return $type ? (isset($income_types[$type]) ? $income_types[$type] : null) : $income_types;
    }

    /**
     * @param string $type
     * @return mixed
     */
    public static function getIncomeTypeId($type)
    {
        return array_search(strtoupper($type), self::incomeTypes());
    }


}
