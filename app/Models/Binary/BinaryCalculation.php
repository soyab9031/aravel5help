<?php

namespace App\Models\Binary;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Binary\BinaryCalculation
 *
 * @property int $id
 * @property int $user_id
 * @property int $left
 * @property int $right
 * @property float $total_left
 * @property float $total_right
 * @property float $current_left
 * @property float $current_right
 * @property float $forward_left
 * @property float $forward_right
 * @property int $status 0: Open, 1: Closed, 2: Rejected
 * @property string|null $remarks
 * @property string|null $closed_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation closed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation open()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereClosedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereCurrentLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereCurrentRight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereForwardLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereForwardRight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereRight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereTotalLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereTotalRight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereUserId($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\Binary\BinaryCalculation newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Binary\BinaryCalculation newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Binary\BinaryCalculation query()
 */
class BinaryCalculation extends Model
{
    use Eloquence;

    CONST OPEN = 0, CLOSED = 1, REJECTED = 2;

    protected $fillable = [
        'user_id',
        'left',
        'right',
        'total_left',
        'total_right',
        'current_left',
        'current_right',
        'forward_left',
        'forward_right',
        'status',
        'remarks',
        'closed_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeOpen($query)
    {
        return $query->where('status', 0);
    }

    public function scopeClosed($query)
    {
        return $query->where('status', 1);
    }
}
