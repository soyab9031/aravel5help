<?php

namespace App\Models;

use App\Models\StoreManager\Store;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StorePendingCommission
 *
 * @property int|null $admin_id
 * @property float $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property int $id
 * @property string|null $remarks
 * @property int|null $sender_store_id
 * @property int $status 1: Not Received, 2: Received
 * @property int $store_id
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin|null $admin
 * @property-read \App\Models\StoreManager\Store $sender_store
 * @property-read \App\Models\StoreManager\Store $store
 * @method static \Sofa\Eloquence\Builder|\App\Models\StorePendingCommission newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StorePendingCommission newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StorePendingCommission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StorePendingCommission whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StorePendingCommission whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StorePendingCommission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StorePendingCommission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StorePendingCommission whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StorePendingCommission whereSenderStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StorePendingCommission whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StorePendingCommission whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StorePendingCommission whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property array|null $payment_details
 * @property float|null $tds
 * @property float|null $total
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StorePendingCommission wherePaymentDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StorePendingCommission whereTds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StorePendingCommission whereTotal($value)
 */
class StorePendingCommission extends Model
{
    use Eloquence;

    const PENDING = 1, TRANSFERRED = 2;

// store id who take commission sender store id who give commission
    protected $fillable = [
        'store_id', 'sender_store_id', 'admin_id', 'tds', 'amount', 'total', 'remarks', 'status', 'payment_details'
    ];

    protected $casts = [
      'payment_details' => 'json'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function store()
    {
        return $this->hasOne(Store::class, 'id', 'store_id');
    }

    public function sender_store()
    {
        return $this->hasOne(Store::class, 'id', 'sender_store_id');
    }
}
