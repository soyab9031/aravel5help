<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\ContactInquiry
 *
 * @property int $id
 * @property string $name
 * @property string $contact
 * @property string|null $email
 * @property string $message
 * @property int $status 0: Pending, 1: Seen, 2: Pined
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry pending()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry pinned()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInquiry whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\ContactInquiry newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\ContactInquiry newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\ContactInquiry query()
 */
class ContactInquiry extends Model
{
    use Eloquence;

    const PENDING = 0, SEEN = 1, PINNED = 2;

    protected $fillable = [
        'name', 'contact', 'email', 'message', 'status'
    ];

    public function scopePending($query)
    {
        $query->where('status', self::PENDING);
    }

    public function scopePinned($query)
    {
        $query->where('status', self::PINNED);
    }
}
