<?php

namespace App\Models;

use App\Library\Calculation\Binary\Helper;
use App\Models\StoreManager\Store;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $admin_id
 * @property int|null $offer_id
 * @property int|null $store_id
 * @property string|null $customer_order_id
 * @property float $amount
 * @property float $wallet
 * @property float $discount
 * @property float $total
 * @property float $total_bv
 * @property string|null $remarks
 * @property string|null $admin_remarks
 * @property int $status 1: Checkout, 2: Placed, 3: Approved, 4: Failed, 5: Rejected
 * @property int $payment_status 1: Checkout, 2: Success, 3: Pending, 4: Failed
 * @property string|null $payment_reference
 * @property string|null $approved_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin|null $admin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderDetail[] $details
 * @property-read int|null $details_count
 * @property-read \App\Models\User $user
 * @method static \Sofa\Eloquence\Builder|\App\Models\Order newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Order newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereAdminRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereApprovedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCustomerOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePaymentReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePaymentStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereTotalBv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereWallet($value)
 * @mixin \Eloquent
 * @property int $bv_calculated 0: Pending, 1: Done
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereBvCalculated($value)
 * @property int|null $wallet_id
 * @property-read \App\Models\StoreManager\Store|null $store
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereWalletId($value)
 * @property string|null $payment_ref_image
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePaymentRefImage($value)
 */
class Order extends Model
{
    use Eloquence;
    const NO = 0, YES = 1;
    const PAYMENT_CHECKOUT = 1, PAYMENT_PENDING = 3, PAYMENT_SUCCESS= 2, PAYMENT_FAILED = 3;
    const CHECKOUT = 1, PLACED = 2, APPROVED = 3, FAILED = 4, REJECTED = 5;

    protected $casts = [
      'payment_reference' => 'json'
    ];

    protected $fillable = [
        'user_id', 'customer_order_id', 'store_id', 'admin_id', 'offer_id', 'amount', 'wallet', 'discount', 'total', 'total_bv', 'remarks', 'admin_remarks', 'status', 'payment_status', 'payment_reference', 'approved_at', 'offer_id', 'bv_calculated', 'wallet_id', 'payment_ref_image'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public static function getStatus($id = null)
    {
        $statuses = [
            1 => 'Checkout', 2 => 'Placed', 3 => 'Approved', 4 => 'Failed', 5 => 'Rejected'
        ];

        return $id ? $statuses[$id] : $statuses;
    }


    public function addBvToTree()
    {
        Order::with(['user'])->whereBvCalculated(Order::NO)
            ->whereNotNull('approved_at')->chunk(20, function ($orders) {

                \DB::transaction(function () use ($orders) {

                    collect($orders)->map( function ($order) {

                        $month_name = $order->created_at->format('M Y');

                        /* Self */
                        OrderBvLog::setData($order, $order->user_id);

                        $next_leg = '';
                        $total_bv = $order->total_bv;

                        if ($order->user_id == 1) {

                            Order::whereId($order->id)->update([
                                'bv_calculated' => Order::YES
                            ]);

                            return false;
                        }

                        (new Helper())->parents($order->user_id, $parents);

                        foreach ($parents as $index => $parent) {

                            if ($index == 0) {
                                $leg = $order->user->leg;
                                $next_leg = $parent->leg;
                            }
                            else {

                                $leg = $next_leg;
                                $next_leg = $parent->leg;
                            }

                            OrderBvLog::setData($order, $parent->id, $leg);

                            if($exist_in_calculation = MonthlyBv::whereUserId($parent->id)->whereMonthName($month_name)->first())
                            {
                                if($leg == 'L')
                                {
                                    MonthlyBv::whereId($exist_in_calculation->id)
                                        ->update([
                                            'lft' => \DB::raw("lft + $total_bv"),
                                        ]);
                                }
                                elseif($leg == 'R')
                                {
                                    MonthlyBv::whereId($exist_in_calculation->id)
                                        ->update([
                                            'rgt' => \DB::raw("rgt + $total_bv"),
                                        ]);
                                }
                            }
                            else
                            {
                                MonthlyBv::create([
                                    'user_id' => $parent->id,
                                    'lft' => $leg == 'L' ? $total_bv : 0,
                                    'rgt' => $leg == 'R' ? $total_bv : 0,
                                    'month_name' => $month_name
                                ]);

                            }

                        }

                        Order::whereId($order->id)->update([
                            'bv_calculated' => Order::YES
                        ]);

                    });

                });

            });
    }

}
