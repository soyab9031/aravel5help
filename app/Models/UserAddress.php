<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\UserAddress
 *
 * @property int $id
 * @property int $user_id
 * @property string $address
 * @property string|null $landmark
 * @property string $city
 * @property string|null $district
 * @property int|null $state_id
 * @property string|null $pincode
 * @property int $type 1: Default, 2: Another
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\State|null $state
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereLandmark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress wherePincode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereUserId($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserAddress newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserAddress newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserAddress query()
 */
class UserAddress extends Model
{
    use Eloquence;

    protected $fillable = [
        'user_id', 'address', 'landmark', 'city', 'district', 'state_id', 'pincode', 'type','country_id'
    ];

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id', 'id');
    }
}
