<?php

namespace App\Models;

use App\Models\StoreManager\Store;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreDocument
 *
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property int $id
 * @property string $image
 * @property string|null $number
 * @property string|null $remarks
 * @property string|null $secondary_image
 * @property int $status 1: Pending, 2: Verified, 3: Rejected
 * @property int $store_id
 * @property int $type 1: PanCard, 2: AadharCard, 3: GSTNumber,4: AddressProof
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\StoreManager\Store $store
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreDocument newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreDocument newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreDocument whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreDocument whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreDocument whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreDocument whereSecondaryImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreDocument whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreDocument whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreDocument whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreDocument whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $document_name
 */
class StoreDocument extends Model
{
    use Eloquence;
    const PENDING = 1, VERIFIED = 2, REJECTED = 3;
    const PAN_CARD = 1,
        INVOICE = 2,
        CANCEL_CHEQUE = 3, BANK_PASSBOOK = 4, // Bank Proof
        AADHAR_CARD = 5, // Aadhar Proof
        VOTER_ID = 6, ELECTRICITY_BILL = 7, PASSPORT = 8, DRIVING_LICENCE = 9, // Address Proof
        GST_NUMBER = 10;

    protected $fillable = [
        'store_id', 'image', 'secondary_image', 'type', 'status', 'number'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function getDocumentNameAttribute()
    {
        $documents = collect(self::documents())->flatten()->toArray();

        return isset($documents[($this->type - 1)]) ? $documents[($this->type - 1)] : 'Other';
    }

    public static function documents($type = null)
    {
        $document_names =  [
            'PanCard' => [
                1 => 'PAN CARD'
            ],
            'Invoice' => [
                2 => 'INVOICE'
            ],
            'Bank' =>  [
                3 => 'CANCEL CHEQUE',
                4 => 'BANK PASSBOOK',
            ],
            'Aadhar' =>  [
                5 => 'AADHAR CARD',
            ],
            'Address' => [
                6 => 'VOTER ID',
                7 => 'ELECTRICITY BILL',
                8 => 'LEAVING CERTIFICATE',
                9 => 'DRIVING LICENCE'
            ],
            'GST Number' => [
                10 => 'GST NUMBER'
            ]
        ];

        return $type ? (isset($document_names[$type]) ? $document_names[$type] : null) : $document_names;
    }

}
