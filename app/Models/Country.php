<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Country
 *
 * @property int $id
 * @property string $name
 * @property string $continent
 * @property int $status 1: Active, 2: Inactive
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\State[] $states
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereContinent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country active()
 * @property-read int|null $states_count
 * @method static \Sofa\Eloquence\Builder|\App\Models\Country newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Country newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Country query()
 */
class Country extends Model
{
    use Eloquence;

    protected $fillable = [
        'name', 'continent', 'status'
    ];

    public function states()
    {
        return $this->hasMany(State::class);
    }

    public function scopeActive($q)
    {
        $q->where('status', 1);
    }
}
