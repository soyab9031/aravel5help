<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\SpecialIncome
 *
 * @property int $admin_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property int $id
 * @property int $status 0 : In Active , 1 : Active
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $user_id
 * @property-read \App\Models\User $user
 * @method static \Sofa\Eloquence\Builder|\App\Models\SpecialIncome newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\SpecialIncome newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\SpecialIncome query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SpecialIncome whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SpecialIncome whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SpecialIncome whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SpecialIncome whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SpecialIncome whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SpecialIncome whereUserId($value)
 * @mixin \Eloquent
 */
class SpecialIncome extends Model
{
    use Eloquence;
    const ACTIVE = 1 , INACTIVE = 0;

    protected $fillable = ['user_id' , 'admin_id' , 'status'];

    public function user()
    {
        return $this->belongsTo(User::class , 'user_id');
    }

    public function calculation()
    {
        $month_name = now()->subMonthNoOverflow()->format('M Y');

        $total_bv = Order::whereNotNull('approved_at')
            ->whereRaw('DATE_FORMAT(approved_at, "%b %Y") = "' . $month_name . '"')->sum('total_bv');

        if($total_bv == 0)
            return false;

        $achievers = SpecialIncome::whereStatus(SpecialIncome::ACTIVE)->get();

        $total_sponsor_income_user = SpecialIncome::whereStatus(SpecialIncome::ACTIVE)->count();

        if($total_sponsor_income_user < 0)
            return false;

        $setting = Setting::whereName('Special Income')->first();

        $bv_value = round($total_bv * $setting->value->percentage / 100);

        $income = round($bv_value / $total_sponsor_income_user,2);

        collect($achievers)->map(function ($special_user) use ($income, $month_name, $setting) {
            $special_user->user->credit(collect([
                'amount' => $income,
                'income_type' => Wallet::SPECIAL_INCOME,
                'remarks' =>  $setting->value->percentage .'% Percent Special Income From Month ' . $month_name
            ]));
        });
    }
}
