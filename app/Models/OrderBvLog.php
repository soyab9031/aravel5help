<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\OrderBvLog
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $order_ids
 * @property string|null $leg
 * @property float $bv
 * @property float $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Sofa\Eloquence\Builder|\App\Models\OrderBvLog newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\OrderBvLog newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\OrderBvLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderBvLog whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderBvLog whereBv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderBvLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderBvLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderBvLog whereLeg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderBvLog whereOrderIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderBvLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderBvLog whereUserId($value)
 * @mixin \Eloquent
 */
class OrderBvLog extends Model
{
    use  Eloquence;

    protected $fillable = [
        'user_id', 'order_ids', 'leg', 'bv', 'amount', 'created_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getOrderIdsAttribute($value)
    {
        return $value ? json_decode($value, true) : [];
    }

    public static function setData(Order $order, $user_id, $leg = null)
    {
        $bv = $order->total_bv;

        if (!$leg) {

            $log = self::whereUserId($user_id)->whereNull('leg')
                ->whereDate('created_at', '=', Carbon::parse($order->approved_at)->toDateString())
                ->first();
        }
        else {
            $log = self::whereUserId($user_id)->where('leg', $leg)
                ->whereDate('created_at', '=', Carbon::parse($order->approved_at)->toDateString())
                ->first();
        }

        if($log) {

            if (!in_array($order->id, $log->order_ids))
            {
                $order_ids = array_merge($log->order_ids, [$order->id]);

                self::whereId($log->id)->update([
                    'bv' => \DB::raw("bv + $bv"),
                    'amount' => \DB::raw("amount + $order->amount"),
                    'order_ids' => json_encode($order_ids)
                ]);
            }

        }
        else {

            self::create([
                'user_id' => $user_id, 'leg' => $leg,
                'bv' => $bv, 'amount' => $order->amount,
                'order_ids' => json_encode([$order->id]), 'created_at' => $order->approved_at
            ]);
        }

    }
}
