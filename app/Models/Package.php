<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Package
 *
 * @property int $id
 * @property string $name
 * @property string $prefix
 * @property int $amount
 * @property int|null $capping
 * @property int|null $sponsor_income
 * @property float $pv
 * @property string $declaration
 * @property int $status 1: Active, 0: InActive
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereCapping($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereDeclaration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package wherePrefix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package wherePv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereSponsorIncome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Package whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PackageItem[] $items
 * @property-read int|null $items_count
 * @method static \Sofa\Eloquence\Builder|\App\Models\Package newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Package newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Package query()
 */
class Package extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'name', 'prefix', 'amount', 'capping', 'sponsor_income', 'pv', 'declaration', 'status'
    ];

    // Scopes
    public function scopeActive($query)
    {
        return $query->where('status',1);
    }

    public function items()
    {
        return $this->hasMany(PackageItem::class);
    }
}
