<?php

namespace App\Models;

use Baum\Node;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\NestedSetUser
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $parent_id
 * @property int|null $lft
 * @property int|null $rgt
 * @property int|null $depth
 * @property string|null $leg
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereLeg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereUserId($value)
 * @mixin \Eloquent
 * @property-read \Baum\Extensions\Eloquent\Collection|\App\Models\NestedSetUser[] $children
 * @property-read \App\Models\NestedSetUser|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node limitDepth($limit)
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node withoutNode($node)
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node withoutRoot()
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node withoutSelf()
 * @property-read int|null $children_count
 * @method static \Sofa\Eloquence\Builder|\App\Models\NestedSetUser newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\NestedSetUser newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\NestedSetUser query()
 */
class NestedSetUser extends Node
{
    use Eloquence;

    protected $fillable = [
        'user_id', 'parent_id', 'lft', 'rgt', 'depth', 'leg', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function childrenCount($user_id)
    {
        $nested_user = self::whereUserId($user_id)->first();

        return $nested_user ? $nested_user->descendants()->count() : 0;
    }

    public function setData()
    {
        User::select(['id', 'parent_id', 'leg', 'created_at'])->whereHas('status', function ($q) {
            $q->where('nested_set_calculated', UserStatus::NO);
        })->get()->map(function ($user) {

            if ($user->parent_id == null) {

                $node = self::create([
                    'user_id' => $user->id
                ]);

                $node->makeRoot();

                UserStatus::whereUserId($node->user_id)->update([
                    'nested_set_calculated' => UserStatus::YES
                ]);
            }
            else
            {
                $node = self::whereUserId($user->parent_id)->first()->children()->create([
                    'user_id' => $user->id,
                    'leg' => $user->leg
                ]);

                UserStatus::whereUserId($node->user_id)->update([
                    'nested_set_calculated' => UserStatus::YES
                ]);

            }

        });

    }
}
