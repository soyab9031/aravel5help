<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BonanzaOffer
 *
 * @property int $end_at
 * @property int $id
 * @property int $lft
 * @property string $name
 * @property int $rgt
 * @property int $start_at
 * @property string $status 1: Active, 2: Inactive
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BonanzaOffer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BonanzaOffer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BonanzaOffer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BonanzaOffer whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BonanzaOffer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BonanzaOffer whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BonanzaOffer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BonanzaOffer whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BonanzaOffer whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BonanzaOffer whereStatus($value)
 * @mixin \Eloquent
 */
class BonanzaOffer extends Model
{

    const ACTIVE = 1, INACTIVE = 2;
    protected $fillable = [
        'name', 'start_at', 'end_at', 'lft', 'rgt', 'status'
    ];
}
