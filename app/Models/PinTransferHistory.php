<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\PinTransferHistory
 *
 * @property int $id
 * @property int $sender_user_id
 * @property int $receiver_user_id
 * @property int $pin_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\User $receiver
 * @property-read \App\Models\User $sender
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinTransferHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinTransferHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinTransferHistory wherePinId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinTransferHistory whereReceiverUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinTransferHistory whereSenderUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinTransferHistory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\PinTransferHistory newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PinTransferHistory newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PinTransferHistory query()
 */
class PinTransferHistory extends Model
{
    use Eloquence;

    protected $fillable = [
        'sender_user_id', 'receiver_user_id', 'pin_id'
    ];

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_user_id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_user_id');
    }
}
