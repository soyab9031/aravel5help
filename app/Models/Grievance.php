<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Grievance
 *
 * @property int $id
 * @property string $name
 * @property string $contact
 * @property string|null $email
 * @property string $subject
 * @property string $message
 * @property int $status 0: Pending, 1: Seen, 2: Pined
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance pending()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance pinned()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grievance whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\Grievance newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Grievance newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Grievance query()
 */
class Grievance extends Model
{
    use Eloquence;

    const PENDING = 0, SEEN = 1, PINNED = 2;

    protected $fillable = [
        'name', 'contact', 'email', 'subject', 'message', 'status'
    ];

    public function scopePending($query)
    {
        $query->where('status', self::PENDING);
    }

    public function scopePinned($query)
    {
        $query->where('status', self::PINNED);
    }
}
