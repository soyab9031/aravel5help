<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Pin
 *
 * @property int $id
 * @property int $admin_id
 * @property int $package_id
 * @property int $user_id
 * @property string $number
 * @property int $amount
 * @property int $status 0: Unused, 1: Used, 2: Block
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Package $package
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Admin $admin
 * @method static \Sofa\Eloquence\Builder|\App\Models\Pin newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Pin newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Pin query()
 * @property-read \App\Models\PackageOrder $package_order
 * @property int|null $pin_payment_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pin wherePinPaymentId($value)
 */
class Pin extends Model
{
    use Eloquence;

    protected $fillable = [
        'admin_id', 'pin_payment_id', 'package_id', 'user_id', 'number', 'amount', 'status', 'used_at'
    ];

    const UNUSED = 0, USED = 1, BLOCKED = 2;

    // Scopes
    public function scopeActive($query)
    {
        return $query->where('status',0);
    }

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function package_order()
    {
        return $this->hasOne(PackageOrder::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function package()
    {
        return $this->hasOne(Package::class, 'id', 'package_id');
    }

}
