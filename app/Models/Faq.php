<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Faq
 *
 * @property int $id
 * @property int $admin_id
 * @property string $question
 * @property string $answer
 * @property int $status 1: Active, 2: Inactive
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Sofa\Eloquence\Builder|\App\Models\Faq newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Faq newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Faq query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Faq extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'admin_id', 'question', 'answer', 'status'
    ];
}
