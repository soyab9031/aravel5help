<?php

namespace App\Models;

use App\Models\Binary\BinaryCalculation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\User
 *
 * @property int $id
 * @property int|null $parent_id
 * @property int $sponsor_by
 * @property string $tracking_id
 * @property string $username
 * @property string $password
 * @property string $wallet_password
 * @property int $pin_id
 * @property int $wallet_id
 * @property string $leg
 * @property string $email
 * @property string $mobile
 * @property int $package_id
 * @property float $joining_amount
 * @property string|null $token
 * @property string|null $activated_at
 * @property string|null $paid_at
 * @property string|null $last_logged_in_ip
 * @property string|null $last_logged_in_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\UserDetail $detail
 * @property-read mixed $referral_link
 * @property-read \App\Models\Package $package
 * @property-read \App\Models\User|null $parent
 * @property-read \App\Models\User $parentBy
 * @property-read \App\Models\Pin $pin
 * @property-read \App\Models\User $sponsorBy
 * @property-read \Baum\Extensions\Eloquent\Collection|\App\Models\User[] $sponsors
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node limitDepth($limit)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereActivatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereJoiningAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLeg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePinId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereWalletId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSponsorBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereTrackingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereWalletPassword($value)
 * @mixin \Eloquent
 * @property-read \App\Models\UserBank $bank
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Binary\BinaryCalculation[] $binary_calculation
 * @property-read \App\Models\UserStatus $status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserAddress[] $user_addresses
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserAddress[] $addresses
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Binary\BinaryCalculation[] $binary_calculations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastLoggedInAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastLoggedInIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePaidAt($value)
 * @property-read mixed $wallet_balance
 * @property-read int|null $addresses_count
 * @property-read int|null $binary_calculations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $children
 * @property-read int|null $children_count
 * @property-read mixed $current_binary_status
 * @property-read int|null $sponsors_count
 * @method static \Sofa\Eloquence\Builder|\App\Models\User newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\User newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\User query()
 * @property-read \App\Models\UserAddress $address
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MonthlyBv[] $monthly_bvs
 * @property-read int|null $monthly_bvs_count
 */
class User extends Model
{
    use Eloquence;

    protected $fillable = [
        'parent_id', 'sponsor_by', 'tracking_id', 'username', 'password', 'wallet_password', 'pin_id', 'wallet_id', 'leg', 'email', 'package_id', 'joining_amount', 'mobile', 'activated_at', 'paid_at', 'token', 'last_logged_in_ip', 'last_logged_in_at', 'isd_code'
    ];

    protected $hidden = [
        'password', 'token', 'wallet_password'
    ];

    /* Relations */
    public function sponsorBy()
    {
        return $this->hasOne(User::class, 'id', 'sponsor_by');
    }

    public function sponsors()
    {
        return $this->hasMany(User::class, 'sponsor_by', 'id');
    }

    public function parentBy()
    {
        return $this->hasOne(User::class, 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(User::class, 'parent_id', 'id');
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function detail()
    {
        return $this->hasOne(UserDetail::class, 'user_id');
    }

    public function status()
    {
        return $this->hasOne(UserStatus::class);
    }

    public function pin()
    {
        return $this->belongsTo(Pin::class);
    }

    public function bank()
    {
        return $this->hasOne(UserBank::class);
    }

    public function address()
    {
        return $this->hasOne(UserAddress::class);
    }

    public function addresses()
    {
        return $this->hasMany(UserAddress::class);
    }

    public function binary_calculations()
    {
        return $this->hasMany(BinaryCalculation::class)->orderBy('id', 'desc');
    }

    public function monthly_bvs()
    {
        return $this->hasMany(MonthlyBv::class, 'user_id');
    }

    public function getWalletBalanceAttribute()
    {
        $wallet = Wallet::selectRaw('COALESCE(SUM(CASE WHEN type = 1 THEN amount END), 0) - COALESCE(SUM(CASE WHEN type = 2 THEN amount END), 0) as balance')->whereUserId($this->id)->first();
        return $wallet->balance;
    }

    /* Helpful Methods */
    public function getCurrentBinaryStatusAttribute()
    {
        if ($calculation = $this->binary_calculations()->orderby('id', 'desc')->open()->first()) {

            return (object) [
                'left' => $calculation->left,
                'right' => $calculation->right,
                'total_left' => $calculation->total_left,
                'total_right' => $calculation->total_right,
                'current_left' => $calculation->current_left,
                'current_right' => $calculation->current_right,
                'forward_left' => $calculation->forward_left,
                'forward_right' => $calculation->forward_right,
            ];
        }

        return (object) [
            'left' => 0, 'right' => 0,
            'total_left' => 0, 'total_right' => 0,
            'current_left' => 0, 'current_right' => 0,
            'forward_left' => 0, 'forward_right' => 0,
        ];
    }

    public function user_default_address()
    {
        return $this->addresses()->where('type', 1)->first();
    }

    public function getReferralLinkAttribute()
    {
        return route('user-register', ['referral' => $this->tracking_id]);
    }

    public function treeStatus()
    {
        $gender = $this->detail->gender;
        $folder = $gender == 1 ? '' : 'female/';

        if ($this->paid_at == null)
            $image = '/user-assets/images/tree/' . $folder .'inactive.svg';

        elseif ($this->status->terminated == 1)
            $image = '/user-assets/images/tree/' . $folder .'terminated.svg';

        elseif ($this->status->payment == 0)
            $image = '/user-assets/images/tree/' . $folder .'payment-stop.svg';

        else
            $image = '/user-assets/images/tree/' . $folder .'active.svg';

        return $image;
    }

    /**
     * User's Wallet Credit
     *
     * @param  Collection  $details
     * @return Wallet
     */
    public function credit($details)
    {
        $pancard = UserDocument::whereUserId($this->id)->where('type', UserDocument::PAN_CARD)
            ->where('status', UserDocument::VERIFIED)->first();

        $tds = $details->get('amount')*($pancard ? 5 : 20)/100;
        $admin_charge = 0;
        $service_charge = 0;

        $amount = $details->get('amount') - ($tds + $admin_charge + $service_charge);

        $wallet = Wallet::create([
            'user_id' => $this->id,
            'total' => $details->get('amount'),
            'tds' => $tds,
            'admin_charge' => $admin_charge,
            'service_charge' => $service_charge,
            'amount' => $amount,
            'type' => Wallet::CREDIT_TYPE,
            'income_type' => $details->get('income_type'),
            'remarks' => $details->get('remarks'),
            'created_at' => $details->get('created_at') ? $details->get('created_at') : now()
        ]);

        return $wallet;
    }

}
