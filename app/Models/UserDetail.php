<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\UserDetail
 *
 * @property int $id
 * @property int $user_id
 * @property string $title Mr, Miss, Mrs, Dr., Sri
 * @property string $first_name
 * @property string $last_name
 * @property string|null $image
 * @property string|null $pan_no
 * @property int $gender 1: Male, 2: Female
 * @property string $birth_date
 * @property string|null $nominee_name
 * @property string|null $nominee_relation
 * @property string|null $nominee_birth_date
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $full_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereNomineeBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereNomineeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereNomineeRelation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail wherePanNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereUserId($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserDetail newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserDetail newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserDetail whereApplyPan($value)
 * @property int|null $apply_pan /*1: Yes, 2: No
 */
class UserDetail extends Model
{
    use Eloquence;

    const Yes = 1, No = 2;

    protected $fillable = [
        'user_id', 'title', 'first_name', 'last_name', 'image','apply_pan' ,'pan_no', 'gender', 'birth_date', 'nominee_name', 'nominee_relation', 'nominee_birth_date','nagrikta_card_no'
    ];

    public function getFullNameAttribute()
    {
        return $this->title.' '.$this->first_name.' '.$this->last_name;
    }

    public function getBirthDateAttribute($value)
    {
        return Carbon::parse($value);
    }

    public function getNomineeBirthDateAttribute($value)
    {
        return Carbon::parse($value);
    }
}
