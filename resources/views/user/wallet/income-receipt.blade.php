@extends('user.template.layout')

@section('title', 'Receipt')

@section('content')
    @php
        $f = new \App\Library\CurrencyInWord;
    @endphp
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Receipt</h3>
        </div>
    </div>

    <div class="content-body">
        <section class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <img src="/user-assets/images/company/logo.png" alt="{{ config('project.brand') }}" class="mb-2 mt-2">
                    </div>
                    <div class="col-md-8">
                        <span class="text-capitalize font-medium-5 pull-right">ARASIS HEALTH & BEAUTY LLP</span><br><br>
                        <p class="text-capitalize text-right">
                            C/O DIPESH GURUNG,DHUKURIA,NEAR DHUKURIA CLUB,<br> NEW CHAMTA,MATIGARA,DARJEELING,
                            Darjeeling,West Bengal. <br>Pin Code:{{ '734009.'  }}
                        </p>
                    </div>
                </div>
                <div class="pt-2">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr class="bg-primary text-white">
                                <th colspan="4" class="text-center">INCOME STATEMENT</th>
                            </tr>
                            <tr class="bg-info bg-lighten-4">
                                <th colspan="4" class="text-center">Payout Details </th>
                            </tr>
                            <tr>
                                <th>Payout No: {{ '' }}</th>
                                <th colspan="2">Month Name: {{ $month }}</th>
                                <th>Period: {{ 'N.A' }}</th>
                            </tr>
                            <tr class="bg-info bg-lighten-4">
                                <th colspan="4" class="text-center">Associate Detail</th>
                            </tr>
                            <tr>
                                <th colspan="3">Name : <span class="ml-1">{{ $user->detail->full_name }}</span></th>
                                <th>ID No: {{ $user->tracking_id }}</th>
                            </tr>
                            <tr>
                                <th colspan="4">Address : <span class="ml-1">{{ $user->address->address  }}</span></th>
                            </tr>
                            <tr>
                                <th colspan="2">Mobile No : <span class="ml-1"> {{ $user->mobile }}</span></th>
                                <th>Email : {{ $user->email }}</th>
                                <th></th>
                            </tr>
                            <tr class="bg-info bg-lighten-4">
                                <th colspan="4" class="text-center">Income Details</th>
                            </tr>
                            <tr class="bg-info bg-lighten-3">
                                <th>Earnings</th>
                                <th>Amount In Rs.</th>
                                <th>TDS Deduction Amount</th>
                                <th>Amount In Rs.</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($incomes as $income)
                                <tr>
{{--                                    <td>{{  1 }}</td>--}}
                                    <td>{{ $income['name'] }}</td>
                                    <td>{{ $income['amount'] }}</td>
                                    <td>{{ $income['tds'] }}</td>
                                    <td>{{ $income['amount'] - $income['tds'] }}</td>
                                </tr>
                            @endforeach
                            <tr class="bg-success bg-lighten-2">
                                <th>Total Earnings: </th>
                                <th >{{ number_format($incomes->sum('amount'), 2) }}/-</th>
                                <th>Total TDS : <span class="pull-right">{{ number_format($incomes->sum('tds'),2) }}/-</span></th>
                                <th></th>
                            </tr>
                            <tr class="bg-success bg-lighten-2" >
                                <th></th>
                                <th></th>
                                <th>Total Amount Payable</th>
                                <th>
                                    <span class="pull-right">{{ number_format($incomes->sum('amount') - $incomes->sum('tds'), 2) }}/-</span>
                                </th>
                            </tr>
                            <tr class="bg-info bg-lighten-4">
                                <th colspan="4" class="text-center">Current Month BV Detail </th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>Self BV</th>
                                <th>Team A-BV</th>
                                <th>Team B-BV</th>
                            </tr>
                            <tr>
                                <th>Total BV</th>
                                <th>{{ $current_month_self_bv ? : 0 }}</th>
                                <td>{{ $current_month_team_record ? $current_month_team_record->lft : 0 }}</td>
                                <td>{{ $current_month_team_record ? $current_month_team_record->rgt : 0 }}</td>
                            </tr>
                            <tr class="bg-info bg-lighten-4">
                                <th colspan="4" class="text-center">Point Details</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>POINT</th>
                                <th>COMMISSION RATE</th>
                                <th>TOTAL</th>
                            </tr>
                            <tr>
                                <th>Performance Bonus</th>
                                <th>{{ $point_detail->performance_bonus }}</th>
                                <th>1000</th>
                                <th>{{ $point_detail->performance_bonus * 1000 }}</th>
                            </tr>
                            <tr>
                                <th>Leadership Bonus</th>
                                <th>{{ $point_detail->leadership_bonus }}</th>
                                <th>5000</th>
                                <th>{{ $point_detail->leadership_bonus * 5000 }}</th>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-md-12 text-center">
                    <a href="javascript:window.print()" class="btn btn-instagram btn-lg my-1 btn-print"><i class="la la-print"></i>
                        Print Receipt
                    </a>
                    <p>
                        <small>This is Computer generated RECEIPT, Not need any Signature or Stamp</small>
                    </p>
                </div>
            </div>
        </section>
    </div>
@stop