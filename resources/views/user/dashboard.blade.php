@extends('user.template.layout')

@section('title', 'Dashboard')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Dashboard</h3>
        </div>
    </div>
    <div class="content-body">

        {{--Include Upgrade / Activate Account Widget Here--}}
        {{--@include('user.upgrade.widget')--}}

        <div class="row match-height">
            <div class="col-xl-3 col-lg-6 col-md-12">
                <a href="{{ route('user-business-binary-tree') }}">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-top">
                                        <i class="la la-sitemap danger font-large-4"></i>
                                    </div>
                                    <div class="media-body text-right align-self-bottom mt-3">
                                        <span class="d-block mb-1 font-medium-1">My Team</span>
                                        <h1 class="info mb-0">{{ number_format($children) }}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12">
                <a href="{{ route('user-business-sponsors') }}">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-top">
                                        <i class="la la-users warning font-large-4"></i>
                                    </div>
                                    <div class="media-body text-right align-self-bottom mt-3">
                                        <span class="d-block mb-1 font-medium-1">My Direct</span>
                                        <h1 class="info mb-0">{{ number_format($sponsors) }}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-3 col-lg-6 col-md-12">
                <a href="{{ route('user-support-view') }}">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-top">
                                        <i class="la la-support purple font-large-4"></i>
                                    </div>
                                    <div class="media-body text-right align-self-bottom mt-3">
                                        <span class="d-block mb-1 font-medium-1">Support</span>
                                        <h1 class="info mb-0">{{ $open_supports }}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-3 col-lg-6 col-md-12">
                <div class="card {{ $user->paid_at ? 'bg-success' : 'bg-danger' }}">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-top">
                                    <i class="la {{ $user->paid_at ? 'la-check-circle' : 'la-times-circle' }} text-white font-large-4"></i>
                                </div>
                                <div class="media-body text-right align-self-bottom ">
                                    @if($user->paid_at)
                                        <span class="d-block mb-1 font-medium-2 text-white">Account is Activated</span>
                                        <small class="d-block text-white font-small-1 text-uppercase">Keep Shopping for high income</small>
                                    @else
                                        <span class="d-block mb-1 mt-0 font-medium-2 text-white">Account is Inactive</span>
                                        <small class="d-block text-white font-small-1 text-uppercase">Please Shopping with minimum Rs. 1500 for activate your account</small>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card dashboard-account-card p-0">
                    <div class="card-header"><img class="img-fluid" src="/user-assets/images/backgrounds/dashboard-account-card.jpg" alt=""></div>
                    <div class="card-profile"><img class="rounded-circle" src="{{ $user->detail->image ? env('USER_PROFILE_IMAGE_URL').$user->detail->image : '/user-assets/images/icons/user-tie.svg' }}" alt=""></div>
                    <div class="text-center profile-details mt-5">
                        <h5>{{ str_limit(Session::get('user')->detail->full_name, 25) }}</h5>
                        <h6 class="text-bold-700 text-danger">ID: {{ Session::get('user')->tracking_id }}</h6>
                    </div>
                    <div class="card-footer row">
                        <div class="col-12 text-center">
                            <div class="text-bold-700 mb-1">My Referral Links</div>
                            <div class="text-center">
                                <button onclick="INGENIOUS.copyToClipboard('.dashboard-referral', 'Your Referral Link has been copied')" class="btn btn-primary btn-sm dashboard-referral" data-clipboard-text="{{ Session::get('user')->referral_link }}"><i class="ft-copy"></i> Copy </button>
                                <a href="{{ \Share::load(Session::get('user')->referral_link, 'Join Me & Earn Income')->facebook() }}" class="btn btn-facebook btn-sm"><i class="ft-facebook"></i></a>
                                <a href="{{ \Share::load(Session::get('user')->referral_link, 'Join Me & Earn Income')->twitter() }}" class="btn btn-twitter btn-sm"><i class="ft-twitter"></i></a>
                                <a href="{{ \Share::load(Session::get('user')->referral_link, 'Join Me & Earn Income')->whatsapp() }}" class="btn btn-whatsapp btn-sm"><i class="la la-whatsapp"></i></a>
                                <a href="{{ \Share::load(Session::get('user')->referral_link, 'Join Me & Earn Income')->telegramMe() }}" class="btn btn-info btn-sm"><i class="la la-telegram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header p-1">
                        <h4 class="card-title float-left">My KYC Status</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-footer text-center p-1">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-4 border-right-blue-grey border-right-lighten-5 text-center">
                                    <p class="black mb-0">PAN CARD</p>
                                    <div class="font-medium-2 mt-1">
                                        @if($user_status->pancard == \App\Models\UserStatus::KYC_PENDING)
                                            <span class="badge bg-pink"><i class="la la-exclamation-triangle"></i> PENDING</span>
                                        @elseif($user_status->pancard == \App\Models\UserStatus::KYC_INPROGRESS)
                                            <span class="badge badge-warning"><i class="la la-refresh"></i> In Progress</span>
                                        @elseif($user_status->pancard == \App\Models\UserStatus::KYC_VERIFIED)
                                            <span class="badge badge-success"><i class="la la-check-circle"></i> Verified </span>
                                        @else
                                            <span class="badge badge-danger"><i class="la la-close"></i> Rejected</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-4 border-right-blue-grey border-right-lighten-5 text-center">
                                    <p class="black mb-0">BANK</p>
                                    <div class="font-medium-2 mt-1">
                                        @if($user_status->bank_account == \App\Models\UserStatus::KYC_PENDING)
                                            <span class="badge bg-pink"><i class="la la-exclamation-triangle"></i> PENDING</span>
                                        @elseif($user_status->bank_account == \App\Models\UserStatus::KYC_INPROGRESS)
                                            <span class="badge badge-warning"><i class="la la-refresh"></i> In Progress</span>
                                        @elseif($user_status->bank_account == \App\Models\UserStatus::KYC_VERIFIED)
                                            <span class="badge badge-success"><i class="la la-check-circle"></i> Verified </span>
                                        @else
                                            <span class="badge badge-danger"><i class="la la-close"></i> Rejected</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-4 text-center">
                                    <p class="black mb-0">AADHAR</p>
                                    <div class="font-medium-2 mt-1">
                                        @if($user_status->aadhar_card == \App\Models\UserStatus::KYC_PENDING)
                                            <span class="badge bg-pink"><i class="la la-exclamation-triangle"></i> PENDING</span>
                                        @elseif($user_status->aadhar_card == \App\Models\UserStatus::KYC_INPROGRESS)
                                            <span class="badge badge-warning"><i class="la la-refresh"></i> In Progress</span>
                                        @elseif($user_status->aadhar_card == \App\Models\UserStatus::KYC_VERIFIED)
                                            <span class="badge badge-success"><i class="la la-check-circle"></i> Verified </span>
                                        @else
                                            <span class="badge badge-danger"><i class="la la-close"></i> Rejected</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row match-height">
                    <div class="col-xl-6 col-lg-6">
                        <div class="card">
                            <div class="card-header p-1">
                                <h4 class="card-title float-left">My BV Details</h4>
                            </div>
                            <div class='table-responsive table-bordered'>
                                <table class='table table-bordered  custom-table'>
                                    <tr class='bg-danger text-white'>
                                        <td colspan='4' class='text-center'><b>Current Month BV</b></td>
                                    </tr>
                                    <tr>
                                        <td>SELF</td>
                                        <td>Left</td>
                                        <td>Right</td>
                                    </tr>
                                    <tr>
                                        <td>{{ $current_month_self_bv ? : 0 }}</td>
                                        <td>{{ $current_month_team_record ? $current_month_team_record->lft : 0 }}</td>
                                        <td>{{ $current_month_team_record ? $current_month_team_record->rgt : 0 }}</td>
                                    </tr>
                                    <tr class='bg-primary text-white'>
                                        <td colspan='4' class='text-center'><b>Previous Month BV</b></td>
                                    </tr>
                                    <tr>
                                        <td>SELF</td>
                                        <td>Left</td>
                                        <td>Right</td>
                                    </tr>
                                    <tr>
                                        <td>{{ $previous_month_self_bv ? : 0 }}</td>
                                        <td>{{ $previous_month_team_record ? $previous_month_team_record->lft : 0 }}</td>
                                        <td>{{ $previous_month_team_record ? $previous_month_team_record->rgt : 0 }}</td>
                                    </tr>
                                    <tr class='bg-info text-white'>
                                        <td colspan='4' class='text-center'><b>Total BV</b></td>
                                    </tr>
                                    <tr>
                                        <td>SELF</td>
                                        <td>Left</td>
                                        <td>Right</td>
                                    </tr>
                                    <tr>
                                        <td>{{ $total_self_bv ? : 0 }}</td>
                                        <td>{{ $total_team_record ? $total_team_record->sum('lft') : 0 }}</td>
                                        <td>{{ $total_team_record ? $total_team_record->sum('rgt') : 0 }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                    <div class="card">
                        <div class="card-header p-1">
                            <h4 class="card-title float-left text-logo-blue">My Business</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-footer pr-1 pl-1 pt-1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="list-group ">
                                                    <li class="list-group-item bg-blue-grey bg-lighten-4">
                                                        <strong class="text-dark">My Team</strong>
                                                        <span class="pull-right text-logo-blue">{{ number_format($children) }} </span>
                                                    </li>
                                                    <li class="list-group-item bg-blue-grey bg-lighten-4">
                                                        <strong class="text-dark">My Direct</strong>
                                                        <span class="pull-right text-logo-blue">{{ number_format($sponsors) }} </span>
                                                    </li>
                                                    <li class="list-group-item bg-blue-grey bg-lighten-4">
                                                        <strong class="text-dark">Active Directs  </strong>
                                                        <span class="pull-right text-logo-blue">{{ $active_sponsors }}</span>
                                                    </li>


                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop