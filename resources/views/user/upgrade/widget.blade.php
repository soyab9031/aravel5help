<div class="row">
    <div class="col-12">
        <div class="card bg-gradient-directional-danger">
            <div class="card-header bg-hexagons-danger">
                <h4 class="card-title white">Upgrade Your Account</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content collapse show bg-hexagons-danger">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn btn-white danger box-shadow-1 round btn-min-width" href="{{ route('user-upgrade-account') }}">
                                Upgrade Account <i class="ft-arrow-up-right pl-1"></i>
                            </a>
                        </div>
                        <div class="col-md-6 text-right mt-1">
                            <h3 class="font-large-2 white">Hurry</h3>
                            <h6 class="mt-1">
                                <span class="text-muted white">Get Ultimate Benefits of {{ config('project.brand') }}</span>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>