@if($user)
    <a href="javascript:void(0)" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-html="true" title="" data-content="@include('user.business.binary.tree-component-content', ['user' => $user])">
        <img src="{{ $user->treeStatus() }}" width="50">
    </a>
    <h6 class="mt-1" style="font-size: 0.8rem;">{{ $user->detail->full_name }} </h6>
    <h6 style="font-size: 0.8rem;">
        @if($level == 0)
            <a href="javascript:void(0)">{{ $user->tracking_id }}</a>
        @else
            <a href="{{ route('user-business-binary-tree', ['tracking_id' => $user->tracking_id]) }}"> {{ $user->tracking_id }}</a>
        @endif
    </h6>
@else
    <a href="{{ $parent ? ($leg == 'L' ? $parent->referral_link . '&leg=L' : $parent->referral_link . '&leg=R') : 'javascript:void(0)' }}" target="{{ $parent ? '_blank' : null }}" title="{{ $parent ? 'Click to Join' : null }}">
        <img src="/user-assets/images/tree/empty.svg" width="50">

        @if($level == 1)
            <div class="line down" style="height: 3.5rem;"></div>
        @elseif($level == 2)
            <div class="line down" style="height: 3.5rem;"></div>
        @endif
    </a>
@endif