@extends('user.template.registration.layout')

@section('title', 'Registration Overview')

@section('content')
    <div class="pt-2 pb-3">
        <div class="col-12 d-flex align-items-center justify-content-center">

            <div class="col-md-8 col-12 box-shadow-2 p-0">

                <form action="" method="post" id="overview_form">
                    {{ csrf_field() }}
                    <div class="card border-grey border-lighten-3 px-1 py-1">
                        <div class="card-header border-0 text-center">
                            <img src="/user-assets/images/company/logo.png" alt="{{ config('project.brand') }}">
                            <h4 class="card-title font-medium-4 text-danger mt-1">
                                Registration Overview
                            </h4>
                        </div>
                        <div class="card-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Sponsor Upline: <span class="pull-right">{{ $session_detail->sponsor_user->detail->full_name }} ({{ $session_detail->sponsor_user->tracking_id }})</span>
                                </li>
                                <li class="list-group-item bg-danger text-white">
                                    Selected Leg: <span class="pull-right">{{ $session_detail->leg == 'L' ? 'Left' : 'Right' }} Leg</span>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                        <div class="card-header border-0">
                            <div class="font-medium-4 text-center">
                                User / Member Details
                            </div>
                        </div>
                        <div class="card-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Name:
                                    <span class="text-primary pull-right">
                                        {{ $session_detail->title }} {{ $session_detail->first_name }} {{ $session_detail->last_name }}
                                    </span>
                                </li>
                                <li class="list-group-item">
                                    Email: <span class="text-primary pull-right"> {{ $session_detail->email }}</span>
                                </li>
                                <li class="list-group-item">
                                    Mobile: <span class="text-primary pull-right"> {{ $session_detail->mobile }}</span>
                                </li>
                                <li class="list-group-item">
                                    Birth date: <span class="text-primary pull-right"> {{ $session_detail->birth_date }}</span>
                                </li>
                                <li class="list-group-item">
                                    Address: <span class="text-primary pull-right"> {{ $session_detail->address }}</span>
                                </li>
                                <li class="list-group-item">
                                    City: <span class="text-primary pull-right"> {{ $session_detail->city }}</span>
                                </li>
                                <li class="list-group-item">
                                    District: <span class="text-primary pull-right"> {{ $session_detail->district }}</span>
                                </li>
                                @if(!empty($session_detail->state_id))
                                <li class="list-group-item">
                                    State: <span class="text-primary pull-right"> {{ \App\Models\State::whereId($session_detail->state_id)->first()->name }}</span>
                                </li>
                               @endif
                            </ul>
                            <hr>
                            @if($session_detail->pan_no)
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        Pan No: <span class="text-primary text-bold-600 pull-right"> {{ $session_detail->pan_no }}</span>
                                    </li>
                                </ul>
                                <hr>
                            @endif
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Password: <span class="text-danger text-bold-600 pull-right"> {{ $session_detail->password }}</span>
                                </li>
                            </ul>
                            <div class="row mt-1">
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-glow btn-bg-gradient-x-blue-cyan mr-1 mb-1" onclick="history.go(-2);">
                                        <i class="la la-arrow-circle-o-left"></i> Back
                                    </button>
                                </div>
                                <div class="text-right col-md-6">
                                    <button type="button" class="btn btn-glow btn-bg-gradient-x-red-pink mr-1 mb-1 continue">
                                        Complete Registration <i class="la la-arrow-circle-o-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script type="text/javascript">

        $('.continue').click(function (e) {

            var form = $('#overview_form');
            e.preventDefault();

            $('.back-btn').remove();
            $(this).attr('disabled', true);

            swal('Processing', 'Do not Close or Refresh this page..!!', 'info');
            form.submit();
        });

    </script>
@stop