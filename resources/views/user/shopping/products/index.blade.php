@extends('user.template.layout')

@section('title', 'Products')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Products</h3>
        </div>
    </div>

    <div class="content-body product">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <form action="" method="get">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text primary"><i class="la la-search"></i></span>
                                    </div>
                                    <input type="text" placeholder="Search Products" name="search" class="form-control" value="{{ Request::get('search') }}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text primary"><i class="la la-list"></i></span>
                                    </div>
                                    <select name="category_id" class="form-control">
                                        <option value="">Filter Category</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}" {{ Request::get('category_id') == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-danger">Search</button>
                                @refreshBtn('user')
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @if(count($product_prices) == 0 )
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="text-center">
                            <i class="la la-exclamation-circle text-danger font-large-3"></i>
                            <h1>No Items Available</h1>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            @foreach($product_prices as $product_price)
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="img-thumbnail img-fluid text-center">
                                    <a href="{{ route('user-products-details', ['slug' => $product_price->code]) }}" >
                                        <img src="{{ $product_price->primary_image }}" title="{{ $product_price->product->name }} ({{$product_price->code}})" width="100%">
                                    </a>
                                </div>
                                <div class="mt-1">
                                    <div class="font-medium-3 text-dark text-bold-700 mb-2">{{ str_limit($product_price->product->name, 30) }}</div>
                                    <ul class="list-group">
                                        <li class="list-group-item primary font-medium-1">
                                            MRP <span class="pull-right"> Rs. {{ $product_price->price }}</span>
                                        </li>
                                        <li class="list-group-item">
                                            DP <span class="pull-right"> Rs. {{ $product_price->distributor_price }}</span>
                                        </li>
                                        <li class="list-group-item bg-purple white">
                                            Points <span class="pull-right"> {{ $product_price->points }} (PV/BV)</span>
                                        </li>
                                    </ul>
                                    <div class="text-center mt-1">
                                        <a href="{{ route('user-products-details', ['slug' => $product_price->code]) }}" class="btn btn-sm btn-danger">View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop