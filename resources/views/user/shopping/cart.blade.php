@extends('user.template.layout')

@section('title', 'Shopping Cart')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Shopping Cart</h3>
        </div>
    </div>

    <div class="content-body">
        <section>
            @if(Session::has('cart'))
                <div class="row" id="cartPage">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="row cart-row text-dark" v-for="(item,index) in items">
                                        <div class="col-md-3 text-center">
                                            <img style="max-height: 10rem;max-width : 10rem;" :alt="item.product" :src="item.image"
                                                 onerror="this.onerror=null;this.src='/website-assets/images/no-img-available.jpg'">
                                        </div>
                                        <div class="col-md-6">
                                            @{{ item.product | str_limit(40) }}
                                            (@{{ item.code ? item.code : 'N.A' }})
                                            <div class="cart-price-box mt-1">
                                                <span class="text-danger">Distributor Price: ₹ @{{ item.selling_price }}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="font-weight-bold text-dark hidden-xs">
                                                ₹ @{{ (item.quantity.current * item.selling_price).toFixed(2) }}
                                                <vue-counter-button v-model="item.quantity" min="1" :product-price-id="item.id"></vue-counter-button>
                                                <br>
                                                <a href="javascript:void(0)" @click="removeItem(item.id)" class="font-13 pull-right hidden-xs hidden-sm" style="margin-top: -1rem;">Remove</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <h4>Cart Details</h4>
                                    <div class="form-group mt-2">
                                        <label>Payment Mode</label>
                                        <select class="form-control" name="payment_mode" v-model="payment_mode">
                                            <option value="" selected>Select Payment Mode</option>
                                            @foreach($payment_modes as $payment_mode)
                                                <option value="{{ $payment_mode }}" {{ old('payment_mode') == $payment_mode ? 'selected' : '' }}>
                                                    {{ $payment_mode }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Reference Number</label>
                                        <input type="text" name="reference_number" placeholder="Enter Reference Number" class="form-control" v-model="reference_number">
                                    </div>
                                    <fieldset class="form-group mt-1">
                                        <label>Upload Cash Receipt | NEFT Transfer Receipt | Cheque Copy Receipt</label>
                                        <vue-image-filer add-more="0" limit="1" base-string="1" v-model="payment_ref_image"></vue-image-filer>
                                        <p class="text-left"><small class="text-muted">Allowed File Formats : .jpg, .jpeg, .png and Maximum File Size Allowed : 2MB</small></p>
                                    </fieldset>
                                    <div class="form-group">
                                        <label>Your Message</label>
                                        <textarea class="form-control" v-model="remarks" required></textarea>
                                    </div>
                                    <ul class="list-group mt-2">
                                        <li class="list-group-item bg-purple text-white">
                                            Total BV <span class="pull-right"> @{{ total_bv }}</span>
                                        </li>
                                        <li class="list-group-item text-bold text-danger">
                                            Total <span class="pull-right">₹ @{{ parseFloat(total_amount) }}</span>
                                        </li>
                                    </ul>
                                    <div class="text-center mt-1">
                                        <button type="button" class="btn btn-danger" @click="placeOrder">
                                            Place Order
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body text-center">
                                    <img src="/user-assets/images/icons/shopping.svg" alt="" style="width: 150px;">
                                    <div class="mt-1">
                                        You do not have any items in Cart
                                    </div>
                                    <a href="{{ route('user-products-view') }}" class="btn btn-danger mt-1">Start Shopping</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </section>
    </div>
@stop

@section('page-javascript')
    <script>

        Vue.prototype.$http = axios;

        new Vue({
            el: '#cartPage',
            data: {
                items: {!! json_encode($items) !!},
                total_bv: 0,
                total_amount: 0,
                remarks: '',
                payment_mode:'',
                reference_number:'',
                payment_ref_image:null
            },
            watch: {
                items: {
                    handler: function () {
                        this.cartManager();
                    },
                    deep: true
                }
            },
            methods: {
                cartManager: function () {

                    this.total_amount = figureRound(_.sumBy(this.items, item => {
                        return parseFloat(item.quantity.current) * parseFloat(item.selling_price);
                    }), 2);

                    this.total_bv = figureRound(_.sumBy(this.items, item => {
                        return parseFloat(item.quantity.current) * parseFloat(item.bv);
                    }), 2);

                },
                removeItem: function (item_id) {

                    let self = this;

                    swal({
                        title: `Are you sure?`,
                        text: 'Are You sure to remove this item ?',
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((confirm) => {

                        if (confirm) {

                            INGENIOUS.blockUI(true);

                            self.$http.get('{{ route("user-cart-remove-item") }}', {
                                params: {
                                    removeAllQty: true,
                                    product_price_id: item_id
                                }
                            }).then(response => {

                                if (response.data.status)
                                {
                                    self.items = _.chain(self.items).reject(item => {
                                        return parseInt(item.id) === parseInt(item_id);
                                    }).value();

                                    if (parseInt(response.data.total_items) === 0) {
                                        location.reload();
                                    }
                                    else {
                                        INGENIOUS.blockUI(false);
                                    }
                                }
                                else {
                                    INGENIOUS.blockUI(false);
                                    swal("Oops", response.data.message, "error");
                                }
                            })

                        }
                    });

                },
                placeOrder: function () {

                    if(this.total_amount < 1500){
                        swal('Minimum shopping amount 1500.00/-','Minimum 1500  Rupees shopping is required','error');
                        return false;
                    }

                    if(!this.payment_ref_image){
                        swal('Payment Receipt Require','Please Upload Payment receipt for process order','error');
                        return false;
                    }

                    let self = this;

                    swal({
                        title: `Are you sure?`,
                        text: 'Are You Sure to Place Order ?',
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((confirm) => {

                        if (confirm)
                        {
                            INGENIOUS.blockUI(true);

                            self.$http.post('{{ route("user-shopping-order-place") }}',{
                                items: self.items,
                                remarks: self.remarks,
                                total_bv: self.total_bv,
                                total_amount: self.total_amount,
                                payment_mode: self.payment_mode,
                                reference_number: self.reference_number,
                                payment_ref_image: self.payment_ref_image
                            }).then(response => {

                                if (response.data.status) {
                                    swal('success','Your Order Successfully Placed','success');
                                    window.location = response.data.route;
                                }
                                else {
                                    INGENIOUS.blockUI(false);
                                    swal('Oops', response.data.message, 'error');
                                }
                            });
                        }
                    });


                }
            },
            mounted: function () {
                this.cartManager();
            }
        });

        function figureRound(value, decimals) {
            return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
        }

    </script>

@stop

@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
@stop

@section('page-css')
    <style>
        .cart-row {
            border-bottom: 1px solid #c7c7c7;
            padding: 23px;
            position: relative;
        }
    </style>
@stop