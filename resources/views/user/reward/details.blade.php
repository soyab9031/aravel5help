@extends('user.template.layout')

@section('title', 'My Reward Achievement')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Achievement</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 offset-md-4 bg-warning text-center p-2" style="border-radius: 50%;">
                                <h1>Prakash Malla</h1>
                            </div>
                            <div class="col-md-12  rounded ">
                                <hr>
                            </div>
                            <div class="col-md-12 bg-light rounded ">
                              <div class="row">
                                  <div class="col-md-3" >
                                    <div class="card mt-1 rounded border shadow-sm  border-light"  style="  box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important; background: linear-gradient(to top left, #c0c0c0 0%, #ffffff 100%);">
                                        <div class="card-body">

                                            <h2><i class="las la-award"></i> Silver</h2>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="card mt-1 rounded border shadow-sm  border-light"  style="  box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important;  background: linear-gradient(to bottom right, #ffd700 0%, #ffffff 100%);">
                                          <div class="card-body">

                                              <h2><i class="las la-medal"></i> Gold</h2>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3" >
                                      <div class="card mt-1 rounded border shadow-sm  border-light"  style=" box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important; background: linear-gradient(to bottom left, #e5e4e2 27%, #ffffff 69%);">
                                          <div class="card-body">

                                              <h2><i class="las la-star-half"></i> Platinum</h2>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="card mt-1 rounded border shadow-sm  border-light"  style=" box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important; background: linear-gradient(to right, #e5e4e2 44%, #ffffcc 97%);">
                                          <div class="card-body">

                                                <h2><i class="las la-star-half-alt"></i> Star Platinum</h2>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="card mt-1 rounded border shadow-sm  border-light"  style=" box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important; background: linear-gradient(to bottom right, #009933 3%, #99ffcc 64%);">
                                          <div class="card-body">

                                              <h2><i class="las la-crown"></i> Emerald</h2>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="card mt-1 rounded border shadow-sm  border-light"  style=" box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important;  background: linear-gradient(to bottom right, #339966 0%, #99ff99 63%);">
                                          <div class="card-body">

                                              <h2><i class="las la-star"></i> Star Emerald</h2>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="card mt-1 rounded border shadow-sm  border-light"  style=" box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important; background: linear-gradient(to bottom right, #ccffff 0%, #ffffff 63%);">
                                          <div class="card-body">

                                              <h2><i class="las la-star-of-david"></i> Diamond</h2>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="card mt-1 rounded border shadow-sm  border-light"  style=" box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important;  background: linear-gradient(to bottom left, #000000 -2%, #99ffcc 73%);">
                                          <div class="card-body">

                                              <h2 class="text-danger font-weight-bold"><i class="las la-trophy"></i> Black Diamond</h2>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop