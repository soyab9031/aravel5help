@extends('user.template.layout')

@section('title', 'My Reward Achievement')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Reward Achievement</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Reward Name</th>
                                    <th width="15%">Pair</th>
                                    <th>Reward</th>
                                    <th>Status</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($rewards as $index => $reward)
                                    <tr>
                                        <td>{{ $index+1 }}</td>
                                        <td class="text-danger">
                                            {{ $reward->name }}
                                        </td>
                                        <td>{{ $reward->pair }}</td>
                                        <td>{{ $reward->reward }}</td>
                                        <td>
                                            @if($reward->achiever_date)
                                                @if($reward->achiever_status == 1)
                                                    <span class="badge badge-success">Achieved</span>
                                                    <br> Date :{{ $reward->achiever_date->format('M d, Y') }}
                                                @else
                                                    <span class="badge badge-danger">FAILED</span>
                                                @endif
                                            @else
                                                <span class="badge badge-warning">PENDING</span>
                                            @endif
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop