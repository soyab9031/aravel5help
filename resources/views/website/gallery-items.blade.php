@extends('website.template.layout')

@section('page-title','Gallery Items')

@section('page-content')

    <div class="breadcrumb-area bc_type t3" style="background-image: url(/website-assets/images/parallaxbg2.jpg)">
        <div class="container">
            <h2 class="page-title">Gallery Items</h2>
            <ul class="breadcrumb">
                <li><a href="{{ route('website-home') }}">Home</a></li>
                <li class="active">{{ $gallery->name }}</li>
            </ul>
        </div>
    </div>
    <section class="section">
        <div class="container">
            <div class="row gallery-3cols">
                @foreach($gallery->items as $item)
                    <div class="col-xs-12 col-sm-6 col-md-3 text-center executive ">
                        <div class="portfolio-item gallery-items">
                            <div class="portfolio-head">
                                <img src="{{ $item->image ? env('GALLERY_IMAGE_URL').$item->image : '/website-assets/images/image-not-found.png'  }}" alt="portfolio" class="img-responsive">
                                <div class="caption-top">
                                    <a data-lightbox="gallery" href="{{ $item->image ? env('GALLERY_IMAGE_URL').$item->image : '/website-assets/images/image-not-found.png'  }}">open</a>
                                </div>
                            </div>
                            <div class="entry-meta">
                                <a href="javascript:void(0)">
                                    <h5 class="gallery-font">
                                        {{ $item->name }}
                                    </h5></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@stop