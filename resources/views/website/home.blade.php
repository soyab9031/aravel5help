@extends('website.template.layout')

@section('page-title','Home')

@section('page-content')
    <div class="swiper-container mySwiper">
        <div class="swiper-wrapper">
            @foreach($banners as $index => $banner)
                <div class="swiper-slide">
                    <img class="img-fluid rounded" src="{{ env('BANNER_IMAGE_URL').$banner->image   }}">
                </div>
            @endforeach
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-pagination"></div>
    </div>

    <section class="section" style="padding:100px 0px 20px !important;">
        <div class="container">
            <div class="row service-3">
                <div class="col-md-6">
                    <header class="section-head">
                        <h2>WELCOME TO {{ config('project.company') }} Health & Beauty</h2>
                        <p>{{ config('project.company') }} isn't a bank but a gifting platform that is made up of diverse people with a common goal to empower one another. You provide assistance to your fellow member through your financial gift.</p>
                        <p>{{ config('project.company') }} says that it is never a big deal to find just one friend from your huge friends or family circles. Because cash investment understand the structural figure of your social circles in this 21st century.</p>
                    </header>
                </div>
                <div class="col-md-6">
                    {{--<iframe width="1184" height="450" src="https://www.youtube.com/embed/KjAQrRgAcao" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>--}}
                    <img src="/website-assets/images/1.jpg" class="img-responsive">
                </div>
            </div>
        </div>
    </section>

    <section style="padding-top: 50px;" class="product-category">
        <div class="promOffer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <div class="promoBlock">
                            <img src="/website-assets/images/collection_01.jpg" alt="">
                            <div class="promoHover verticalMiddle">
                                <div class="verticalInner">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="promoBlock">
                            <img src="/website-assets/images/collection_02.jpg" alt="">
                            <div class="promoHover verticalMiddle">
                                <div class="verticalInner">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="promoBlock">
                            <img src="/website-assets/images/collection_03.jpg" alt="">
                            <div class="promoHover verticalMiddle">
                                <div class="verticalInner">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  <!-- // row -->
            </div>  <!-- // container -->
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row margin_bottom100">
                <div class="col-xs-12">
                    <h3 class="subtitle2">Our Income Types</h3>
                    <div id="spn_sc_slider_1" class="course-carousel spn_sc_slider_1">
                        <div class="edu-course">
                            <figure class="course-thumb">
                                <img src="/website-assets/images/income_types/1.png" alt="">
                            </figure>
                        </div>
                        <div class="edu-course">
                            <figure class="course-thumb">
                                <img src="/website-assets/images/income_types/2.png" alt="">
                            </figure>
                        </div>
                        <div class="edu-course">
                            <figure class="course-thumb">
                                <img src="/website-assets/images/income_types/3.png" alt="">
                            </figure>
                        </div>
                        <div class="edu-course">
                            <figure class="course-thumb">
                                <img src="/website-assets/images/income_types/4.png" alt="">
                            </figure>
                        </div>
                        <div class="edu-course">
                            <figure class="course-thumb">
                                <img src="/website-assets/images/income_types/5.png" alt="">
                            </figure>
                        </div>
                        <div class="edu-course">
                            <figure class="course-thumb">
                                <img src="/website-assets/images/income_types/6.png" alt="">
                            </figure>
                        </div>
                        <div class="edu-course">
                            <figure class="course-thumb">
                                <img src="/website-assets/images/income_types/7.png" alt="">
                            </figure>
                        </div>
                        <div class="edu-course">
                            <figure class="course-thumb">
                                <img src="/website-assets/images/income_types/8.png" alt="">
                            </figure>
                        </div>
                        <div class="edu-course">
                            <figure class="course-thumb">
                                <img src="/website-assets/images/income_types/9.png" alt="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="sc_cta primary">
        <div class="container">
            <p>ARE YOU READY FOR JOIN OUR AWESOMENESS Team</p>
            <a href="{{ route('user-register') }}" target="_blank">Join now</a>
        </div>
    </div>

@stop
@section('page-javascript')

    <script type="text/javascript">
        $(document).ready(function() {
            $("#spn_sc_slider_1").owlCarousel({
                nav     : true,
                loop    : true,
                items   : 4,
                dots    : false,
                margin  : 30,
                navText  : ["<span class='icon-arrow-left10'></span>","<span class='icon-uniE936'></span>"],
                responsive : {
                    // breakpoint from 0 up
                    0 : {
                        items     : 1
                    },
                    // breakpoint from 480 up
                    480 : {
                        items     : 2
                    },
                    // breakpoint from 768 up
                    768 : {
                        items     : 3
                    }
                }
            });
            $("#spn_sc_slider_2").owlCarousel({
                nav    : true,
                loop    : true,
                items     : 4,
                margin  : 30,
                dots    : false,
                slideBy    : "page",
                navText  : ["<span class='icon-arrow-left10'></span>","<span class='icon-uniE936'></span>"],
                responsive : {
                    // breakpoint from 0 up
                    0 : {
                        items     : 1
                    },
                    // breakpoint from 480 up
                    480 : {
                        items     : 2
                    },
                    // breakpoint from 768 up
                    768 : {
                        items     : 3
                    }
                }

            });
            $("#spn_sc_slider_3").owlCarousel({
                nav    : true,
                loop    : true,
                items     : 4,
                margin  : 30,
                dots    : false,
                navText  : ["<span class='icon-arrow-left10'></span>","<span class='icon-uniE936'></span>"],
                responsive : {
                    // breakpoint from 0 up
                    0 : {
                        items     : 1
                    },
                    // breakpoint from 480 up
                    480 : {
                        items     : 2
                    },
                    // breakpoint from 768 up
                    768 : {
                        items     : 3
                    },
                    // breakpoint from 1050 up
                    1050 : {
                        items     : 4
                    }
                }
            });
            $("#spn_sc_slider_4").owlCarousel({
                nav    : true,
                loop    : true,
                items     : 4,
                margin  : 30,
                dots    : false,
                slideBy    : "page",
                navText  : ["<span class='icon-arrow-left10'></span>","<span class='icon-uniE936'></span>"],
                responsive : {
                    // breakpoint from 0 up
                    0 : {
                        items     : 1
                    },
                    // breakpoint from 480 up
                    480 : {
                        items     : 2
                    },
                    // breakpoint from 768 up
                    768 : {
                        items     : 3
                    },
                    // breakpoint from 1050 up
                    1050 : {
                        items     : 4
                    }
                }
            });
            $("#spn_sc_slider_5").owlCarousel({
                nav    : true,
                loop    : true,
                items     : 4,
                margin  : 30,
                dots    : false,
                slideBy    : "page",
                navText  : ["<span class='icon-arrow-left10'></span>","<span class='icon-uniE936'></span>"],
                responsive : {
                    // breakpoint from 0 up
                    0 : {
                        items     : 1
                    },
                    // breakpoint from 480 up
                    480 : {
                        items     : 2
                    },
                    // breakpoint from 768 up
                    768 : {
                        items     : 3
                    }
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            // tooltip code
            $(".sc_tooltip").tooltip();
            $(".short_code_image_gallery a[rel^='pixcphoto[gallery]']").prettyPhoto();
            $(".sc_light_box a[rel^='pixcphoto']").prettyPhoto();
        });

        // Coming Soon Page
        $(function() {
            /*$('.coming-wrapper').pagepiling({
             menu: null,
             anchors: ['welcome', 'subscribe'],
             navigation: false
             });*/
        });
    </script>

    <script>
        $('#carousel_service_thumb').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            directionNav: false,
            itemWidth: 250,
            itemMargin: 0,
            asNavFor: '#carousel_service_slider'
        });

        $('#carousel_service_slider').flexslider({
            animation: "fade",
            controlNav: false,
            directionNav: false,
            animationLoop: true,
            slideshow: true,
            sync: "#carousel_service_thumb",
            slideshowSpeed: "3000"
        });
    </script>
    <script>
        var swiper = new Swiper(".mySwiper", {
            spaceBetween: 30,
            centeredSlides: true,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    </script>
@stop
@section('page-css')
    <style>
        .swiper-container {
            width: 100%;
            height: 100%;
        }

        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;

            /* Center slide text vertically */
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
        }

        .swiper-slide img {
            display: block;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
    </style>
@stop