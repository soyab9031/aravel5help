<section id="footer_canvas">
    <section class="footer-widget">
        <div class="container">
            <div class="row with-border">
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <div class="footer-item">
                        <img src="/user-assets/images/company/w-logo.png" alt="{{ config('project.company') }}" >
                        <p>
                            {{ config('project.company') }} Health & beauty that it is never a big deal to find just one friend from your huge friends or family circles. Because cash investment understand the structural figure of your social circles in this 21st century.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <div class="footer-item recent-posts">
                        <h4>Company</h4>
                        <ul>
                            <li><a href="{{route('website-company-about')}}">About Us <i class="fa fa-angle-right"></i></a></li>
                            <li><a href="{{route('website-company-legals')}}">Legals <i class="fa fa-angle-right"></i></a></li>
                            <li><a href="{{route('website-company-vision-mission')}}">Vision Mission <i class="fa fa-angle-right"></i></a></li>
                            <li><a href="{{route('website-company-terms')}}">Company Terms<i class="fa fa-angle-right"></i></a></li>
                            <li><a href="{{route('website-company-faqs')}}">Faqs<i class="fa fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <div class="footer-item recent-posts">
                        <h4>User Links</h4>
                        <ul>
                            <li><a href="{{route('user-register')}}" target="_blank">Sign Up<i class="fa fa-angle-right"></i></a></li>
                            <li><a href="{{route('user-login')}}" target="_blank">Log In<i class="fa fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <div class="footer-item contact-info">
                        <h4>contact info</h4>
                        <ul>
                            <li><i class="fa fa-map-marker"></i><span>ARASIS HEALTH & BEAUTY LLP
                                    <p>C/O DIPESH GURUNG,DHUKURIA,NEAR DHUKURIA CLUB NEW CHAMTA,MATIGARA,DARJEELING,<br>
                                        Darjeeling,West Bengal,734009,India.</p></span></li>
                            <li><i class="fa fa-phone"></i><span>(+91) 0000-0000-00</span></li>
                            <li><i class="fa fa-envelope-o"></i><span><a href="mailto:customercare.arasis@gmail.com">customercare.arasis@gmail.com</a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<footer class="footer">
    <div class="container">
        <p class="copyright pull-left item_left">
            © Copyright {{date('Y')}} <a href="{{ env('APP_URL') }} " target="_blank" class="text-uppercase">({{ config('project.company') }} )</a> All rights Reserved
        </p>
        <ul class="pull-right social-links text-center item_right">
            <li><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
            <li><a href="javascript:void(0);"><i class="fa fa-instagram"></i></a></li>
            <li><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
            <li><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
            {{--<li><a href="https://youtube.com/watch?v=KjAQrRgAcao&feature=share"><i class="fa fa-youtube"></i></a></li>--}}
        </ul>
    </div>
</footer>