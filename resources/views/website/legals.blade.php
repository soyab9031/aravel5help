@extends('website.template.layout')

@section('page-title','Legals')

@section('page-content')

    <div class="breadcrumb-area bc_type t3" style="background-image: url(/website-assets/images/parallaxbg2.jpg)">
        <div class="container">
            <h2 class="page-title">Legals</h2>
            <ul class="breadcrumb">
                <li><a href="{{ route('website-home') }}">Home</a></li>
                <li class="active">Legals</li>
            </ul>
        </div>
    </div>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-xs-3 col-md-3 col-lg-3 gallery">
                    <div class="service-item ">
                        <figure class="service-thumb">
                            <img alt="Service Icon" src="/website-assets/images/legals/1.jpeg">
                            <div class="mask">
                                <a data-lightbox="service" href="/website-assets/images/legals/1.jpeg"></a>
                            </div>
                        </figure>
                    </div>
                </div>
                <div class="col-xs-3 col-md-3 col-lg-3 gallery">
                    <div class="service-item ">
                        <figure class="service-thumb">
                            <img alt="Service Icon" src="/website-assets/images/legals/2.jpeg">
                            <div class="mask">
                                <a data-lightbox="service" href="/website-assets/images/legals/2.jpeg"></a>
                            </div>
                        </figure>
                    </div>
                </div>
                <div class="col-xs-3 col-md-3 col-lg-3 gallery">
                    <div class="service-item ">
                        <figure class="service-thumb">
                            <img alt="Service Icon" src="/website-assets/images/legals/3.jpeg">
                            <div class="mask">
                                <a data-lightbox="service" href="/website-assets/images/legals/3.jpeg"></a>
                            </div>
                        </figure>
                    </div>
                </div>
                <div class="col-xs-3 col-md-3 col-lg-3 gallery">
                    <div class="service-item ">
                        <figure class="service-thumb">
                            <img alt="Service Icon" src="/website-assets/images/legals/4.jpeg">
                            <div class="mask">
                                <a data-lightbox="service" href="/website-assets/images/legals/4.jpeg"></a>
                            </div>
                        </figure>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-md-3 col-lg-3 gallery">
                    <div class="service-item ">
                        <figure class="service-thumb">
                            <img alt="Service Icon" src="/website-assets/images/legals/5.jpeg">
                            <div class="mask">
                                <a data-lightbox="service" href="/website-assets/images/legals/5.jpeg"></a>
                            </div>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop