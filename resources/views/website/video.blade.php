<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        * {
            box-sizing: border-box;
        }

        body {
            margin: 0;
            font-family: Arial;
            font-size: 17px;
        }
        h2{
            text-transform: uppercase;
        }
        #myVideo {
            position: fixed;
            right: 0;
            bottom: 0;
            min-width: 100%;
            min-height: 100%;
        }

        .content {
            position: fixed;
            bottom: 0;
            background: rgba(0, 0, 0, 0.5);
            color: #f1f1f1;
            width: 100%;
            padding: 20px;
        }

        #myBtn {
            width: 200px;
            font-size: 18px;
            padding: 10px;
            text-decoration: none;
            border: none;
            background: #264cff;
            color: #fff;
            cursor: pointer;
        }
        video {
            width: 100%;
            height: auto;
        }
        #myBtn:hover {
            background: #ddd;
            color: black;
        }
    </style>
    <link type="text/css" rel="stylesheet" href="/website-assets/css/bootstrap.min.css">
</head>
<body style="background: #000">

<video autoplay muted loop id="myVideo">
    <source src="/user-assets/video/arasis.mp4" type="video/mp4">
    Your browser does not support HTML5 video.
</video>

<div class="content">
    <h2>WELCOME TO {{ config('project.company') }} Health & Beauty</h2>
    <a href="{{ route('website-home') }}" id="myBtn" class="btn ">Skip & Go To Website</a>
</div>


</body>
<script>
    setInterval(function(){ window.location.href='{{ route('website-home') }}' }, 7000);

</script>
<script type="text/javascript" src="/website-assets/js/vendor/jquery-1.11.1.min.js"></script>
