<footer class="footer footer-static footer-light navbar-shadow">
    <div class="clearfix blue-grey lighten-2 text-center mb-0 px-2">
        <span class="d-block d-md-inline-block">{{ date('Y') }}  &copy; Copyright <a class="text-bold-800 grey darken-2">{{ config('project.company') }}</a></span>
    </div>
</footer>