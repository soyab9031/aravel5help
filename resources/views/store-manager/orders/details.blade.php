@extends('store-manager.template.layout')

@section('title', 'Order Details of ' . $order->customer_order_id)

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Order Details of {{ $order->customer_order_id }}</h3>
        </div>
    </div>

    <div class="content-body">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <h5>Order Details</h5>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Order Id: <span class="pull-right">{{ $order->customer_order_id }}</span>
                                </li>
                                <li class="list-group-item">
                                    Order Date: <span
                                            class="pull-right">{{ $order->created_at->format('M d, Y h:i A') }}</span>
                                </li>
                                <li class="list-group-item">
                                    Payment Mode:
                                    <span class="pull-right">
                                        {{ $order->payment_reference['payment_mode'] ? $order->payment_reference['payment_mode'] : 'N.A' }}
                                    </span>
                                </li>
                                <li class="list-group-item">
                                    Payment Reference No:
                                    <span class="pull-right">
                                         {{ $order->payment_reference['reference_number'] ? $order->payment_reference['reference_number'] : 'N.A' }}
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <h5>Order Summary</h5>
                            <ul class="list-group">
                                <li class="list-group-item bg-dark text-white">
                                    Status: <span
                                            class="pull-right">{{ \App\Models\Order::getStatus($order->status) }}</span>
                                </li>
                                <li class="list-group-item">
                                    Total Items: <span class="pull-right">{{ $order->details->sum('qty') }}</span>
                                </li>
                                <li class="list-group-item">
                                    Total BV: <span class="pull-right">{{ $order->total_bv }}</span>
                                </li>
                                <li class="list-group-item">
                                    Amount <span class="pull-right">{{ $order->amount }}</span>
                                </li>
                                <li class="list-group-item text-danger">
                                    Total Amount<span class="pull-right">{{ $order->total }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if($order->status == \App\Models\Order::PLACED)
                <div class="col-md-6 offset-md-3">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <h5 class="text-center">Order Status Update</h5>
                                <hr>
                            </div>
                            <div class="card-body">
                                <form action="" method="post" onsubmit="INGENIOUS.blockUI(true)">
                                    {{ csrf_field() }}
                                    <div class="col-md-8 offset-md-2">
                                        <div class="form-group">
                                            <label>Order Status</label>
                                            <select class="form-control" name="status">
                                                <option value="">Select Status</option>
                                                <option value="3" {{ $order->status == 3 ? 'selected' : '' }}>Approved
                                                </option>
                                                <option value="5" {{ $order->status == 5 ? 'selected' : '' }}>Rejected
                                                </option>
                                            </select>
                                        </div>
                                        <input type="hidden" name="payment_status" value="2">
                                        <div class="text-center">
                                            <button class="btn text-center btn-bg-gradient-x-purple-blue" type="submit">
                                                Update
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-content collapse show">
                    <div class="card-body table-responsive">
                        <h5>Order Item Details</h5>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>GST</th>
                                <th>Amount</th>
                                <th>Qty</th>
                                <th>Total BV</th>
                                <th class="text-danger">Total Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order->details as $index => $detail)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>
                                        {{ $detail->product_price->product->name }}
                                        <code class="pull-right">CODE: {{ $detail->product_price->code }}</code>
                                    </td>
                                    <td>{{ $detail->gst->percentage }}%</td>
                                    <td>{{ $detail->selling_price }}</td>
                                    <td>{{ $detail->qty }}</td>
                                    <td>{{ $detail->total_bv }}</td>
                                    <td class="text-danger">{{ $detail->total_amount }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop