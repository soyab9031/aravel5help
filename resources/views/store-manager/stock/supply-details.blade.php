@extends('store-manager.template.layout')

@section('title', 'Stock Supply Details # ' . $supply->id)

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Stock Supply Details # {{ $supply->id }}</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <b>Sender:</b>
                                @if($supply->admin_id)
                                    <span class="pull-right text-danger">ADMIN/COMPANY <br> <small>({{ $supply->admin->name }})</small></span>
                                @else
                                    <span class="pull-right text-primary">{{ $supply->senderStore->name }} <br> ({{ $supply->senderStore->tracking_id }})</span>
                                @endif
                            </li>
                            <li class="list-group-item">
                                <b>Supply ID:</b> <span class="pull-right">{{ $supply->id }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <b>Date:</b> <span class="pull-right">{{ $supply->created_at->format('M d, Y h:i A') }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Delivery Method:</b> <span class="pull-right">{{ $supply->delivery_type_name }}</span>
                            </li>
                            @if($supply->delivery_type <> 1)
                                <li class="list-group-item">
                                    <b>Transport/ Courier:</b> <span class="pull-right">{{ $supply->courier_name }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Docket Number:</b> <span class="pull-right">{{ $supply->courier_docket_number }}</span>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Amount</th>
                                <th>Opening Qty</th>
                                <th>Qty</th>
                                <th>Closing Qty</th>
                                <th class="text-right">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($supply->details as $index => $detail)
                                @php
                                    $stock_transaction = collect($supply->stockTransactions)->where('product_price_id', $detail->product_price_id)->first()
                                @endphp
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>
                                        {{ $detail->product_price->product->name }}
                                        <code class="pull-right">Code: {{ $detail->product_price->code }}</code>
                                    </td>
                                    <td>{{ $detail->distributor_price }}</td>
                                    <td>{{ $stock_transaction ? $stock_transaction->opening : 'N.A' }}</td>
                                    <td>{{ $detail->qty }}</td>
                                    <td>{{ $stock_transaction ? $stock_transaction->closing : 'N.A' }}</td>
                                    <td class="text-right"> {{ number_format($detail->total_amount, 2) }}</td>
                                </tr>
                            @endforeach
                            <tr class="text-white bg-danger bg-darken-1">
                                <td colspan="4" class="text-right">Total</td>
                                <td>{{ collect($supply->details)->sum('qty') }}</td>
                                <td></td>
                                <td class="text-right">{{ number_format(collect($supply->details)->sum('total_amount'), 2) }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop