@extends('store-manager.template.layout')

@section('title')
    My Stocks
@stop

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Store Stocks</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form action="" method="get">
                                <div class="row mb-2">
                                    <div class="col-md-3 mt-1">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="la la-search"></i></span>
                                            </div>
                                            <input type="text" placeholder="Search Order..." name="search" class="form-control" value="{{ Request::get('search') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4 mt-1">
                                        <button class="btn btn-primary"> Search </button>
                                        <a href="{{ route('store-stock-view', ['download' => 'yes','search' => Request::get('search'),]) }}" class="btn btn-dark btn-icon" title="Download">
                                            <i class="la la-cloud-download"></i> <span class="bold">Download</span>
                                        </a>
                                        @refreshBtn('store')
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Product</th>
                                        <th class="bg-danger lighten-1 text-white">Stock</th>
                                        <th class="bg-primary lighten-1 text-white">Minimum Stock</th>
                                        <th>Valuation</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($records) == 0)
                                        <tr>
                                            <td colspan="10" class="text-center">No Stocks Available</td>
                                        </tr>
                                    @endif
                                    @foreach ($records as $index => $record)
                                        <tr>
                                            <td>{{ \App\Library\Helper::tableIndex($records, $index) }}</td>
                                            <td>{{ $record->product_price->product->name }} <br> <code>Code: {{ $record->product_price->code }}</code></td>
                                            <td class="bg-danger bg-lighten-3 text-white">{{ number_format($record->balance) }}</td>
                                            <td class="bg-primary bg-lighten-3 text-dark">
                                                <span>{{ $record->minimum_stock ? $record->minimum_stock->qty : 0}}</span>
                                                <button type="button" class="btn btn-sm btn-icon btn-danger pull-right" data-toggle="modal" data-keyboard="false" data-target="#updateMinimumQty{{ $record->product_price->id }}" title="Edit Minimum Qty of {{ $record->product_price->product->name }}"><i class="la la-edit"></i></button>

                                                <div class="modal fade" id="updateMinimumQty{{ $record->product_price->id }}" tabindex="-1" role="dialog" aria-labelledby="basicModalLabel3" aria-hidden="true">
                                                    <form action="{{ route('store-stock-minimum-qty-update') }}" onsubmit="INGENIOUS.blockUI()">
                                                        {{ csrf_field() }}
                                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title mt-0" id="basicModalLabel3">Update Minimum Quantity </h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="form-group mt-2">
                                                                        <label for="">Product</label>
                                                                        <input type="text" value="{{ $record->product_price->product->name }}" readonly disabled class="form-control">
                                                                    </div>
                                                                    <div class="form-group mt-2">
                                                                        <label for="">Current Minimum Qty</label>
                                                                        <input type="number" name="qty"
                                                                               value="{{ $record->minimum_stock ? $record->minimum_stock->qty : 0 }}" class="form-control">
                                                                    </div>
                                                                    <input type="hidden" name="product_price_id" value="{{ $record->product_price->id }}">
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-danger transferPins">Update</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>

                                            </td>

                                            <td>{{ number_format($record->balance * $record->product_price->distributor_price) }}</td>
                                        </tr>
                                    @endforeach
                                </table>
                                {{ $records->appends(['search' => Request::get('search')])->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop