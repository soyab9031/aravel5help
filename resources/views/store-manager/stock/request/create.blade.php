@extends('store-manager.template.layout')

@section('title', 'Create Stock Request')

@section('content')
    <div class="row" id="stockRequestCreate">
        <div class="col-lg-3 col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5 class=" text-primary">
                        Current Balance :
                        <span class="pull-right text-primary">{{ number_format($wallet_balance, 2) }}/-</span>
                    </h5>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label>Send Request To</label>
                        <vue-select2 :options="request_options" place-holder="Send Request To"
                                     v-model="request_to" @change="getCompanyStock"></vue-select2>
                    </div>
                    <div v-if="request_to == 1">
                        <ul class="list-group">
                            <li class="list-group-item bg-danger text-white">
                                Request To: <span class="pull-right">Company</span>
                            </li>
                        </ul>
                    </div>
                    <div v-if="request_to > 1">
                        <div class="form-group">
                            <label>Select Store</label>
                            <vue-select2 :options="search_stores" place-holder="Select Store" allow-search="0"
                                         v-model="store_id" @change-select2="getStoreWithStock"></vue-select2>
                        </div>
                    </div>
                    <div class="form-group mt-1">
                        <textarea v-model="remarks" class="form-control" cols="10" rows="5"
                                  placeholder="Your Message for Stock Request"></textarea>
                    </div>
                </div>
            </div>
            <div class="card" v-if="order.amount > 0">
                <div class="card-body">
                    <ul class="list-group">
                        <li class="list-group-item bg-amber text-dark">Total Qty: <span
                                    class="pull-right">@{{ order.total_qty }}</span></li>
                        <li class="list-group-item bg-success text-dark">Total BV: <span
                                    class="pull-right">@{{ order.total_bv }}</span></li>
                        <li class="list-group-item">Amount: <span class="pull-right">@{{ order.amount }}</span></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-12">
            <div class="card" v-if="items.length > 0">
                <div class="card-body">
                    <div class="form-group " >
                        <label>Search Product / Item</label>
                        <vue-select2 :options="items" place-holder="Search or Select Item"  empty-after-select="1"
                                     allow-search="0" @change-select2="setProductToOrder"></vue-select2>
                    </div>
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="40%">Item</th>
                            <th width="5%" class="bg-primary text-white">Available <br>Stock</th>
                            <th width="5%" class="bg-danger text-white">My Current <br>Stock</th>
                            <th width="10%">DP Amount</th>
                            <th width="40%">Qty</th>
                            <th width="40%">BV</th>
                            <th width="5%">Total</th>
                            <th width="3%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(order_item, index) of order_items">
                            <td>@{{ index+1 }}</td>
                            <td>
                                @{{ order_item.name | str_limit(35) }} <br> <code>Code: @{{ order_item.code }}</code>
                            </td>
                            <td class="bg-primary-lighter">
                                <span class="font-weight-bold text-black">@{{ order_item.balance }}</span>
                            </td>
                            <td class="bg-danger-lighter">
                                <span class="font-weight-bold text-black">@{{ order_item.store_stock }}</span>
                            </td>
                            <td>
                                <span class="">@{{ parseFloat(order_item.distributor_price).toFixed(2) }}</span>
                            </td>
                            <td>
                                <input type="number" min="1" :max="order_item.balance"
                                       onkeypress="INGENIOUS.numericInput(event, false)"
                                       class="form-control qty-input input-sm" v-model="order_item.selected_qty">
                            </td>
                            <td>
                                <span class="">@{{ parseFloat(order_item.total_bv).toFixed(2) }}</span>
                            </td>
                            <td>
                                <span class="">@{{ parseFloat(order_item.total_amount).toFixed(2) }}</span>
                            </td>
                            <td>
                                <a href="javascript:void(0)" class="btn btn-social-icon btn-xs btn-danger"
                                   title="Remove" @click="removeItem(index)">
                                    <span class="la la-close"></span>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="text-center mt-5" v-if="order.amount > 0">
                        <hr>
                        <button class="btn btn-danger btn-lg" type="button" @click="createStockRequest">Purchase Stock
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('page-javascript')
    <script>
        Vue.prototype.$http = axios;

        new Vue({
            el: '#stockRequestCreate',
            data: {
                wallet_balance: {!! $wallet_balance !!},
                stores: {!! json_encode($stores) !!},
                request_options: {!! json_encode($request_options) !!},
                request_to: '',
                store_id: '',
                store: null,
                items: [],
                remarks: null,
                store_type: '{!! json_encode(Session::get('store')->first()->type) !!}',
                order_items: [],
                order: {
                    amount: 0, total_qty: 0,
                },
            },
            watch: {
                request_to: function () {
                    this.items = [];
                    this.getCompanyStock();
                },
                order_items: {
                    handler() {
                        this.calculateSummary();
                    },
                    deep: true
                },
            },
            methods: {
                getCompanyStock: function () {

                    this.store_id = null;
                    this.store = null;
                    this.items = [];
                    this.order_items = [];

                    if(!this.request_to)
                        return false;

                    if(parseInt(this.request_to) > 1){

                        return false;
                    }

                    INGENIOUS.blockUI(true);

                    this.$http.get('{{ route("api-get-company-stock") }}', {
                    }).then(response => {
                        INGENIOUS.blockUI(false);
                        if (response.data.status) {
                            setTimeout(() => {
                                this.items = response.data.items
                            }, 250);
                        }
                        else {
                            swal('Oops', response.data.message, 'error');
                        }

                    });

                },
                getStoreWithStock: function () {

                    this.store = null;
                    this.items = [];
                    this.order_items = [];

                    INGENIOUS.blockUI(true);

                    this.$http.get('{{ route("api-get-store-with-stock") }}', {
                        params: {
                            store_id: this.store_id
                        }
                    }).then(response => {

                        INGENIOUS.blockUI(false);

                        if (response.data.status) {

                            this.store = response.data.store;

                            setTimeout(() => {
                                this.items = response.data.items
                            }, 250);
                        }
                        else {
                            swal('Oops', response.data.message, 'error');
                        }

                    });

                },
                setProductToOrder: function (selected_item_id) {

                    let item_exists = _.chain(this.order_items).filter(order_item => {
                        return parseInt(order_item.id) === parseInt(selected_item_id)
                    }).head().value();

                    if (!item_exists) {

                        let selected_product = _.chain(this.items).filter(item => {
                            return parseInt(item.id) === parseInt(selected_item_id)
                        }).head().value();

                        if (selected_product) {
                            this.order_items.push(selected_product);
                        }

                    }

                },
                removeItem: function (index) {

                    let self = this;
                    swal({
                        title: "Are you sure?",
                        text: 'Are you confirm to remove this item ?',
                        icon: "info", buttons: true, dangerMode: true
                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            self.$delete(self.order_items, index);
                            swal('Removed', 'Item has been removed form List', 'success');
                        }
                    });
                },
                calculateSummary: function () {

                    this.order.amount = _.sumBy(this.order_items, item => {
                        return _.round((item.selected_qty * item.distributor_price), 2);
                    });

                    this.order.total_bv = _.sumBy(this.order_items, item => {
                        return _.round((item.selected_qty * item.bv), 2);
                    });

                    _.map(this.order_items, item => {
                        item.total_amount = _.round(item.distributor_price * item.selected_qty, 2);
                        return item;
                    });

                    _.map(this.order_items, item => {
                        item.total_bv = _.round(item.bv * item.selected_qty, 2);
                        return item;
                    });

                    this.order.total_qty = _.sumBy(this.order_items, item => {
                        return parseInt(item.selected_qty);
                    });

                },
                createStockRequest: function () {

                    if(parseInt(this.request_to) === 1 && this.order.amount < 5000){
                        swal('Error', 'Minimum Order amount is 10000 in company order','error');
                        return false;
                    }

                    if(this.wallet_balance < this.order.amount){
                        swal('Error', 'You don`t have enough balance for purchase stock','error');
                        return false;
                    }

                    let self = this;

                    swal({
                        title: "Are you sure?",
                        text: 'Are you confirm to Create Stock Request ?',
                        icon: "info",
                        buttons: true,
                        dangerMode: true
                    }).then(function (isConfirm) {

                        if (isConfirm) {

                            INGENIOUS.blockUI(true);

                            self.$http.post('{{ route("store-stock-request-create") }}', {
                                order_items: self.order_items,
                                order_details: self.order,
                                remarks: self.remarks,
                                store_id: self.store_id,
                                request_to: self.request_to,
                            }).then(response => {
                                if (response.data.status) {
                                    window.location = response.data.route;
                                }
                                else {
                                    INGENIOUS.blockUI(false);
                                    swal('Oops', response.data.message, 'error');
                                }
                            });
                        }
                    });
                }
            },
            computed: {

                search_stores: function () {
                    return this.stores.map(store => {
                        let type = store.type === 1 ? 'Master' : 'Warehouse';
                        store.value = store.id;
                        store.label = store.label + " (" + type + ")";
                        return store;
                    });
                },
            }
        });
    </script>
@stop