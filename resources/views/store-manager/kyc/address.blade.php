@extends('store-manager.template.layout')

@section('title', 'Address Proof - KYC Update')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">
                Address Proof - KYC Update
            </h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-md-8 offset-md-3 offset-lg-2">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            @if(in_array($store_status->address_proof, [\App\Models\UserStatus::KYC_PENDING, \App\Models\UserStatus::KYC_REJECTED]))
                                <form action="" method="post" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input type="file" id="imageUploader" name="image" data-allowed-file-extensions="jpg png jpeg" data-max-file-size="4M" />
                                    </div>
                                    <div class="form-group">
                                        <select name="type" class="form-control">
                                            <option value="4">Electricity Bill</option>
                                            <option value="5">Telephone Bill</option>
                                        </select>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-danger">Upload</button>
                                        <br>
                                        <small>Jpg, png, jpeg Image Formats are allowed & Maximum Size is 4 MB</small>
                                    </div>
                                </form>
                            @endif
                            @if(count($documents) > 0)
                                <hr>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Type</th>
                                        <th>Image</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($documents as $index => $document)

                                        <tr>
                                            <td>{{ $index +1 }}</td>
                                            <td>{{ $document->created_at->format('M d, Y h:i A') }}</td>
                                            <td>
                                                @if($document->type == 4)
                                                    <span class="badge bg-pink"> Electricity Bill</span>
                                                @elseif($document->type == 5)
                                                    <span class="badge badge-success"> Telephone Bill </span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ env('DOCUMENT_IMAGE_URL') . $document->image  }}" target="_blank" class="btn btn-facebook btn-sm">View Image</a>
                                            </td>
                                            <td>
                                                @if($document->status == \App\Models\UserDocument::PENDING)
                                                    <span class="badge bg-pink"><i class="la la-exclamation-triangle"></i> In Progress</span>
                                                @elseif($document->status == \App\Models\UserDocument::VERIFIED)
                                                    <span class="badge badge-success"><i class="la la-check-circle"></i> Verified </span>
                                                @else
                                                    <span class="badge badge-danger"><i class="la la-close"></i> Rejected</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>
        $('#imageUploader').dropify({
            messages: {'default': 'Upload Your Address Proof Image'},
            tpl: {
                filename: '<p class="dropify-filename">Address Proof</p>',
            }
        });
    </script>
@stop

@section('import-javascript')
    <script src="/plugins/dropify/dropify.min.js"></script>
@stop

@section('import-css')
    <link href="/plugins/dropify/dropify.min.css" type="text/css" rel="stylesheet" />
@stop