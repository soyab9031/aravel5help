@extends('store-manager.template.layout')

@section('title', 'Supply To Other Stores Report')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Supply To Other Stores</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form action="" method="get">
                                <div class="row mb-2">
                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar"></i></span></div>
                                            <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary"> Search </button>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="{{ route('store-report-supply-transfer', ['download' => 'yes', 'dateRange' => Request::get('dateRange')]) }}" class="btn btn-dark" title="Download"><i class="la la-download"></i> <span class="bold">Download</span></a>
                                        @refreshBtn('store')
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th class="bg-danger bg-darken-1 white">Amount</th>
                                        <th>Total Supplies</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($records) == 0)
                                        <tr>
                                            <td colspan="10" class="text-center">No Supply Report Available</td>
                                        </tr>
                                    @endif
                                    @foreach ($records as $index => $record)
                                        <tr>
                                            <td>{{ ($index+1) }}</td>
                                            <td>{{ $record->date->format('M d, Y') }}</td>
                                            <td class="bg-danger bg-lighten-4">{{ number_format($record->amount) }}</td>
                                            <td>{{ number_format($record->supply_count) }}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td class="bg-danger bg-darken-1 white">{{ collect($records)->sum('amount') }}</td>
                                        <td>{{ collect($records)->sum('supply_count') }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="auto-overflow">
                                <canvas id="chart" width="400" height="200"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')

    <script>
        var ctx = document.getElementById('chart');
        new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    @foreach(collect($records) as $record)
                        '{{ $record->date->format('M d') }}',
                    @endforeach
                ],
                datasets: [{
                    label: 'Supplies',
                    data: [{{ implode(collect($records)->pluck('amount')->toArray(), ', ') }}],
                    backgroundColor: 'rgb(255, 99, 132)', borderColor: 'rgb(255, 99, 132)', borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true, precision: 0
                        }
                    }]
                }
            }
        });
    </script>
@stop

@section('import-javascript')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
@stop