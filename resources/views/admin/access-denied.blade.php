@extends('admin.template.layout')

@section('title', 'Access denied')

@section('content')
    <div class="container-fluid container-fixed-lg">
        <div class="card text-center m-t-30">
            <i class="sl-ban p-t-20 text-danger" style="font-size: 5rem;"></i>
            <h1 class="semi-bold">Access Denied</h1>
            <h3 class="semi-bold">Sorry, but we couldn't available...!!</h3>
            <p class="p-b-10">You do not have any access to this page, Contact to Master Admin or  Go to <a href="{{ route('admin-dashboard') }}"> Dashboard ?</a>
            </p>
        </div>
    </div>
@stop