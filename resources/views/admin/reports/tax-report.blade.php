@extends('admin.template.layout')

@section('title', 'Tax Report - Company Order Selling')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Tax Report - Company Order Selling:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group">
                                <select  class="full-width" data-init-plugin="select2" name="month_name">
                                    <option value="">Filter Month</option>
                                    @foreach($month_names as $month_name)
                                        <option value="{{ $month_name }}" {{ Request::get('month_name') == $month_name ? 'selected' : null }}>{{ $month_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <select class="form-control" name="tax_area">
                                    <option value="">Tax Area</option>
                                    <option value="1" {{ Request::get('tax_area') == 1 ? 'selected' : null }}>Within
                                        State
                                    </option>
                                    <option value="2" {{ Request::get('tax_area') == 2 ? 'selected' : null }}>Outer
                                        State
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <select class="form-control" name="order_from">
                                    <option value="">Select Order From</option>
                                    <option value="1" {{ Request::get('order_from') == 1 ? 'selected' : null }}>Arasis Company</option>
                                    @foreach($stores as $store)
                                        <option value="{{ $store->id }}" {{ Request::get('order_from') == $store->id ? 'selected' : null }}>
                                            {{ $store->name }} (TID : {{ $store->tracking_id }})
                                            (Type : {{ $store->type == 1  ? 'Master' : ($store->type == 2 ? 'Mini' : 'Warehouse')}})
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-danger btn-sm"> Get Report </button>
                            @if(count($records) > 0)
                                <a href="{{ route('admin-report-gst-report', ['download' => 'yes', 'month_name' => Request::get('month_name'), 'store_id' => Request::get('store_id'), 'tax_area' => Request::get('tax_area'),'order_from' => Request::get('order_from')]) }}" class="btn btn-info btn-sm" title="Download">
                                    <i class="fa fa-cloud-download"></i> <span class="bold">Download</span>
                                </a>
                            @endif
                        </div>
                        <div class="col-md-1">
                            @refreshBtn()
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>HSN Code</th>
                            <th>GST Rate</th>
                            <th>Taxable Amount</th>
                            <th>SGST</th>
                            <th>CGST</th>
                            <th>IGST</th>
                            <th>Total Tax</th>
                            <th>Total Amount</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($records as $index => $record)
                            <tr>
                                <td>{{ ($index+1) }}</td>
                                <td>{{ $record->hsn_code }}</td>
                                <td>{{ $record->percentage }}%</td>
                                <td>{{ number_format($record->taxable_amount) }}</td>
                                <td>{{ number_format($record->sgst_amount) }}</td>
                                <td>{{ number_format($record->cgst_amount) }}</td>
                                <td>{{ number_format($record->igst_amount) }}</td>
                                <td>{{ number_format($record->total_tax_amount) }}</td>
                                <td>{{ number_format($record->total_amount) }}</td>
                            </tr>
                        @endforeach
                        <tr class="bg-danger-lighter text-dark">
                            <td colspan="3" class="text-right">Total</td>
                            <td>{{ number_format(collect($records)->sum('taxable_amount')) }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>{{ number_format(collect($records)->sum('total_tax_amount')) }}</td>
                            <td>{{ number_format(collect($records)->sum('total_amount')) }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop