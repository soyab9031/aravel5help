@extends('admin.template.layout')

@section('title', 'Website Inquiry')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Website Inquiry:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Created On</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Message</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($inquiries as $index => $inquiry)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($inquiries, $index) }}</td>
                                <td>{{ $inquiry->created_at->format('d-m-Y')}}</td>
                                <td>{{ $inquiry->name }}</td>
                                <td>{{ $inquiry->email }}</td>
                                <td>{{ $inquiry->contact }}</td>
                                <td><div class="message-box">{{ $inquiry->message }}</div></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $inquiries->links() }}
                </div>
            </div>
        </div>
    </div>


    @stop