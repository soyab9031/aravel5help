@extends('admin.template.layout')

@section('title', 'System Settings ' . $setting->module_name)

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,System Settings:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body" id="walletTransactionCard">
                        <h4>{{ $setting->module_name }}</h4>
                        <form action="" method="post" role="form" id="systemSettingForm">
                            {{ csrf_field() }}
                            @foreach($setting->value as $value)
                                @if($value->input_type == 'select')
                                    <div class="form-group form-group-default form-group-default-select2">
                                        <label>{{ ucwords(str_replace('_', ' ', $value->title)) }}</label>
                                        <select name="{{ $value->title }}" class="full-width" data-init-plugin="select2">
                                            @foreach($value->options as $option)
                                                <option value="{{ $option->value }}" {{ $option->value == $value->value ? 'selected' : null  }}>{{ $option->label }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @elseif($value->input_type == 'text')
                                    <div class="form-group form-group-default">
                                        <label>{{ ucwords(str_replace('_', ' ', $value->title)) }}</label>
                                        <input type="text" class="form-control" name="{{ $value->title }}" value="{{ $value->value }}" required autocomplete="off">
                                    </div>
                                @endif
                            @endforeach
                            <div class="text-center">
                                <button type="button" class="btn btn-danger systemSettingBtn"> Update Settings of {{ $setting->module_name }} </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')

    <script>
        $('.systemSettingBtn').click(function (e) {

            e.preventDefault();

            var form = $('#systemSettingForm');
            var self = $(this);

            self.attr('disabled', true);

            swal({
                title: "Are you sure?",
                text: 'Are you confirm to change this System Setting?',
                icon: "info",
                buttons: true,
                dangerMode: true
            }).then( function (isConfirm) {

                if (isConfirm) {

                    swal('Processing', 'Do not Close or Refresh this page..!!', 'info');
                    form.submit();
                }
                else {
                    self.attr('disabled', false);
                }

            });
        });
    </script>

@stop