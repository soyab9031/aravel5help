@extends('admin.template.layout')

@section('title', 'Store Wallet Request')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Wallet Request:admin-store-manager-wallet-request-view,Store Wallet Request:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <b>Approved By:</b> <span class="pull-right">{{ $wallet_request->admin ? $wallet_request->admin->name : 'N.A' }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Request ID:</b> <span class="pull-right">{{ $wallet_request->id }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Store:</b> <span class="pull-right">{{ $wallet_request->store->name }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Store ID:</b> <span class="pull-right">{{ $wallet_request->store->tracking_id }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>City:</b> <span class="pull-right">{{ $wallet_request->store->city }}</span>
                            </li>
                            @if($wallet_request->wallet_id)
                                <li class="list-group-item bg-danger text-white">
                                    <b>Instant Transfer:</b> <span class="pull-right">YES</span>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                @if($wallet_request->status == \App\Models\StoreManager\StoreWalletRequest::PENDING)
                    <form action="" method="post" onsubmit="INGENIOUS.blockUI(true)">
                        {{ csrf_field() }}
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group form-group-default form-group-default-select2 m-t-10">
                                    <label>Status</label>
                                    <select name="status" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                        <option value="1" {{ $wallet_request->status == 1 ? 'selected' : '' }}>Pending</option>
                                        <option value="2" {{ $wallet_request->status == 2 ? 'selected' : '' }}>Approved</option>
                                        <option value="3" {{ $wallet_request->status == 3 ? 'selected' : '' }}>Rejected</option>
                                    </select>
                                </div>
                                <div class="form-group form-group-default form-group-default-select2 m-t-10">
                                    <label>Instant Transfer <span class="text-danger">*</span> </label>
                                    <select name="instant_transfer" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                        <option value="NO">NO</option>
                                        <option value="YES">YES</option>
                                    </select>
                                </div>
                                <div class="text-center">
                                    <small class="text-danger text-center">If Yes & Approve then Money Rs. <b>{{ number_format($wallet_request->amount) }}</b> will be transfer immediately into Store Wallet</small>
                                </div>
                                <div class="form-group form-group-default m-t-10">
                                    <label>Remarks or Message <span class="text-danger">*</span></label>
                                    <textarea name="remarks" class="form-control" cols="30" rows="10" required>{{ $wallet_request->remarks }}</textarea>
                                </div>
                                <div class="text-center m-t-10">
                                    <button class="btn btn-theme btn-lg">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                @else
                    <div class="card">
                        <div class="card-body">
                            @if($wallet_request->status == \App\Models\StoreManager\StoreWalletRequest::APPROVED)
                                <div class="alert alert-info">
                                    Wallet Request is Approved on {{ $wallet_request->updated_at->format('M d, Y h:i A') }}
                                </div>
                            @else
                                <div class="alert alert-danger">
                                    Wallet Request is Rejected on {{ $wallet_request->updated_at->format('M d, Y h:i A') }}
                                </div>
                            @endif
                            <hr>
                            <b>Your Message</b>: {{ $wallet_request->remarks }}
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th width="15%">Amount</th>
                                <th>Details</th>
                                <th>Deposited On</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $wallet_request->created_at->format('M d, Y h:i A') }}</td>
                                <td class="font-small-3">{{ number_format($wallet_request->amount, 2) }}</td>
                                <td>
                                    {{ $wallet_request->bank_name }} - {{ $wallet_request->payment_mode }}<br>
                                    <i>{{ $wallet_request->reference_number }}</i>
                                    @if($wallet_request->image)
                                        <br>
                                        <a href="{{ env('PAYMENT_RECEIPT_IMAGE_URL') . $wallet_request->image }}" target="_blank" class="badge badge-info">
                                            Receipt Image
                                        </a>
                                    @endif
                                </td>
                                <td class="text-danger">{{ \Carbon\Carbon::parse($wallet_request->deposited_at)->format('M d, Y h:i A')  }}</td>
                                <td>
                                    @if($wallet_request->status == 1)
                                        <span class="badge badge-warning">Pending</span>
                                    @elseif($wallet_request->status == 2)
                                        <span class="badge badge-success">Approved</span>
                                    @else
                                        <span class="badge badge-danger">Rejected</span>
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop