@extends('admin.template.layout')

@section('title', 'Create Stock Purchase')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Stock Purchase:admin-store-manager-stock-purchase-view, Create:active)

    <div class="container-fluid container-fixed-lg" id="purchaseOrderPage">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Select Supplier</label>
                            <vue-select2 :options="suppliers" place-holder="Select Supplier" allow-search="0" v-model="supplier_id"></vue-select2>
                        </div>
                        <div class="form-group">
                            <label>Select Delivery Type</label>
                            <select class="form-control" v-model="delivery_type">
                                <option value="">Select</option>
                                <option value="1">Self Pickup</option>
                                <option value="2">Courier</option>
                                <option value="3">Transport</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Your Message or Remarks</label>
                            <textarea class="form-control" cols="30" rows="6" placeholder="Your Message" v-model="remarks"></textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h3 class="text-success">
                            Stock Purchase Note
                        </h3>
                        <p>Once Stock Purchase Create, it will add the stocks to <code>Company's Main</code> Stock System.</p>
                        <p>After adding the Stock to Company, You can supply items to your Stores / Stock Points or Warehouse.</p>
                        <p>Once, Stock added <code>you can not edit</code>. However, a Return option is available.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="card" v-if="supplier_id && delivery_type">
            <div class="card-content collapse show">
                <div class="card-body table-responsive">
                    <div class="form-group">
                        <label>Search Product / Item</label>
                        <vue-select2 :options="search_items" place-holder="Select Item" empty-after-select="1" allow-search="0" @change-select2="setProductToOrder"></vue-select2>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="37%">Item</th>
                            <th width="10%">Qty</th>
                            <th width="12%">Purchase Price</th>
                            <th width="10%">Taxable Amount</th>
                            <th width="10%">GST</th>
                            <th width="13%">Total</th>
                            <th width="3%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(order_item, index) of order_items">
                            <td>@{{ index+1 }}</td>
                            <td>
                                @{{ order_item.name | str_limit(35) }} <br> <code>Code: @{{ order_item.code }}</code>
                            </td>
                            <td>
                                <input type="text" onkeypress="INGENIOUS.numericInput(event, false)" class="form-control form-control-sm" v-model="order_item.selected_qty">
                            </td>
                            <td>
                                <input type="text" onkeypress="INGENIOUS.numericInput(event)" class="form-control form-control-sm" v-model="order_item.purchase_price">
                            </td>
                            <td>
                                <span class="">@{{ parseFloat(order_item.selected_qty*order_item.purchase_price).toFixed(2) }}</span>
                            </td>
                            <td>
                                <span class="">@{{ (parseFloat(order_item.selected_qty*order_item.purchase_price*order_item.tax_percentage/100)).toFixed(2) }}</span>
                                <br>
                                <span class="font-weight-bold font-small-1">(@{{ order_item.tax_percentage }}%)</span>
                            </td>
                            <td>
                                <span class="font-weight-bold font-small-3 text-danger">@{{ (parseFloat(order_item.selected_qty*order_item.purchase_price) + parseFloat(order_item.selected_qty*order_item.purchase_price*order_item.tax_percentage/100)).toFixed(2) }}</span>
                            </td>
                            <td>
                                <a href="javascript:void(0)" class="btn btn-social-icon btn-xs btn-danger" title="Remove" @click="removeItem(index)">
                                    <span class="fa fa-close"></span>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="row" v-if="order.amount > 0">
                        <div class="col-md-6">
                            <ul class="list-group mt-3">
                                <li class="list-group-item">
                                    Amount: <span class="pull-right">@{{ order.amount }}</span>
                                </li>
                                <li class="list-group-item">
                                    GST/TAX: <span class="pull-right">@{{ order.gst }}</span>
                                </li>
                                <li class="list-group-item font-weight-bold font-small-3 bg-danger text-white">
                                    Total: <span class="pull-right">@{{ order.total_amount }}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <div class="text-center mt-5">
                                <button class="btn btn-danger btn-lg" type="button" @click="createOrder">Create</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>

        Vue.prototype.$http = axios;

        new Vue({
            el:"#purchaseOrderPage",
            data:{
                suppliers: {!! json_encode($suppliers) !!},
                items: {!! collect($items)->toJson() !!},
                supplier_id: null,
                delivery_type: '',
                remarks: null,
                order_items: [],
                order: {
                    amount: 0, total_qty: 0, gst: 0, total_amount: 0
                },
            },
            watch: {
                order_items: {
                    handler() {
                        this.calculateSummary();
                    },
                    deep: true
                }
            },
            methods: {
                setProductToOrder: function (selected_item_id) {

                    let item_exists = _.chain(this.order_items).filter(order_item => {
                        return parseInt(order_item.id) === parseInt(selected_item_id)
                    }).head().value();

                    if (!item_exists) {

                        let selected_product = _.chain(this.items).filter(item => {
                            return parseInt(item.id) === parseInt(selected_item_id)
                        }).head().value();

                        if (selected_product) {
                            this.order_items.push(selected_product);
                        }

                    }

                },
                removeItem: function (index) {

                    let self = this;
                    swal({
                        title: "Are you sure?",
                        text: 'Are you confirm to remove this item ?',
                        icon: "info", buttons: true, dangerMode: true
                    }).then( function (isConfirm) {

                        if (isConfirm) {
                            self.$delete(self.order_items, index);
                            swal('Removed', 'Item has been removed form List', 'success');
                        }
                    });
                },
                calculateSummary: function () {

                    this.order.amount = _.sumBy(this.order_items, item => {
                        return _.round((item.selected_qty*item.purchase_price),2);
                    });

                    this.order.total_qty = _.sumBy(this.order_items, item => {
                        return parseInt(item.selected_qty);
                    });

                    this.order.gst = _.sumBy(this.order_items, item => {
                        let amount = parseFloat((item.selected_qty*item.purchase_price).toFixed(2));
                        return _.round((amount*item.tax_percentage/100), 2);
                    });

                    this.order.total_amount = this.order.amount + this.order.gst;

                },
                createOrder: function () {

                    let self = this;

                    swal({
                        title: "Are you sure?",
                        text: 'Are you confirm to Create New Stock Purchase?',
                        icon: "info",
                        buttons: true,
                        dangerMode: true
                    }).then( function (isConfirm) {

                        if (isConfirm) {

                            INGENIOUS.blockUI(true);

                            self.$http.post('{{ route("admin-store-manager-stock-purchase-create") }}', {
                                order_items: self.order_items,
                                order_details: self.order,
                                supplier_id: self.supplier_id,
                                delivery_type: self.delivery_type,
                                remarks: self.remarks,
                            }).then(response => {

                                if (response.data.status) {
                                    window.location = response.data.route;
                                }
                                else {
                                    INGENIOUS.blockUI(false);
                                    swal('Oops', response.data.message, 'error');
                                }

                            });

                        }

                    });

                }
            },
            computed: {

                search_items: function () {

                    return this.items.map(item => {
                        item.value = item.id;
                        item.label = item.name + ' (Code: ' + item.code + ')';
                        return item;
                    });

                }
            }
        });

    </script>
@stop