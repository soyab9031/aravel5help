@extends('admin.template.layout')

@section('title', 'Store Sales Report')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Store Sales Report:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row mb-2">
                        <div class="col-md-5">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-primary"> Search </button>
                            @refreshBtn()
                        </div>
                        <div class="col-md-4">
                            <div class="alert alert-primary">
                                Report: {{ $start_at->format('M d, Y') }} - {{ $end_at->format('M d, Y') }}
                            </div>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Store</th>
                            <th class="bg-danger-light text-white">Sales</th>
                            <th class="bg-warning-light text-dark">Orders</th>
                            <th>Wallet Balance</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($stores) == 0)
                            <tr>
                                <td colspan="10" class="text-center">No Report Available</td>
                            </tr>
                        @endif
                        @foreach ($stores as $index => $store)
                            <tr>
                                <td>{{ ($index+1) }}</td>
                                <td class="text-primary">{{ $store->name }} (ID: {{ $store->tracking_id }})</td>
                                <td class="bg-danger-lighter">
                                    {{ number_format($store->order_details->total_amount) }}
                                </td>
                                <td class="bg-warning-lighter">{{ number_format($store->order_details->order_count) }}</td>
                                <td>{{ number_format($store->wallet) }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="2" class="text-right">Total</td>
                            <td class="bg-danger-light text-white">{{ number_format(collect($stores)->sum('order_details.total_amount')) }}</td>
                            <td class="bg-warning-light text-dark">{{ number_format(collect($stores)->sum('order_details.order_count')) }}</td>
                            <th></th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop