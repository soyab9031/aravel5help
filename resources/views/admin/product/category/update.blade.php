@extends('admin.template.layout')

@section('title', 'Update Category')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Category:admin-category-view,Update:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default">
                                <label>Parent Category</label>
                                <input type="text" name="name" class="form-control" value="{{ $category->parent_id ? $category->parent->name : 'N.A' }}" readonly>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Category Name</label>
                                <input type="text" name="name" class="form-control" value="{{ $category->name }}" required>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                    <option value="1" {{ $category->status == 1 ? 'selected' : '' }} >Active</option>
                                    <option value="2" {{ $category->status == 2 ? 'selected' : '' }}>Inactive</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-danger"> Update </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop