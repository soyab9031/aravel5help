@extends('admin.template.layout')

@section('title', 'Wallet Report')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Wallet Balance Report:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search Keyword" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>User</th>
                            <th>Tracking ID</th>
                            <th>Contact</th>
                            <th>Balance</th>
                            <th>Details</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($reports as $index => $report)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($reports, $index) }}</td>
                                <td>
                                    <a href="{{ route('admin-wallet-status', ['tracking_id' => $report->user->tracking_id ]) }}" title="View Wallet Status">{{ $report->user->detail->full_name }}</a>
                                </td>
                                <td>{{ $report->user->tracking_id }}</td>
                                <td>{{ $report->user->mobile }}</td>
                                <td class="text-danger">{{ number_format($report->balance, 2) }}</td>
                                <td>
                                    <a href="{{ route('admin-wallet-status', ['tracking_id' => $report->user->tracking_id ]) }}" class="btn btn-primary btn-xs"> View </a>
                                </td>
                            </tr>
                        @endforeach
                        <tr class="bg-danger-darker text-white">
                            <td class="text-right" colspan="4">Total</td>
                            <td class="large-text">{{ number_format($reports->sum('balance'), 2) }}</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                    {{ $reports->appends(['search' => Request::get('search')])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop