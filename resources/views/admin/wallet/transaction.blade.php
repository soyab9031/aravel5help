@extends('admin.template.layout')

@section('title', 'Wallet Transaction')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Wallet:admin-wallet-status,Transaction:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-lg-3 offset-md-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <span class="text-danger">* Press Search button to Find User</span>
                            <div class="form-group form-group-default input-group">
                                <div class="form-input-group">
                                    <label>Search User</label>
                                    <input type="text" class="form-control tracking_id_input"  required autocomplete="off">
                                </div>
                                <div class="input-group-append">
                                    <button class="btn btn-danger" type="button" onclick="INGENIOUS.getUser(this, 'tracking_id_input')">Search</button>
                                </div>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Type</label>
                                <select name="type" class="full-width" data-init-plugin="select2">
                                    <option value="1" {{ old('type') == 1 ? 'selected' : '' }}>Credit</option>
                                    <option value="2" {{ old('type') == 2 ? 'selected' : '' }}>Debit</option>
                                </select>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Amount</label>
                                <input type="number" name="amount" class="form-control" value="{{ old('amount') }}" required>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="remarks"cols="10" rows="5" placeholder="Remarks" >{{ old('remarks') }}</textarea>
                            </div>
                            <div class="text-center">
                                <p class="small text-primary">Total, Amount & TDS should be in Rupees Only</p>
                                <button class="btn btn-danger"> Create </button>
                            </div>
                        </form>
                    </div>
                </div></div>
        </div>
    </div>
@stop