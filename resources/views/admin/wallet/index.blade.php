@extends('admin.template.layout')

@section('title', 'Wallet Report')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Wallet Report:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <select  class="full-width" data-init-plugin="select2" name="incomeType">
                                    <option value="" selected disabled>Select</option>
                                    @foreach(\App\Models\Wallet::incomeTypes() as $index => $income_type)
                                        @if($index > 0)
                                            <option value="{{ $index }}" {{ old('incomeType') == $index ? 'selected' : ''}}>{{ $income_type }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            <a href="{{ route('admin-wallet-report', ['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'incomeType' => Request::get('incomeType'), 'export' => 'yes']) }}" class="btn btn-info text-white btn-sm"> <i class="fa fa-download"></i> Download </a>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th colspan="4">
                                <span class="text-complete"><i class="fa fa-square"></i> Credit &nbsp; </span>
                                <span class="text-danger"><i class="fa fa-square"></i> Debit</span>
                            </th>
                            <th colspan="2" class="text-center bg-success-darker text-white">Tax / Charges</th>
                            <th colspan="4"></th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>User</th>
                            <th>Total</th>
                            <th>TDS</th>
                            <th>Admin</th>
                            <th>Amount</th>
                            <th>Income Type</th>
                            <th>Remarks</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($wallets as $index => $wallet)
                            <tr class="{{ $wallet->type == 1 ? 'bg-complete-lighter' : 'bg-danger-lighter' }}">
                                <td>{{ \App\Library\Helper::tableIndex($wallets, $index) }}</td>
                                <td>{{ $wallet->created_at->format('M d, Y') }}</td>
                                <td>
                                    {{ $wallet->user->detail->full_name }} <br>
                                    ({{ $wallet->user->tracking_id }})
                                </td>
                                <td>{{ $wallet->total }}</td>
                                <td>{{ $wallet->tds }}</td>
                                <td>{{ $wallet->admin_charge }}</td>
                                <td>{{ $wallet->amount }}</td>
                                <td class="text-primary">{{ $wallet->income_type_name }}</td>
                                <td>{{ $wallet->remarks }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $wallets->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'incomeType' => Request::get('incomeType')])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop