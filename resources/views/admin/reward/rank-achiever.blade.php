@extends('admin.template.layout')

@section('title', 'Reward Achievers')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Reward Achievers:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="input-group">
                                <select name="reward_id" class="full-width" data-init-plugin="select2">
                                    <option value="">Select</option>
                                   @foreach($rewards as $reward)
                                        <option value="{{ $reward->id }}" {{ Request::get('reward_id') == $reward->id ? 'selected' : '' }}>
                                            {{ $reward->name }}
                                        </option>
                                   @endforeach

                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="13%">Achievement Date</th>
                            <th>Name</th>
                            <th>Contact number</th>
                            <th>Reward Name</th>
                            <th>Income</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($rewardAchievers as $index => $achiever)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                {{--<td>{{ \App\Library\Helper::tableIndex($achiever, $index) }}</td>--}}
                                <td>{{ $achiever->created_at->format('M d, Y') }} </td>
                                <td>
                                    {{ $achiever->user->detail->full_name }} <br>
                                    ({{ $achiever->user->tracking_id }})
                                </td>
                                <td> {{ $achiever->user->mobile }}</td>
                                <td>{{ $achiever->reward->name }}</td>
                                <td>{{ $achiever->reward->reward }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $rewardAchievers->appends(['search' => Request::get('search'), 'status' => Request::get('status'), 'dateRange' => Request::get('dateRange') ])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop