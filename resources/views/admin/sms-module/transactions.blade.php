@extends('admin.template.layout')

@section('title', 'Sms Report')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Sms Report:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-group">
                                <select  class="full-width" data-init-plugin="select2" name="category" data-disable-search="true">
                                    <option value="">Filter Category</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category }}" {{ Request::get('category') == $category ? 'selected' : '' }}>{{ ucwords($category) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="15%">Date</th>
                            <th>Category</th>
                            <th>Receipt</th>
                            <th width="40%">Message</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($transactions as $index => $transaction)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($transactions, $index) }}</td>
                                <td>{{ Carbon\Carbon::parse($transaction->created_at)->format('M d, Y h:i A') }}</td>
                                <td><code>{{ $transaction->category }}</code></td>
                                <td>
                                    @if($transaction->user)
                                        {{ $transaction->user->detail->full_name }} <span class="bold">({{ $transaction->user->tracking_id }})</span>
                                    @else
                                        <span class="text-dark">{{ $transaction->mobile }}</span>
                                    @endif
                                </td>
                                <td>{{ $transaction->message }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $transactions->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'category' => Request::get('category') ])->links()}}
                </div>
            </div>
        </div>
    </div>

@stop
