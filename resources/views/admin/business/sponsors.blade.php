@extends('admin.template.layout')

@section('title', 'Sponsor Team Details')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Sponsor Team Detail:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" placeholder="Search Tracking Id" name="tracking_id" class="form-control" value="{{ Request::get('tracking_id') }}">
                                <div class="input-group-prepend">
                                    <button class="btn btn-danger"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                            <p class="small text-danger m-t-10">
                                Search Users through their Tracking ID & Get List of Sponsors
                            </p>
                        </div>
                        <div class="col-md-6">
                            @refreshBtn()
                        </div>
                    </div>
                    @if($searched_user)
                        <ul class="list-group m-t-10">
                            <li class="list-group-item bg-primary-light text-white">
                                User: <span class="pull-right">{{ $searched_user->detail->full_name }}</span>
                            </li>
                            <li class="list-group-item">
                                Tracking Id: <span class="pull-right">{{ $searched_user->tracking_id }}</span>
                            </li>
                        </ul>
                    @endif
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Package</th>
                            <th>User</th>
                            <th>Direct Upline</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($children) == 0)
                            <tr>
                                <td colspan="7" class="text-center">
                                    No any sponsors Available
                                </td>
                            </tr>
                        @endif
                        @foreach($children as $index => $child)
                            <tr class="accordion-toggle">
                                <td>{{ ($index+1) }}</td>
                                <td>
                                    <span class="text-primary"> Joining : {{ $child->created_at->format('M d, Y h:i A') }}</span><br>
                                    <span class="text-danger"> Activation : {{ $child->paid_at ? \Carbon\Carbon::parse($child->paid_at)->format('M d, Y h:i A') : 'N.A'}}</span>
                                </td>
                                <td>
                                    @if($child->package)
                                        {{ $child->package->name }} <br> (Rs. {{ $child->joining_amount }})
                                    @else
                                        Not Available
                                    @endif
                                </td>
                                <td class="text-primary">
                                    {{ $child->detail->full_name }} <br>
                                    ({{ $child->tracking_id }})
                                </td>
                                <td>
                                    {{ $child->parentBy ? $child->parentBy->detail->full_name : '' }} <br>
                                    {{ $child->parentBy ? '(' . $child->parentBy->tracking_id . ')' : '' }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop