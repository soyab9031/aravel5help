<table class='table table-bordered table-hover custom-table'>
    <tr>
        <td><b>Name</b></td>
        <td>
            {{ $user->detail->full_name }}<br/>
            <a href='{{ route('admin-user-update', ['id' => $user->id]) }}' target='_blank'>Edit</a> |
            <a href='{{ route('admin-user-account-access', ['id' => $user->id]) }}' target='_blank'>View</a> |
            <a href='{{ route('admin-business-sponsors-view', ['tracking_id' => $user->tracking_id]) }}' target='_blank'>Sponsors</a> |
            <a href='{{ route('admin-wallet-status', ['tracking_id' => $user->tracking_id]) }}' target='_blank'>Wallet</a>
        </td>
    </tr>
    <tr>
        <td><b>Join Date</b></td>
        <td>{{ \Carbon\Carbon::parse($user->created_at)->format('M d, Y') }}</td>
    </tr>
    <tr>
        <td><b>Activation Date</b></td>
        <td>{{ $user->paid_at ? \Carbon\Carbon::parse($user->paid_at)->format('M d, Y') : 'N.A' }}</td>
    </tr>
    @php $binary_total = $user->current_binary_status @endphp
    <tr>
        <td><b>Left/Right</b></td>
        <td>
            {{ $binary_total->left }} / {{ $binary_total->right }}
        </td>
    </tr>
    <tr>
        <div class='table-responsive'>
            <table class='table table-bordered table-hover custom-table'>
                <tr>
                    <td><b>Sponsor By</b></td>
                    <td>
                        {{ $user->sponsorBy ? $user->sponsorBy->tracking_id : '' }}
                    </td>
                </tr>
                <tr>
                    <td>City</td>
                    <td>{{ $user->address->city }}</td>
                </tr>
            </table>
        </div>
    </tr>
    @php

        $binary_total = $user->current_binary_status;

        $current_month = now()->format("M Y");
        $previous_month = now()->subMonthNoOverflow()->format("M Y");

        $team_records = \App\Models\MonthlyBv::whereIn('month_name', [$current_month, $previous_month])->whereUserId($user->id)->get();

        $self_records = \App\Models\OrderBvLog::whereUserId($user->id)
        ->selectRaw("COALESCE(SUM(bv), 0) as total_bv, DATE_FORMAT(created_at, '%b %Y') as month_name")
        ->whereNull('leg')->whereRaw("DATE_FORMAT(created_at, '%b %Y') in ('{$current_month}', '{$previous_month}')")->groupBy('month_name')->get();

        $current_month_self_bv = optional($self_records->where('month_name', $current_month)->first())->total_bv;
        $previous_month_self_bv = optional($self_records->where('month_name', $previous_month)->first())->total_bv;

        $current_month_team_record = $team_records->where('month_name', $current_month)->first();
        $previous_month_team_record = $team_records->where('month_name', $previous_month)->first();

    @endphp
    <tr>
        <div class='table-responsive'>
            <table class='table table-bordered  custom-table'>
                <tr class='bg-danger text-white'>
                    <td colspan='4' class='text-center'><b>Current Month BV</b></td>
                </tr>
                <tr>
                    <td>SELF</td>
                    <td>Left</td>
                    <td>Right</td>
                </tr>
                <tr>
                    <td>{{ $current_month_self_bv ? : 0 }}</td>
                    <td>{{ $current_month_team_record ? $current_month_team_record->lft : 0 }}</td>
                    <td>{{ $current_month_team_record ? $current_month_team_record->rgt : 0 }}</td>
                </tr>
                <tr class='bg-primary text-white'>
                    <td colspan='4' class='text-center'><b>Previous Month BV</b></td>
                </tr>
                <tr>
                    <td>SELF</td>
                    <td>Left</td>
                    <td>Right</td>
                </tr>
                <tr>
                    <td>{{ $previous_month_self_bv ? : 0 }}</td>
                    <td>{{ $previous_month_team_record ? $previous_month_team_record->lft : 0 }}</td>
                    <td>{{ $previous_month_team_record ? $previous_month_team_record->rgt : 0 }}</td>
                </tr>
            </table>
        </div>

    </tr>
</table>