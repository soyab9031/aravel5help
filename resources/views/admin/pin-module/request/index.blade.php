@extends('admin.template.layout')

@section('title', 'Pin Request List')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Pin:admin-pin-view,Pin Request List:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th> Date </th>
                            <th> User </th>
                            <th width="25%"> Details </th>
                            <th> Payment </th>
                            <th> Status </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($pin_requests as $index => $pin_request)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($pin_requests, $index) }}</td>
                                <td>{{ $pin_request->created_at->format('M d, Y h:i A') }} </td>
                                <td>
                                    {{ $pin_request->user->detail->full_name }}
                                    <br> ({{ $pin_request->user->tracking_id }})
                                </td>
                                <td>
                                    @php $amount = 0; @endphp
                                    @foreach(json_decode($pin_request->details) as $pin_detail)
                                        @php $amount = $pin_detail->qty * $pin_detail->package_amount @endphp
                                        <span class="d-block  text-primary">{{ $pin_detail->package_name }}</span>
                                        ({{ $pin_detail->qty }} Pins X Rs. {{ $pin_detail->package_amount }}) = Rs. {{ $amount }}
                                        <hr>
                                    @endforeach
                                </td>
                                <td>
                                    <span class="text-danger d-block">{{ $pin_request->payment_mode }} Mode</span>
                                    {{ \Carbon\Carbon::parse($pin_request->deposited_at)->format('M d, Y h:i A')  }} <br>
                                    Ref No: {{ $pin_request->reference_number }} <br>
                                    @if($pin_request->image)
                                        <a href="{{ env('PAYMENT_RECEIPT_IMAGE_URL') . $pin_request->image }}" target="_blank" class="btn-xs btn btn-primary">
                                            Receipt
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    @if ($pin_request->status == 1 )
                                        <span class="label label-warning">Pending</span>
                                    @elseif ($pin_request->status == 2 )
                                        <span class="label label-success">Approved</span>
                                    @else
                                        <span class="label label-danger">Rejected</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin-pin-request-update', ['id' => $pin_request->id]) }}" class="btn btn-theme btn-sm">
                                        Update
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $pin_requests->appends([
                    'search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'package_id' => Request::get('package_id')
                    ])->links() }}
                </div>
            </div>
        </div>
    </div>

@stop