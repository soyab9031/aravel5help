@extends('admin.template.layout')

@section('title', 'Update Pin Request')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Pin Request:admin-pin-request-view,Update Pin Request:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Request Date:
                                    <span class="pull-right">{{ $pin_request->created_at->format('M d, Y') }}</span>
                                </li>
                                <li class="list-group-item">
                                    User:
                                    <span class="pull-right">{{ $pin_request->user->detail->full_name }}</span>
                                </li>
                                <li class="list-group-item">
                                    User Tracking ID:
                                    <span class="pull-right text-dark">{{ $pin_request->user->tracking_id }}</span>
                                </li>
                                <li class="list-group-item">
                                    Request Pin Quantity:
                                    <span class="pull-right text-dark">{{ $pin_request->pin_quantity }}</span>
                                </li>
                                <li class="list-group-item">
                                    Package:
                                    <span class="pull-right text-dark">
                                    @foreach(json_decode($pin_request->details) as $pin_detail)
                                            {{ $pin_detail->package_name }} <br>
                                            ({{ $pin_detail->qty }} Pins X Rs. {{ $pin_detail->package_amount }})
                                        @endforeach
                                    </span>
                                </li>
                                <li class="list-group-item">
                                    Deposit Bank:
                                    <span class="pull-right text-dark">{{ $pin_request->bank_name }}</span>
                                </li>
                                <li class="list-group-item">
                                    Reference Number:
                                    <span class="pull-right text-dark">{{ $pin_request->reference_number }}</span>
                                </li>
                                @if($pin_request->image)
                                    <li class="list-group-item">
                                        Reference Image:
                                        <span class="pull-right">
                                         <a href="{{ env('PAYMENT_RECEIPT_IMAGE_URL') . $pin_request->image }}" target="_blank" class="btn-xs btn btn-primary">
                                        View Image
                                    </a>
                                    </span>
                                    </li>
                                @endif
                                <li class="list-group-item">
                                    Deposit At:
                                    <span class="pull-right text-dark">
                                        {{ \Carbon\Carbon::parse($pin_request->deposited_at)->format('M d, Y h:i A')  }} <br>
                                    </span>
                                </li>
                            </ul>
                            <div class="form-group form-group-default form-group-default-select2 m-t-10">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2">
                                    <option value="1" {{ $pin_request->status == 1 ? 'selected' : '' }}>Pending</option>
                                    <option value="2" {{ $pin_request->status == 2 ? 'selected' : '' }}>Approved</option>
                                    <option value="3" {{ $pin_request->status == 3 ? 'selected' : '' }}>Rejected</option>
                                </select>
                            </div>
                            <div class="text-center m-t-10">
                                <button class="btn btn-theme btn-lg">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop