@extends('admin.template.layout')

@section('title', 'Create Pin')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Pin:admin-pin-view,Create:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" id="generatePin" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <span class="text-danger">* Press Search button to Find User</span>
                            <div class="form-group form-group-default input-group">
                                <div class="form-input-group">
                                    <label>Search User</label>
                                    <input type="text" class="form-control tracking_id_input"  required autocomplete="off">
                                </div>
                                <div class="input-group-append">
                                    <button class="btn btn-danger" type="button" onclick="INGENIOUS.getUser(this, 'tracking_id_input')">Search</button>
                                </div>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Number Of Pins</label>
                                <input type="text" min="1" max="50" name="pin_qty" class="form-control" value="{{ old('pin_qty') }}" required autocomplete="off">
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Package</label>
                                <select name="package_id" class="full-width" data-init-plugin="select2">
                                    @foreach ($packages as $package)
                                        <option value="{{ $package->id }}" >
                                            {{ $package->name.' ( Rs.'.$package->amount.')' }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <hr>
                            <h4>Payment Details</h4>
                            <div class="form-group form-group-default">
                                <label>Payment Date</label>
                                <input type="text" name="payment_at" class="form-control" id="payment_at" autocomplete="off" value="{{ old('payment_at') }}" required readonly>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Payment Mode</label>
                                <select name="payment_mode" class="full-width" data-init-plugin="select2">
                                    @foreach($payment_modes as $payment_mode)
                                        <option value="{{ $payment_mode }}" {{ old('payment_mode') == $payment_mode ? 'selected' : '' }}>
                                            {{ $payment_mode }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Bank Name</label>
                                <input type="text" name="bank_name" class="form-control" autocomplete="off" value="{{ old('bank_name') }}">
                            </div>
                            <div class="form-group form-group-default">
                                <label>Remarks</label>
                                <input type="text" name="remarks" class="form-control" autocomplete="off" value="{{ old('remarks') }}" required>
                            </div>
                            <div class="text-center mt-1">
                                <button type="button" onclick="INGENIOUS.formConfirmation('#generatePin', 'Are you confirm to generate pins for this Package?')" class="btn btn-primary btn-lg btn-rounded"> Generate PIN </button>&nbsp;
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>
        $('#payment_at').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            startDate: "-1y",
            endDate: "+1M"
        });
    </script>
@stop
