@extends('admin.template.layout')

@section('title', 'Order Details ' . $order->customer_order_id)

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Orders:admin-shopping-order-view, Details:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <h5>Order Details</h5>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Admin Manager: <span class="pull-right">{{ isset($order->admin->name) ? $order->admin->name : 'N.A' }}</span>
                                </li>
                                <li class="list-group-item">
                                    Order Id: <span class="pull-right">{{ $order->customer_order_id }}</span>
                                </li>


                                <li class="list-group-item">
                                    Order Date: <span class="pull-right">{{ $order->created_at->format('M d, Y h:i A') }}</span>
                                </li>
                                @if($order->store_id)
                                    <li class="list-group-item">
                                        Store: <span class="pull-right">{{ $order->store->name}}</span>
                                    </li>
                                    <li class="list-group-item">
                                        Store Id: <span class="pull-right">{{ $order->store->tracking_id }}</span>
                                    </li>
                                @endif
                                <li class="list-group-item">
                                    Remarks: <span class="pull-right">{{ $order->remarks }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <h5>Payment Details</h5>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Payment Mode: <span class="pull-right"> {{ $order->payment_reference['payment_mode'] ? $order->payment_reference['payment_mode'] : 'N.A' }}</span>
                                </li>
                                <li class="list-group-item">
                                    Payment Reference NO:<span class="pull-right">
                                         {{ $order->payment_reference['reference_number'] ? $order->payment_reference['reference_number'] : 'N.A' }}
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <h5>Order Summary</h5>
                            <ul class="list-group">
                                <li class="list-group-item bg-dark text-white">
                                    Status: <span class="pull-right">{{ \App\Models\Order::getStatus($order->status) }}</span>
                                </li>
                                <li class="list-group-item">
                                    Total Items: <span class="pull-right">{{ $order->details->sum('qty') }}</span>
                                </li>
                                <li class="list-group-item">
                                    Amount <span class="pull-right">{{ $order->amount }}</span>
                                </li>
                                <li class="list-group-item">
                                    Shipping Charge <span class="pull-right">{{ $order->shipping_charge }}</span>
                                </li>
                                <li class="list-group-item text-danger">
                                    Total Amount <span class="pull-right">{{ $order->total }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <h5>Payment Reference Image</h5>
                            <ul class="list-group">
                                <li class="list-group-item bg-dark text-white">
                                    @if($order->payment_ref_image)
                                        <span class="pull-right">
                                         <img src="{{ env('PAYMENT_RECEIPT_IMAGE_URL').$order->payment_ref_image }}" alt="" class="pull-right border" width="20%">
                                        <a href="{{ env('PAYMENT_RECEIPT_IMAGE_URL').$order->payment_ref_image }}" target="_blank" class="btn  btn-xs btn-primary">View Image</a>
                                    </span>
                                    @else
                                        N.A
                                    @endif
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" id="detailPage">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            @if($order->status == \App\Models\Order::PLACED)
                                <h5>Update Order</h5>
                                <form action="" method="post">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <select name="status" id="" class="form-control" v-model="status">
                                            <option value="2" {{ $order->status == 2 ? 'selected' : '' }}>Placed</option>
                                            <option value="3" {{ $order->status == 3 ? 'selected' : '' }}>Approved</option>
                                            <option value="5" {{ $order->status == 5 ? 'selected' : '' }}>Rejected</option>
                                        </select>
                                    </div>
                                    <div v-if="status == 3">
                                        <div class="form-group mt-2">
                                            <label>Payment Mode</label>
                                            <select class="form-control" name="payment_mode">
                                                @foreach($payment_modes as $payment_mode)
                                                    <option value="{{ $payment_mode }}"
                                                            {{ $order->payment_reference['payment_mode'] == $payment_mode ? 'selected' : '' }}>
                                                        {{ $payment_mode }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Reference Number (optional)</label>
                                            <input type="text" name="reference_number" placeholder="Enter Reference Number" class="form-control" value=" {{ $order->payment_reference['reference_number'] ? $order->payment_reference['reference_number'] : 'N.A' }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Your Message or Remarks (optional)</label>
                                        <textarea name="admin_remarks" class="form-control" cols="30" rows="5" placeholder="Your Message or Remarks"></textarea>
                                    </div>

                                    <div class="text-center">
                                        <button class="btn btn-md btn-primary" type="submit">Update</button>
                                    </div>
                                </form>
                            @else
                                @if($order->status == 3)
                                    <span class="alert block alert-primary">This Order is approved by admin <br> Remarks :{{ $order->admin_remarks }}</span>
                                @else
                                    <span class="alert block alert-danger">This Order is rejected by admin <br> Remarks :{{ $order->admin_remarks }}</span>
                                @endif
                                <a href="{{ route('admin-shopping-order-view') }}" class="btn btn-md btn-primary"><i class="fa fa-arrow-circle-left"></i> Back</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>GST</th>
                                <th>Amount</th>
                                <th>Qty</th>
                                <th class="text-danger">Total Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order->details as $index => $detail)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>
                                        {{ $detail->product_price->product->name }}
                                        <code class="pull-right">CODE: {{ $detail->product_price->code }}</code>
                                    </td>
                                    <td>{{ $detail->gst->percentage }}%</td>
                                    <td>{{ $detail->selling_price }}</td>
                                    <td>{{ $detail->qty }}</td>
                                    <td class="text-danger">{{ $detail->total_amount }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>

        new Vue({
            el: '#detailPage',
            data: {
                status:''
            }});
    </script>
@endsection