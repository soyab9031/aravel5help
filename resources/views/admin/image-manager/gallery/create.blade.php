@extends('admin.template.layout')

@section('title', 'Create Gallery')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Gallery:admin-gallery-view,Create:active)

    <div class="container-fluid container-fixed-lg">
        <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group form-group-default">
                                <label>Title of Gallery</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
                            </div>
                            <label>Primary Image</label>
                            <div class="form-group">
                                <input type="file" name="primary_image" id="primaryImage">
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-md btn-danger">Create </button>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="card" id="galleryPage">
                        <div class="card-body">
                            <div class="row" v-for="(gallery_item, index) in gallery_items">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Image Name</label>
                                        <input type="text" :name="'gallery_items['+index+'][name]'" class="form-control" value="" required>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <label>Image</label>
                                    <div class="form-group">
                                        <vue-filer add-more="0" v-bind:name="'gallery_items['+index+'][image]'"></vue-filer>
                                    </div>
                                </div>
                                <div class="col-md-1 mt-4">
                                    <button type="button" v-if="index == 0" @click="addGalleryItem" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></button>
                                    <button type="button" class="btn btn-danger btn-sm" @click="removeGalleryItem(index)" v-else><i class="fa fa-close"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop


@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
@stop

@section('page-javascript')
    <script>
        $('#primaryImage').filer({ limit: 1, maxSize: 2, extensions: ['jpg', 'jpeg', 'png'], changeInput: true, showThumbs: true });</script>


    <script>

        new Vue({
            el: '#galleryPage',
            data: {
                gallery_items: [{
                    name: '', image: null
                }],
            },
            methods: {
                addGalleryItem: function () {
                    this.gallery_items.push({name: '', image: null})
                },
                removeGalleryItem: function (index) {
                    this.$delete(this.gallery_items, index);
                }
            }
        });

    </script>
@stop

