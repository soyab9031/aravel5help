@extends('admin.template.layout')

@section('title', 'Update Gallery ' . $gallery->name)

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,gallery:admin-gallery-view,Update:active)

    <div class="container-fluid container-fixed-lg">
        <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group form-group-default">
                                <label>Title of Gallery</label>
                                <input type="text" name="name" class="form-control" value="{{ $gallery->name }}" required>
                            </div>
                            <label>Primary Image</label>
                            <div class="form-group">
                                <input type="file" name="primary_image" id="primaryImage">
                            </div>
                            <div>
                                <img src="{{ env('GALLERY_IMAGE_URL') . $gallery->primary_image }}" class="img-thumbnail">
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-lg btn-danger">Update </button>
                    </div>
                </div>
                <div class="col-md-7" id="galleryPage">
                    <div class="card">
                        <div class="card-body">
                            <h5>Add New Gallery Images</h5>
                            <div class="row" v-for="(gallery_item, index) in gallery_items">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Image Name</label>
                                        <input type="text" :name="'gallery_items['+index+'][name]'" class="form-control" value="" required>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <label>Image</label>
                                    <div class="form-group">
                                        <vue-filer add-more="0" v-bind:name="'gallery_items['+index+'][image]'"></vue-filer>
                                    </div>
                                </div>
                                <div class="col-md-1 mt-4">
                                    <button type="button" class="btn btn-danger btn-sm" @click="removeGalleryItem(index)"><i class="fa fa-close"></i></button>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="button" @click="addGalleryItem" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Add New Gallery Item</button>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h5>Existing Gallery Images</h5>
                            <div class="row">
                                @foreach($gallery->items as $gallery_item)
                                    <div class="col-md-3 text-center">
                                        <img src="{{ env('GALLERY_IMAGE_URL') . $gallery_item->image }}" class="img-thumbnail">
                                        <span class="text-danger d-block">
                                            {{ $gallery_item->name }}
                                        </span>
                                        <a href="javascript:void(0)" @click="deleteImage('{{ $gallery_item->id }}')" class="btn btn-danger btn-sm" title="Delete this Image"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop


@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
@stop

@section('page-javascript')
    <script>$('#primaryImage').filer({ limit: 1, maxSize: 2, extensions: ['jpg', 'jpeg', 'png'], changeInput: true, showThumbs: true });</script>

    <script>

        new Vue({
            el: '#galleryPage',
            data: {
                base_image_url: '{!! env('GALLERY_IMAGE_URL') !!}',
                gallery_items: [],
            },
            methods: {
                addGalleryItem: function () {
                    this.gallery_items.push({name: '', image: null})
                },
                removeGalleryItem: function (index) {
                    this.$delete(this.gallery_items, index);
                },
                deleteImage: function (gallery_item_id) {

                    let self = $(this);

                    self.attr('disabled', true);

                    swal({
                        title: "Are you sure?",
                        text: 'Are you confirm to process the Payout ?',
                        icon: "info",
                        buttons: true,
                        dangerMode: true
                    }).then( function (isConfirm) {

                        if (isConfirm) {

                            INGENIOUS.blockUI(true);
                            window.location = '?delete=' + gallery_item_id
                        }

                    });

                }
            }
        });

    </script>
@stop

