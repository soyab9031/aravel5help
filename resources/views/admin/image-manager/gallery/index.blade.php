@extends('admin.template.layout')

@section('title', 'Users')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Gallery:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-12 pull-right">
                            <a href="{{ route('admin-gallery-create') }}" class="btn btn-success btn-sm pull-right"> Create New </a>
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Created At</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($galleries as $index => $gallery)
                            <tr >
                                <td>{{ \App\Library\Helper::tableIndex($galleries, $index) }}</td>
                                <td>{{ $gallery->created_at->format('M d, Y') }}</td>
                                <td>{{ $gallery->name }}</td>
                                <td>
                                    @if( $gallery->status == 1 )
                                        <label class="label label-primary">Active</label>
                                    @else
                                        <label class="label label-primary">In Active</label>
                                    @endif
                                </td>
                                <td><a href="{{ env('GALLERY_IMAGE_URL') . $gallery->primary_image }}" target="_blank" class="btn-sm btn-primary"> View Image </a></td>
                                <td>
                                    <a href="{{ route('admin-gallery-update', [ 'id' => $gallery->id ]) }}" class="btn btn-info btn-xs"> Edit</a>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                    {{ $galleries->links() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')

@endsection

