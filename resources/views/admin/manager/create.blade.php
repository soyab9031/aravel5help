@extends('admin.template.layout')

@section('title', 'Create Admin Manager')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Admin Manager:admin-manager-view,Create:active)

    <div class="container-fluid container-fixed-lg">

        <div class="card">
            <div class="card-body">
                <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group form-group-default">
                                <label>Manager Name</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Mobile</label>
                                <input type="text" name="mobile" class="form-control" value="{{ old('mobile') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control" value="{{ old('email') }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Password</label>
                                <div class="controls input-group">
                                    <input type="password" class="form-control passwordInput" name="password" placeholder="Credentials" required>
                                    <div class="input-group-append">
                                        <a href="javascript:void(0)" onclick="INGENIOUS.showPassword(this)">
                                            <i class="fa fa-eye-slash"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Role Type</label>
                                <select name="type" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                    <option value="1" {{ old('type') == 1 ? 'selected' : '' }}>Master</option>
                                    <option value="2" {{ old('type') == 2 ? 'selected' : '' }}>Manager</option>
                                    <option value="3" {{ old('type') == 3 ? 'selected' : '' }}>Employee</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 text-center"> <hr> </div>
                        <div class="form-group col-md-12">
                            <label>Permission on Pages</label>
                            <select name="routes[]" class="listbox" multiple="multiple">
                                @foreach ($routes as $route)
                                    <option value="{{ $route->key }}">{{ $route->value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12 text-center">
                            <button class="btn btn-danger"> Create </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

@stop

@section('import-css')
    <link href="/plugins/bootstrap-duallistbox/bootstrap-duallistbox.css"  rel="stylesheet" type="text/css" />
@stop

@section('import-javascript')
    <script type="text/javascript" src="/plugins/bootstrap-duallistbox/jquery.bootstrap-duallistbox.js"></script>
@stop

@section('page-javascript')
    <script>
        $(function() {
            $('.listbox').bootstrapDualListbox({
                selectorMinimalHeight: 300
            });
        })
    </script>
@stop