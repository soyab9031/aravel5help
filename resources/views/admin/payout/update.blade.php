@extends('admin.template.layout')

@section('title', 'Payout Report')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Payout:admin-payout-view, Update:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                Name <span class="pull-right">{{ $payout->user->detail->full_name }}</span>
                            </li>
                            <li class="list-group-item">
                                Tracking Id <span class="pull-right">{{ $payout->user->tracking_id }}</span>
                            </li>
                        </ul>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Total <span class="pull-right">{{ $payout->total }}</span>
                            </li>
                            <li class="list-group-item">
                                TDS <span class="pull-right">{{ $payout->tds }}</span>
                            </li>
                            <li class="list-group-item">
                                Admin <span class="pull-right">{{ $payout->admin_charge }}</span>
                            </li>
                            <li class="list-group-item text-danger">
                                Payout <span class="pull-right">{{ $payout->amount }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                    <option value="1" {{ $payout->status == 1 ? 'selected' : '' }}>Pending</option>
                                    <option value="2" {{ $payout->status == 2 ? 'selected' : '' }}>Transferred</option>
                                </select>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Transfer Type</label>
                                <select name="transfer_type" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                    <option value="">Select</option>
                                    <option value="1" {{ $payout->transfer_type == 1 ? 'selected' : '' }} >Cheque</option>
                                    <option value="2" {{ $payout->transfer_type == 2 ? 'selected' : '' }}>NEFT</option>
                                    <option value="3" {{ $payout->transfer_type == 3 ? 'selected' : '' }}>IMPS</option>
                                    <option value="4" {{ $payout->transfer_type == 4 ? 'selected' : '' }}>CASH</option>
                                </select>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Payout Reference No / UTR</label>
                                <input type="text" name="reference_number" class="form-control" value="{{ $payout->reference_number }}">
                            </div>
                            <div class="form-group form-group-default">
                                <label>Remarks</label>
                                <input type="text" name="remarks" class="form-control" value="{{ $payout->remarks }}">
                            </div>
                            <div class="form-group text-center m-t-10">
                                <button class="btn btn-danger">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop