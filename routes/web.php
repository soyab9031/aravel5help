<?php

Route::group(['namespace' => 'Website'], function () {

//    Route::get('/', 'HomeController@video')->name('website-video');
    Route::get('/', 'HomeController@index')->name('website-home');

    Route::get('about', 'CompanyController@about')->name('website-company-about');
    Route::get('legals', 'CompanyController@legals')->name('website-company-legals');
    Route::get('vision-mission', 'CompanyController@vision_mission')->name('website-company-vision-mission');
    Route::get('terms', 'CompanyController@terms')->name('website-company-terms');

    Route::get('contact', 'ContactController@index')->name('website-contact');
    Route::post('contact', 'ContactController@index')->name('website-contact');

    Route::get('gallery','CompanyController@gallery')->name('website-gallery');
    Route::get('gallery-items','CompanyController@galleryItems')->name('website-gallery-items');

    Route::get('news','CompanyController@news')->name('website-news');
    Route::get('faqs','CompanyController@faqs')->name('website-company-faqs');

});
